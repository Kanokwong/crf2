import './App.css'
import {BrowserRouter, Route, Routes} from "react-router-dom";
import {ToastContainer} from "react-toastify";
import Personal from "./pages/Personal.jsx";
import TreatmentHistory from "./pages/TreatmentHistory.jsx";
import TreatmentHistoryOld from "./pages/TreatmentHistoryOld.jsx";
import SingleHospital from "./pages/SingleHospital.jsx";
import Login from "./pages/Login.jsx";
import LoginOld from "./pages/LoginOld.jsx";
import Register from "./pages/Register.jsx";
import RegisterOld from "./pages/RegisterOld.jsx";
import Home from "./pages/Home.jsx";
import Stepper from "./components/Stepper.jsx";
import LabResult from "./pages/LabResult.jsx";
import ResearchData from "./pages/ResearchData.jsx";
import ResearchData2 from "./pages/ResearchData2.jsx";
import Profile from "./pages/Profile.jsx";

function App() {

    return (
        <BrowserRouter>
            {/*<Header/>*/}
            <main className="h-full bg-gray-50 dark:bg-gray-900">
                <div className="mx-auto /*max-w-7xl*/ /*px-2*/ /*sm:px-6*/ /*lg:px-8*/">
                    <ToastContainer
                        position="bottom-right"
                    />
                    <Routes>
                        <Route path="/" element={<Personal/>} exact/>
                        {/*<Route path="/personal" element={<Personal/>}/>*/}
                        <Route path="/treatment" element={<TreatmentHistory/>} exact/>
                        <Route path="/treatmentOld" element={<TreatmentHistoryOld/>} exact/>
                        <Route path="/hospitals/:id" element={<SingleHospital/>}/>
                        <Route path="/login" element={<Login/>}/>
                        <Route path="/loginOld" element={<LoginOld/>}/>
                        <Route path="/register" element={<Register/>}/>
                        <Route path="/registerOld" element={<RegisterOld/>}/>
                        <Route path="/home" element={<Home/>}/>
                        <Route path="/stepper" element={<Stepper/>}/>
                        <Route path="/labResult" element={<LabResult/>}/>
                        <Route path="/telephoneInquiry" element={<ResearchData/>}/>
                        <Route path="/visitInquiry" element={<ResearchData2/>}/>
                        <Route path="/profile" element={<Profile/>}/>
                        {/*<Route path="*" element={PageNotFound} />*/}
                    </Routes>
                </div>

            </main>

        </BrowserRouter>
    )
}

export default App
