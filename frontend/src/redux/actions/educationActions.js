import axios from "axios";
import {EDUCATION_LIST_FAIL, EDUCATION_LIST_REQUEST, EDUCATION_LIST_SUCCESS} from "../constants/educationConstants";
import {urlApi} from "../../constant.js";

export const listEducation = () => async (dispatch) => {
    try {
        dispatch({type: EDUCATION_LIST_REQUEST});
        const {data} = await axios.get(urlApi + "api/educations");
        dispatch({type: EDUCATION_LIST_SUCCESS, payload: data});
    } catch (error) {
        dispatch({
            type: EDUCATION_LIST_FAIL,
            payload: error.response && error.response.data.message ? error.response.data.message : error.message,
        });
    }
};