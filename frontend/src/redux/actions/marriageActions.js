import axios from "axios";
import {MARRIAGE_LIST_FAIL, MARRIAGE_LIST_REQUEST, MARRIAGE_LIST_SUCCESS} from "../constants/marriageConstants";
import {urlApi} from "../../constant.js";

export const listMarriage = () => async (dispatch) => {
    try {
        dispatch({type: MARRIAGE_LIST_REQUEST});
        const {data} = await axios.get(urlApi + "api/marriages");
        dispatch({type: MARRIAGE_LIST_SUCCESS, payload: data});
    } catch (error) {
        dispatch({
            type: MARRIAGE_LIST_FAIL,
            payload: error.response && error.response.data.message ? error.response.data.message : error.message,
        });
    }
};