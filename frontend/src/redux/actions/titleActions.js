import axios from "axios";
import {TITLE_LIST_FAIL, TITLE_LIST_REQUEST, TITLE_LIST_SUCCESS} from "../constants/titleConstants";
import {urlApi} from "../../constant.js";

export const listTitle = () => async (dispatch) => {
    try {
        dispatch({type: TITLE_LIST_REQUEST});
        const {data} = await axios.get(urlApi + "api/titles");
        dispatch({type: TITLE_LIST_SUCCESS, payload: data});
    } catch (error) {
        dispatch({
            type: TITLE_LIST_FAIL,
            payload: error.response && error.response.data.message ? error.response.data.message : error.message,
        });
    }
};