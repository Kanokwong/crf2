import axios from "axios";
import {
    NATIONALITY_LIST_FAIL,
    NATIONALITY_LIST_REQUEST,
    NATIONALITY_LIST_SUCCESS
} from "../constants/nationConstants";
import {urlApi} from "../../constant.js";

export const listNationality = () => async (dispatch) => {
    try {
        dispatch({type: NATIONALITY_LIST_REQUEST});
        const {data} = await axios.get(urlApi + "api/nations");
        dispatch({type: NATIONALITY_LIST_SUCCESS, payload: data});
    } catch (error) {
        dispatch({
            type: NATIONALITY_LIST_FAIL,
            payload: error.response && error.response.data.message ? error.response.data.message : error.message,
        });
    }
};