import axios from "axios";
import {
    USER_DETAILS_FAIL,
    USER_DETAILS_REQUEST, USER_DETAILS_RESET, USER_DETAILS_SUCCESS,
    USER_LOGIN_FAIL,
    USER_LOGIN_REQUEST,
    USER_LOGIN_SUCCESS,
    USER_LOGOUT, USER_REGISTER_FAIL,
    USER_REGISTER_REQUEST, USER_REGISTER_SUCCESS, USER_UPDATE_FAIL, USER_UPDATE_REQUEST, USER_UPDATE_SUCCESS
} from "../constants/userConstants";
import {urlApi} from "../../constant.js";

// Login
export const login = (username, password) => async (dispatch) => {
    try {
        dispatch({type: USER_LOGIN_REQUEST});

        const config = {
            headers: {
                "Content-Type": "application/json",
            },
        }
        const {data} = await axios.post(urlApi + `api/users/login`, {username, password}, config);
        dispatch({type: USER_LOGIN_SUCCESS, payload: data});

        localStorage.setItem("userInfo", JSON.stringify(data));
    } catch (error) {
        dispatch({
            type: USER_LOGIN_FAIL,
            payload: error.response && error.response.data.message ? error.response.data.message : error.message,
        });
    }
};

// Logout
export const logout = () => async (dispatch) => {
    localStorage.removeItem("userInfo");
    dispatch({type: USER_LOGOUT});
    dispatch({type: USER_DETAILS_RESET});
    document.location.href = "/login";
}

// Register
export const register = (hospital, title, firstName, lastName, email, username, password, confirmPassword) => async (dispatch) => {
    try {
        dispatch({type: USER_REGISTER_REQUEST});

        const config = {
            headers: {
                "Content-Type": "application/json",
            },
        }
        if (password === confirmPassword) {
            const {data} = await axios.post(`api/users/register`, {
                hospital,
                title,
                firstName,
                lastName,
                email,
                username,
                password,
                confirmPassword
            }, config);
            dispatch({type: USER_REGISTER_SUCCESS, payload: data});
            dispatch({type: USER_LOGIN_SUCCESS, payload: data});
            localStorage.setItem("userInfo", JSON.stringify(data));
        }


    } catch (error) {
        dispatch({
            type: USER_REGISTER_FAIL,
            payload: error.response && error.response.data.message ? error.response.data.message : error.message,
        });
    }
};

// User Details
export const userDetails = (id) => async (dispatch, getState) => {
    try {
        dispatch({type: USER_DETAILS_REQUEST});

        const {userLogin: {userInfo},} = getState();

        const config = {
            headers: {
                Authorization: `Bearer ${userInfo.token}`
            },
        }
        const {data} = await axios.get(`api/users/${id}`, config);
        dispatch({type: USER_DETAILS_SUCCESS, payload: data});
    } catch (error) {
        const message = error.response && error.response.data.message ? error.response.data.message : error.message;

        if (message === "Not authorized, token failed") {
            dispatch(logout())
        }
        dispatch({
            type: USER_DETAILS_FAIL,
            payload: message,
        });
    }
};

// User Update Profile
export const userUpdateProfile = (user) => async (dispatch, getState) => {
    try {
        dispatch({type: USER_UPDATE_REQUEST});

        const {userLogin: {userInfo},} = getState();

        const config = {
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${userInfo.token}`
            },
        }
        const {data} = await axios.put(`api/users/profile`, user, config);
        dispatch({type: USER_UPDATE_SUCCESS, payload: data});

        localStorage.setItem("userInfo", JSON.stringify(data))
    } catch (error) {
        const message = error.response && error.response.data.message ? error.response.data.message : error.message;

        if (message === "Not authorized, token failed") {
            dispatch(logout())
        }
        dispatch({
            type: USER_UPDATE_FAIL,
            payload: message,
        });
    }
};