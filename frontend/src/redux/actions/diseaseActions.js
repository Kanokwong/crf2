import axios from "axios";
import {DISEASE_LIST_FAIL, DISEASE_LIST_REQUEST, DISEASE_LIST_SUCCESS} from "../constants/diseaseConstants";
import {urlApi} from "../../constant.js";

export const listDisease = () => async (dispatch) => {
    try {
        dispatch({type: DISEASE_LIST_REQUEST});
        const {data} = await axios.get(urlApi + "api/diseases");
        dispatch({type: DISEASE_LIST_SUCCESS, payload: data});
    } catch (error) {
        dispatch({
            type: DISEASE_LIST_FAIL,
            payload: error.response && error.response.data.message ? error.response.data.message : error.message,
        });
    }
};