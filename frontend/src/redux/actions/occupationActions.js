import {OCCUPATION_LIST_FAIL, OCCUPATION_LIST_REQUEST, OCCUPATION_LIST_SUCCESS} from "../constants/occupationConstants";
import axios from "axios";
import {urlApi} from "../../constant.js";

export const listOccupation = () => async (dispatch) => {
    try {
        dispatch({type: OCCUPATION_LIST_REQUEST});
        const {data} = await axios.get(urlApi + "api/occupations");
        dispatch({type: OCCUPATION_LIST_SUCCESS, payload: data});
    } catch (error) {
        dispatch({
            type: OCCUPATION_LIST_FAIL,
            payload: error.response && error.response.data.message ? error.response.data.message : error.message,
        });
    }
};