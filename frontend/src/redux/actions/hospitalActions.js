import {HOSPITAL_LIST_FAIL, HOSPITAL_LIST_REQUEST, HOSPITAL_LIST_SUCCESS} from "../constants/hospitalConstants";
import axios from "axios";
import {urlApi} from "../../constant.js";

export const listHospital = () => async (dispatch) => {
    try {
        dispatch({type: HOSPITAL_LIST_REQUEST});
        const  { data } = await axios.get(urlApi + "api/hospitals");
        dispatch({type: HOSPITAL_LIST_SUCCESS, payload: data});
    }catch (error) {
        dispatch({
            type: HOSPITAL_LIST_FAIL,
            payload: error.response && error.response.data.message ? error.response.data.message : error.message,
        });
    }
};