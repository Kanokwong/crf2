import axios from "axios";
import {PROVINCE_LIST_FAIL, PROVINCE_LIST_REQUEST, PROVINCE_LIST_SUCCESS} from "../constants/provinceConstants";
import {urlApi} from "../../constant.js";

export const listProvince = () => async (dispatch) => {
    try {
        dispatch({type: PROVINCE_LIST_REQUEST});
        const {data} = await axios.get(urlApi + "api/provinces");
        dispatch({type: PROVINCE_LIST_SUCCESS, payload: data});
    } catch (error) {
        dispatch({
            type: PROVINCE_LIST_FAIL,
            payload: error.response && error.response.data.message ? error.response.data.message : error.message,
        });
    }
};