import {createStore, combineReducers, applyMiddleware} from "redux";
import thunk from "redux-thunk";
import {composeWithDevTools} from "redux-devtools-extension";
import {hospitalReducers} from "./reducers/hospitalReducers";
import {userDetailsReducer, userLoginReducer, userRegisterReducer} from "./reducers/userReducers";
import {titleReducers} from "./reducers/titleReducers";
import {provinceReducers} from "./reducers/provinceReducers";
import {nationReducers} from "./reducers/nationReducers";
import {occupationReducers} from "./reducers/occupationReducers";
import {marriageReducers} from "./reducers/marriageReducers";
import {educationReducers} from "./reducers/educationReducer";
import {diseaseReducers} from "./reducers/diseaseReducer";
import {userUpdateProfile} from "./actions/userActions";

const reducer = combineReducers({
    hospitalList: hospitalReducers,
    userLogin: userLoginReducer,
    userRegister: userRegisterReducer,
    userDetails: userDetailsReducer,
    userUpdateProfile: userUpdateProfile,
    titleList: titleReducers,
    provinceList: provinceReducers,
    nationList: nationReducers,
    occupationList: occupationReducers,
    marriageList: marriageReducers,
    educationList: educationReducers,
    diseaseList: diseaseReducers,
});

// Login
const userInfoLocalStorage = localStorage.getItem("userInfo") ? JSON.parse(localStorage.getItem("userInfo")) : null;

const initialState = {
    userLogin: {
        userInfo: userInfoLocalStorage
    },
}

const middleware = [thunk]

const store = createStore(
    reducer,
    initialState,
    composeWithDevTools(applyMiddleware(...middleware))
)

export default store;