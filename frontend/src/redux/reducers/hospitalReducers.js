import {HOSPITAL_LIST_FAIL, HOSPITAL_LIST_REQUEST, HOSPITAL_LIST_SUCCESS} from "../constants/hospitalConstants";

export const hospitalReducers = (state = {hospitals: []}, action) => {
    switch (action.type) {
        case HOSPITAL_LIST_REQUEST:
            return {loading: true, hospitals: []};
        case HOSPITAL_LIST_SUCCESS:
            return {loading: false, hospitals: action.payload}
        case HOSPITAL_LIST_FAIL:
            return {loading: false, error: action.payload}
        default:
            return state;
    }
}