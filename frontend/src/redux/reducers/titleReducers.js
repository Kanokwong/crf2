import {TITLE_LIST_FAIL, TITLE_LIST_REQUEST, TITLE_LIST_SUCCESS} from "../constants/titleConstants";

export const titleReducers = (state = {titles: []}, action) => {
    switch (action.type) {
        case TITLE_LIST_REQUEST:
            return {loading: true, titles: []};
        case TITLE_LIST_SUCCESS:
            return {loading: false, titles: action.payload}
        case TITLE_LIST_FAIL:
            return {loading: false, error: action.payload}
        default:
            return state;
    }
}