import {
    NATIONALITY_LIST_FAIL,
    NATIONALITY_LIST_REQUEST,
    NATIONALITY_LIST_SUCCESS
} from "../constants/nationConstants";

export const nationReducers = (state = {nations: []}, action) => {
    switch (action.type) {
        case NATIONALITY_LIST_REQUEST:
            return {loading: true, nations: []};
        case NATIONALITY_LIST_SUCCESS:
            return {loading: false, nations: action.payload}
        case NATIONALITY_LIST_FAIL:
            return {loading: false, error: action.payload}
        default:
            return state;
    }
}