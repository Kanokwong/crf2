import {EDUCATION_LIST_FAIL, EDUCATION_LIST_REQUEST, EDUCATION_LIST_SUCCESS} from "../constants/educationConstants";

export const educationReducers = (state = {educations: []}, action) => {
    switch (action.type) {
        case EDUCATION_LIST_REQUEST:
            return {loading: true, educations: []};
        case EDUCATION_LIST_SUCCESS:
            return {loading: false, educations: action.payload}
        case EDUCATION_LIST_FAIL:
            return {loading: false, error: action.payload}
        default:
            return state;
    }
}