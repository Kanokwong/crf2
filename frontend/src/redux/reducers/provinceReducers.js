import {PROVINCE_LIST_FAIL, PROVINCE_LIST_REQUEST, PROVINCE_LIST_SUCCESS} from "../constants/provinceConstants";

export const provinceReducers = (state = {provinces: []}, action) => {
    switch (action.type) {
        case PROVINCE_LIST_REQUEST:
            return {loading: true, provinces: []};
        case PROVINCE_LIST_SUCCESS:
            return {loading: false, provinces: action.payload}
        case PROVINCE_LIST_FAIL:
            return {loading: false, error: action.payload}
        default:
            return state;
    }
}