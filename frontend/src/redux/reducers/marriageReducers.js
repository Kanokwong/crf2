import {MARRIAGE_LIST_FAIL, MARRIAGE_LIST_REQUEST, MARRIAGE_LIST_SUCCESS} from "../constants/marriageConstants";

export const marriageReducers = (state = {marriages: []}, action) => {
    switch (action.type) {
        case MARRIAGE_LIST_REQUEST:
            return {loading: true, marriages: []};
        case MARRIAGE_LIST_SUCCESS:
            return {loading: false, marriages: action.payload}
        case MARRIAGE_LIST_FAIL:
            return {loading: false, error: action.payload}
        default:
            return state;
    }
}