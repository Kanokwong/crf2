import {OCCUPATION_LIST_FAIL, OCCUPATION_LIST_REQUEST, OCCUPATION_LIST_SUCCESS} from "../constants/occupationConstants";

export const occupationReducers = (state = {occupations: []}, action) => {
    switch (action.type) {
        case OCCUPATION_LIST_REQUEST:
            return {loading: true, occupations: []};
        case OCCUPATION_LIST_SUCCESS:
            return {loading: false, occupations: action.payload}
        case OCCUPATION_LIST_FAIL:
            return {loading: false, error: action.payload}
        default:
            return state;
    }
}