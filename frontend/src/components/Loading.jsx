import React from "react";
import {HashLoader} from "react-spinners";

function Loading(){
    return (
        <div className="grid min-h-screen place-content-center bg-white dark:bg-gray-900">
            <HashLoader color="#3be8b0"/>
        </div>
    )

}

export default Loading;