import React, {useState} from 'react';
import {MapPinIcon} from "@heroicons/react/20/solid";

const Stepper = ({steps, children}) => {
    const [step, setStep] = useState(0);

    const handleNext = () => {
        setStep(step + 1);
    };

    const handlePrev = () => {
        setStep(step - 1);
    };

    return (
        <>
            <div>
                <div className="flex flex-auto items-center">
                    {steps.map((label, index) => (
                        index === step ? (
                            <div className="flex flex-row items-center">
                                <div className="flex-none">
                                    <MapPinIcon className="block h-8 w-8 text-blue-600" aria-hidden="true"/>
                                </div>
                                <div className="flex-1 item-center">
                                    dfdfd
                                </div>
                                <div className="flex-1">
                                    03
                                </div>
                            </div>) : (
                            <p>{label}</p>
                        )

                        // < button
                        //     key={label}
                        //     type="button"
                        //     style={{
                        //         backgroundColor: index === step ? '#333' : '#ccc',
                        //         color: index === step ? 'white' : 'black',
                        //     }}
                        //     onClick={() => setStep(index)}
                        // >
                        //     {label}
                        // </button>
                    ))}
                </div>
                <div>
                    {children[step]}
                    <button type="button" onClick={handlePrev}>
                        Prev
                    </button>
                    <button type="button" onClick={handleNext}>
                        Next
                    </button>
                </div>
            </div>
        </>

    );
};

export default Stepper;
