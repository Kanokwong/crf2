import {Fragment} from "react"
import {Disclosure, Menu, Transition} from "@headlessui/react"
import {
    Bars3Icon,
    BellIcon,
    XMarkIcon,
    ArrowSmallRightIcon
} from "@heroicons/react/24/outline"
import logo from "../logo.png"
import {useDispatch, useSelector} from "react-redux";
import {Link, useLocation} from "react-router-dom";
import {ChevronDownIcon} from "@heroicons/react/20/solid";
import {logout} from "../redux/actions/userActions";

const navigation = [
    {name: 'แบบเก็บข้อมูล', href: '#', current: true},
    {name: 'รายงาน', href: '#', current: false},
    {name: 'เกี่ยวกับ', href: '#', current: false},
    {name: 'ติดต่อเรา', href: '#', current: false},
]


function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
}

// bg-blue-600 primary
// bg-green-500 secondary
function Header() {
    const userLogin = useSelector((state) => state.userLogin);
    const {userInfo} = userLogin;
    const location = useLocation();

    const redirect = location.search ? location.search.split("=")[1] : "/";

    const dispatch = useDispatch();

    const logoutHandler = () => {
        dispatch(logout());
    }

    return (
        <header>
            <Disclosure as="nav" className="bg-blue-600">
                {({open}) => (
                    <>
                        <div className="mx-auto max-w-7xl px-2 sm:px-6 lg:px-8">
                            <div className="relative flex h-16 items-center justify-between">
                                <div className="absolute inset-y-0 left-0 flex items-center sm:hidden">
                                    {/* Mobile menu button*/}
                                    <Disclosure.Button
                                        className="inline-flex items-center justify-center rounded-md p-2 text-gray-300 hover:bg-blue-700 hover:text-white focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white">
                                        <span className="sr-only">Open main menu</span>
                                        {open ? (
                                            <XMarkIcon className="block h-6 w-6" aria-hidden="true"/>
                                        ) : (
                                            <Bars3Icon className="block h-6 w-6" aria-hidden="true"/>
                                        )}
                                    </Disclosure.Button>
                                </div>
                                <div
                                    className="flex flex-1 items-center justify-center sm:items-stretch sm:justify-start">
                                    <div className="flex flex-shrink-0 items-center">
                                        <a href="/">
                                            <img
                                                className="block h-8 w-auto lg:hidden"
                                                src={logo}
                                                alt="CRF"
                                            />
                                            <img
                                                className="hidden h-8 w-auto lg:block"
                                                src={logo}
                                                alt="CRF"
                                            />
                                        </a>
                                    </div>
                                    <div className="hidden sm:ml-6 sm:block">
                                        <div className="flex space-x-4">
                                            {navigation.map((item) => (
                                                <a
                                                    key={item.name}
                                                    href={item.href}
                                                    className={classNames(
                                                        item.current ? 'bg-blue-700 text-white' : 'text-gray-200 hover:bg-blue-700 hover:text-white',
                                                        'px-3 py-2 rounded-md text-sm font-medium'
                                                    )}
                                                    aria-current={item.current ? 'page' : undefined}
                                                >
                                                    {item.name}
                                                </a>
                                            ))}
                                        </div>
                                    </div>
                                </div>
                                <div
                                    className="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
                                    <button
                                        type="button"
                                        className="rounded-full bg-blue-700 p-1 text-gray-300 hover:text-white focus:outline-none focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-blue-700"
                                    >
                                        <span className="sr-only">View notifications</span>
                                        <BellIcon className="h-6 w-6" aria-hidden="true"/>
                                    </button>

                                    {/* Profile dropdown */}
                                    {
                                        userInfo ? (
                                                <Menu as="div" className="relative ml-3">
                                                    <div>
                                                        <Menu.Button
                                                            className="flex text-sm font-medium text-white">
                                                            <span className="sr-only">Open user menu</span>
                                                            <span>{userInfo.title.name + userInfo.firstName + " " + userInfo.lastName}</span>
                                                            <ChevronDownIcon className="h-4 w-4 self-center"
                                                                             aria-hidden="true"/>
                                                            {/*<img*/}
                                                            {/*    className="h-8 w-8 rounded-full"*/}
                                                            {/*    src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"*/}
                                                            {/*    alt=""*/}
                                                            {/*/>*/}
                                                        </Menu.Button>
                                                    </div>
                                                    <Transition
                                                        as={Fragment}
                                                        enter="transition ease-out duration-100"
                                                        enterFrom="transform opacity-0 scale-95"
                                                        enterTo="transform opacity-100 scale-100"
                                                        leave="transition ease-in duration-75"
                                                        leaveFrom="transform opacity-100 scale-100"
                                                        leaveTo="transform opacity-0 scale-95"
                                                    >
                                                        <Menu.Items
                                                            className="absolute right-0 z-10 mt-2 w-48 origin-top-right rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                                                            <Menu.Item>
                                                                {({active}) => (
                                                                    <Link to={redirect ? `/profile?redirect=${redirect}` : "/profile"}
                                                                          className={classNames(active ? 'bg-blue-100 text-blue-700' : '', 'block px-4 py-2 text-sm text-gray-700')}
                                                                    >
                                                                        ข้อมูลส่วนตัว
                                                                    </Link>
                                                                )}
                                                            </Menu.Item>
                                                            <Menu.Item>
                                                                {({active}) => (
                                                                    <Link to="#"
                                                                          className={classNames(active ? 'bg-blue-100 text-blue-700' : '', 'block px-4 py-2 text-sm text-gray-700')}
                                                                    >
                                                                        ตั้งค่า
                                                                    </Link>)}
                                                            </Menu.Item>
                                                            {
                                                                userInfo.isAdmin ?
                                                                    (
                                                                        <Menu.Item>
                                                                            {({active}) => (
                                                                                <Link to="#"
                                                                                      className={classNames(active ? 'bg-blue-100 text-blue-700' : '', 'block px-4 py-2 text-sm text-gray-700')}
                                                                                >
                                                                                    จัดการผู้ใช้งาน
                                                                                </Link>)}
                                                                        </Menu.Item>
                                                                    ) : ""

                                                            }

                                                            <Menu.Item>
                                                                {({active}) => (
                                                                    <Link to="#"
                                                                          className={classNames(active ? 'bg-blue-100 text-blue-700' : '', 'block px-4 py-2 text-sm text-gray-700')}
                                                                          onClick={logoutHandler}
                                                                    >
                                                                        ออกจากระบบ
                                                                    </Link>
                                                                )}
                                                            </Menu.Item>
                                                        </Menu.Items>
                                                    </Transition>
                                                </Menu>
                                            )
                                            :
                                            (
                                                <div>

                                                    <div className="hidden ml-3 sm:block">
                                                        <Link to="/login"
                                                              className="inline-flex items-center px-4 py-2 tracking-wide text-sm text-white transition-colors duration-300 transform bg-green-500 rounded-md hover:bg-green-600 focus:outline-none focus:bg-green-600 focus:ring focus:ring-green-500 focus:ring-opacity-50">

                                                            {/*className="inline-flex items-center rounded-md border border-transparent bg-green-500 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2">*/}
                                                            เข้าสู่ระบบ
                                                            <ArrowSmallRightIcon className="h-4 w-4 text-green-200"
                                                                                 aria-hidden="true"/>
                                                        </Link>
                                                    </div>
                                                    {/*<div className="pt-1 pb-[3px] pl-2 sm:hidden">*/}
                                                    {/*    <button*/}
                                                    {/*        type="button"*/}
                                                    {/*        className="rounded-full bg-green-700 p-1 text-gray-300 hover:text-white focus:outline-none focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-blue-700"*/}
                                                    {/*    >*/}
                                                    {/*        <span className="sr-only">เข้าสู่ระบบ</span>*/}
                                                    {/*        <ArrowRightOnRectangleIcon className="h-6 w-6" aria-hidden="true"/>*/}
                                                    {/*    </button>*/}
                                                    {/*</div>*/}

                                                </div>

                                            )

                                    }

                                </div>
                            </div>
                        </div>

                        <Disclosure.Panel className="sm:hidden">
                            <div className="space-y-1 px-2 pt-2 pb-3">
                                {navigation.map((item) => (
                                    <Disclosure.Button
                                        key={item.name}
                                        as="a"
                                        href={item.href}
                                        className={classNames(
                                            item.current ? 'bg-blue-700 text-white' : 'text-gray-200 hover:bg-blue-700 hover:text-white',
                                            'block px-3 py-2 rounded-md text-base font-medium'
                                        )}
                                        aria-current={item.current ? 'page' : undefined}
                                    >
                                        {item.name}
                                    </Disclosure.Button>
                                ))}
                                <div className="pt-3 mx-2">
                                    <div>
                                        <Link to="/login"
                                              className="w-full inline-flex items-center justify-center rounded-md border border-transparent bg-green-500 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2">
                                            เข้าสู่ระบบ
                                        </Link>
                                    </div>

                                    <div className="pt-3">
                                        <Link to={redirect ? `/login?redirect=${redirect}` : "/login"}
                                              className="w-full inline-flex items-center justify-center rounded-md border-2 border-green-500 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2">
                                            สมัครสมาชิก
                                        </Link>
                                    </div>

                                </div>
                            </div>

                        </Disclosure.Panel>

                    </>
                )}
            </Disclosure>

        </header>
    )
}

export default Header;