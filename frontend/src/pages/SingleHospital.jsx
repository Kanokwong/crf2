import axios from "axios";
import React, {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import {toast} from "react-toastify";

const SingleHospital = () => {
    const params = useParams();
    const [hospital, setHospital] = useState({});
    // const mountFlag = useRef(false);

    useEffect(() => {
        toast.dismiss();
        const fetchHospital = async () => {
            try {
                const {data} = await axios.get(`/api/hospitals/${params.id}`)
                // console.log(data)
                setHospital(data);
                toast.success(`${data.code} ${data.name}`,
                    {toastId: `${data.code}`});

            } catch (error) {
                toast.error(`${error.message}`,
                    {toastId: `${error.message}`});
            }
            // if (!mountFlag.current) {
            //     mountFlag.current = true
            //     try {
            //         const {data} = await axios.get(`/api/hospitals/${params.id}`)
            //         // console.log(data)
            //         setHospital(data);
            //         toast.success(`${data.hospitalCode} ${data.hospitalName}`);
            //
            //     } catch (error) {
            //         toast.error(`${error.message}`);
            //     }
            // }
        }
        fetchHospital();

    }, [params]);

    const [step, setStep] = useState(0);
    const handleNext = () => {
        setStep(step + 1);
    };

    const handlePrev = () => {
        setStep(step - 1);
    };

    return (
        <>
            <div>
                <p>{hospital.code}</p>
            </div>
            <div className="flex overflow-x-auto whitespace-nowrap">
                <button
                    onClick={handlePrev}
                    className="inline-flex items-center h-12 px-4 py-2 text-sm text-center text-gray-700 border border-b-0 border-gray-300 sm:text-base dark:border-gray-500 rounded-t-md dark:text-white whitespace-nowrap focus:outline-none">
                    Profile
                </button>

                <button
                    onClick={handleNext}
                    className="inline-flex items-center h-12 px-4 py-2 text-sm text-center text-gray-700 bg-transparent border-b border-gray-300 sm:text-base dark:border-gray-500 dark:text-white whitespace-nowrap cursor-base focus:outline-none hover:border-gray-400 dark:hover:border-gray-300">
                    Account
                </button>

                <button
                    className="inline-flex items-center h-12 px-4 py-2 text-sm text-center text-gray-700 bg-transparent border-b border-gray-300 sm:text-base dark:border-gray-500 dark:text-white whitespace-nowrap cursor-base focus:outline-none hover:border-gray-400 dark:hover:border-gray-300">
                    Notification
                </button>
            </div>

            {step === 0 ? (
                <p>00000000000</p>
            ) : (
                <p>11111111111</p>
            )}


            <div className="text-sm font-medium text-center text-gray-500 border-b border-gray-200 ">
                <ul className="flex flex-wrap -mb-px">
                    <li className="mr-2">
                        <a href="#"
                           className="inline-block p-4 border-b-2 border-transparent rounded-t-lg hover:text-gray-600 hover:border-gray-300 ">
                            Home
                        </a>
                    </li>
                    <li className="mr-2">
                        <a href="#"
                           className="inline-block p-4 text-purple-600 border-b-2 border-purple-600 rounded-t-lg active "
                           aria-current="page">
                            Calendar
                        </a>
                    </li>
                    <li className="mr-2">
                        <a href="#"
                           className="inline-block p-4 border-b-2 border-transparent rounded-t-lg hover:text-gray-600 hover:border-gray-300 ">
                            Results
                        </a>
                    </li>
                    <li className="mr-2">
                        <a href="#"
                           className="inline-block p-4 border-b-2 border-transparent rounded-t-lg hover:text-gray-600 hover:border-gray-300 ">
                            Live
                        </a>
                    </li>
                </ul>
            </div>

        </>


    )
}
export default SingleHospital;