import React, {Fragment, useState} from "react";
import {Combobox, Transition} from "@headlessui/react";
import {CheckIcon, ChevronUpDownIcon} from "@heroicons/react/20/solid";


const people = [
    {id: 1, name: 'Wade Cooper'},
    {id: 2, name: 'Arlene Mccoy'},
    {id: 3, name: 'Devon Webb'},
    {id: 4, name: 'Tom Cook'},
    {id: 5, name: 'Tanya Fox'},
    {id: 6, name: 'Hellen Schmidt'},
]

function TreatmentHistoryOld() {
    const [selected, setSelected] = useState(people[0])
    const [query, setQuery] = useState('')

    const filteredPeople =
        query === ''
            ? people
            : people.filter((person) =>
                person.name
                    .toLowerCase()
                    .replace(/\s+/g, '')
                    .includes(query.toLowerCase().replace(/\s+/g, ''))
            )


    return (
        <div className="my-10 sm:mt-10">
            <div className="px-4 sm:px-0">
                <h3 className="text-lg font-medium leading-6 text-gray-900">แบบเก็บข้อมูล (Case record form):
                    การศึกษาประสิทธิผลของแคนนาบินอยด์และ
                    การประเมินคุณภาพชีวิตในผู้ป่วยมะเร็งระยะแพร่กระจาย
                </h3>
                <p className="mt-1 text-md text-gray-600">ส่วนที่ 2 : ประวัติการเจ็บป่วยปัจจุบัน</p>
            </div>
            <div className="md:grid md:grid-cols-2 md:gap-6">
                <div className="mt-5 md:col-span-2 md:mt-5">
                    <form action="#" method="POST">
                        <div className="shadow sm:overflow-hidden sm:rounded-md">
                            <div className="space-y-6 bg-white px-4 py-5 sm:p-6">
                                <div className="grid grid-cols-6 gap-6">
                                    <div className="col-span-6 sm:col-span-3">
                                        <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                            การวินิจฉัย
                                        </label>
                                        <Combobox value={selected} onChange={setSelected}>
                                            <div className="relative mt-1">
                                                <div
                                                    className="relative w-full cursor-default overflow-hidden rounded-lg bg-white text-left shadow-sm border border-gray-300 text-gray-900 shadow-sm focus:outline-none focus:border-blue-500 focus:ring-1 focus:ring-blue-500 focus-visible:border-blue-500 focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 focus-visible:ring-offset-2 focus-visible:ring-offset-blue-300  sm:text-sm">
                                                    <Combobox.Input
                                                        className="w-full border-none py-2 pl-3 pr-10 text-sm leading-5 text-gray-900 focus:ring-0"
                                                        displayValue={(person) => person.name}
                                                        onChange={(event) => setQuery(event.target.value)}
                                                    />
                                                    <Combobox.Button
                                                        className="absolute inset-y-0 right-0 flex items-center pr-2">
                                                        <ChevronUpDownIcon
                                                            className="h-5 w-5 text-gray-400"
                                                            aria-hidden="true"
                                                        />
                                                    </Combobox.Button>
                                                </div>
                                                <Transition
                                                    as={Fragment}
                                                    leave="transition ease-in duration-100"
                                                    leaveFrom="opacity-100"
                                                    leaveTo="opacity-0"
                                                    afterLeave={() => setQuery('')}
                                                >
                                                    <Combobox.Options
                                                        className="absolute mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                                                        {filteredPeople.length === 0 && query !== '' ? (
                                                            <div
                                                                className="relative cursor-default select-none py-2 px-4 text-gray-700">
                                                                Nothing found.
                                                            </div>
                                                        ) : (
                                                            filteredPeople.map((person) => (
                                                                <Combobox.Option
                                                                    key={person.id}
                                                                    className={({active}) =>
                                                                        `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                                                            active ? 'bg-blue-500 text-white' : 'text-gray-900'
                                                                        }`
                                                                    }
                                                                    value={person}
                                                                >
                                                                    {({selected, active}) => (
                                                                        <>
                        <span
                            className={`block truncate ${
                                selected ? 'font-medium' : 'font-normal'
                            }`}
                        >
                          {person.name}
                        </span>
                                                                            {selected ? (
                                                                                <span
                                                                                    className={`absolute inset-y-0 left-0 flex items-center pl-3 ${
                                                                                        active ? 'text-white' : 'text-blue-500'
                                                                                    }`}
                                                                                >
                            <CheckIcon className="h-5 w-5" aria-hidden="true"/>
                          </span>
                                                                            ) : null}
                                                                        </>
                                                                    )}
                                                                </Combobox.Option>
                                                            ))
                                                        )}
                                                    </Combobox.Options>
                                                </Transition>
                                            </div>
                                        </Combobox>
                                    </div>

                                    <div className="col-span-6 sm:col-span-3">
                                        <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                            การวินิจฉัย อื่นๆ ระบุ
                                        </label>
                                        <input
                                            type="text"
                                            name="first-name"
                                            id="first-name"
                                            autoComplete="given-name"

                                            className="mt-1 block w-full rounded-md border border-gray-300 px-3 py-2 text-gray-900 shadow-sm focus:outline-none focus:border-blue-500 focus:ring-1 focus:ring-blue-500 sm:text-sm"
                                        />
                                    </div>

                                    <div className="col-span-6 sm:col-span-3">
                                        <fieldset>
                                            <label htmlFor="first-name"
                                                   className="block text-sm font-medium text-gray-700">
                                                ตำแหน่งการแพร่กระจายของมะเร็ง(ตอบได้มากกว่า 1 ข้อ)
                                            </label>
                                            <div className="md:grid md:grid-cols-2 md:gap-6">
                                                <div className="col-span-6 sm:col-span-1">
                                                    <div className="mt-4 space-y-4">
                                                        <div className="flex items-start">
                                                            <div className="flex h-5 items-center">
                                                                <input
                                                                    id="comments"
                                                                    name="comments"
                                                                    type="checkbox"
                                                                    className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500"
                                                                />
                                                            </div>
                                                            <div className="ml-3 text-sm">
                                                                <label htmlFor="comments"
                                                                       className="font-medium text-gray-700">
                                                                    กระดูก
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div className="flex items-start">
                                                            <div className="flex h-5 items-center">
                                                                <input
                                                                    id="candidates"
                                                                    name="candidates"
                                                                    type="checkbox"
                                                                    className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500"
                                                                />
                                                            </div>
                                                            <div className="ml-3 text-sm">
                                                                <label htmlFor="candidates"
                                                                       className="font-medium text-gray-700">
                                                                    ปอด
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div className="flex items-start">
                                                            <div className="flex h-5 items-center">
                                                                <input
                                                                    id="offers"
                                                                    name="offers"
                                                                    type="checkbox"
                                                                    className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500"
                                                                />
                                                            </div>
                                                            <div className="ml-3 text-sm">
                                                                <label htmlFor="offers"
                                                                       className="font-medium text-gray-700">
                                                                    ต่อมน้ำเหลือง
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div className="flex items-start">
                                                            <div className="flex h-5 items-center">
                                                                <input
                                                                    id="offers"
                                                                    name="offers"
                                                                    type="checkbox"
                                                                    className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500"
                                                                />
                                                            </div>
                                                            <div className="ml-3 text-sm">
                                                                <label htmlFor="offers"
                                                                       className="font-medium text-gray-700">
                                                                    ตับ
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-span-6 sm:col-span-1">
                                                    <div className="mt-4 space-y-4">
                                                        <div className="flex items-start">
                                                            <div className="flex h-5 items-center">
                                                                <input
                                                                    id="comments"
                                                                    name="comments"
                                                                    type="checkbox"
                                                                    className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500"
                                                                />
                                                            </div>
                                                            <div className="ml-3 text-sm">
                                                                <label htmlFor="comments"
                                                                       className="font-medium text-gray-700">
                                                                    เยื่อบุช่องท้อง
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div className="flex items-start">
                                                            <div className="flex h-5 items-center">
                                                                <input
                                                                    id="candidates"
                                                                    name="candidates"
                                                                    type="checkbox"
                                                                    className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500"
                                                                />
                                                            </div>
                                                            <div className="ml-3 text-sm">
                                                                <label htmlFor="candidates"
                                                                       className="font-medium text-gray-700">
                                                                    เยื่อหุ้มปอด
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div className="flex items-start">
                                                            <div className="flex h-5 items-center">
                                                                <input
                                                                    id="offers"
                                                                    name="offers"
                                                                    type="checkbox"
                                                                    className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500"
                                                                />
                                                            </div>
                                                            <div className="mx-3 text-sm flex items-center">
                                                                <label htmlFor="offers"
                                                                       className="font-medium text-gray-700">
                                                                    อื่นๆ
                                                                </label>
                                                            </div>
                                                            <div className="flex w-full h-5 items-center">

                                                                <input
                                                                    type="text"
                                                                    name="first-name"
                                                                    id="first-name"
                                                                    autoComplete="given-name"

                                                                    className="mt-1 block w-full rounded-md border border-gray-300 px-3 py-2 text-gray-900 shadow-sm focus:outline-none focus:border-blue-500 focus:ring-1 focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </fieldset>
                                    </div>

                                    <div className="col-span-6 sm:col-span-3">
                                        <fieldset>
                                            <label htmlFor="first-name"
                                                   className="block text-sm font-medium text-gray-700">
                                                แผนการรักษาของแพทย์(ตอบได้มากกว่า 1 ข้อ)
                                            </label>

                                            <div className="mt-4 space-y-4">
                                                <div className="flex items-start">
                                                    <div className="flex h-5 items-center">
                                                        <input
                                                            id="comments"
                                                            name="comments"
                                                            type="checkbox"
                                                            className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500"
                                                        />
                                                    </div>
                                                    <div className="ml-3 text-sm">
                                                        <label htmlFor="comments" className="font-medium text-gray-700">
                                                            การรักษาแบบประคับประคอง(Palliative care)
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="md:grid md:grid-cols-3 space-y-3">
                                                    <div className="md:col-span-2 md:mt-5">
                                                        <div className="flex items-start">
                                                            <div className="flex h-5 items-center">
                                                                <input
                                                                    id="candidates"
                                                                    name="candidates"
                                                                    type="checkbox"
                                                                    className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500"
                                                                />
                                                            </div>
                                                            <div className="mx-3 text-sm">
                                                                <label htmlFor="candidates"
                                                                       className="font-medium text-gray-700 md:whitespace-nowrap">
                                                                    เคมีบำบัดแบบประคับประคองจำนวนครั้ง
                                                                </label>
                                                            </div>
                                                            <div className="flex w-full md:h-5 h-10 items-center">
                                                                <input
                                                                    type="number"
                                                                    name="first-name"
                                                                    id="first-name"
                                                                    autoComplete="given-name"
                                                                    className="mt-1 block md:w-full rounded-md border border-gray-300 px-3 py-2 text-gray-900 shadow-sm focus:outline-none focus:border-blue-500 focus:ring-1 focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="md:col-span-1 md:mt-5">
                                                        <div className="flex items-start">
                                                            <div className="mx-3 text-sm">
                                                                <label htmlFor="candidates"
                                                                       className="font-medium text-gray-700 md:whitespace-nowrap">
                                                                    วัน/เดือน/ปี
                                                                </label>
                                                            </div>
                                                            <div className="flex w-full md:h-5 h-10 items-center">
                                                                <input
                                                                    type="number"
                                                                    name="first-name"
                                                                    id="first-name"
                                                                    autoComplete="given-name"
                                                                    className="mt-1 block w-full rounded-md border border-gray-300 px-3 py-2 text-gray-900 shadow-sm focus:outline-none focus:border-blue-500 focus:ring-1 focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                {/*<div className="md:grid md:grid-cols-2 md:gap-6 space-y-3">*/}
                                                {/*    <div className="md:col-span-2 md:mt-5 sm:col-span-1">*/}
                                                {/*        <div className="flex items-start">*/}
                                                {/*            <div className="flex h-5 items-center">*/}
                                                {/*                <input*/}
                                                {/*                    id="candidates"*/}
                                                {/*                    name="candidates"*/}
                                                {/*                    type="checkbox"*/}
                                                {/*                    className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500"*/}
                                                {/*                />*/}
                                                {/*            </div>*/}
                                                {/*            <div className="mx-3 text-sm">*/}
                                                {/*                <label htmlFor="candidates" className="font-medium text-gray-700">*/}
                                                {/*                    เคมีบำบัดแบบประคับประคองจำนวนครั้ง*/}
                                                {/*                </label>*/}
                                                {/*            </div>*/}
                                                {/*            <div className="flex h-5 items-center">*/}
                                                {/*                <input*/}
                                                {/*                    type="text"*/}
                                                {/*                    name="first-name"*/}
                                                {/*                    id="first-name"*/}
                                                {/*                    autoComplete="given-name"*/}

                                                {/*                    className="mt-1 block w-full rounded-md border border-gray-300 px-3 py-2 text-gray-900 shadow-sm focus:outline-none focus:border-blue-500 focus:ring-1 focus:ring-blue-500 sm:text-sm"*/}
                                                {/*                />*/}
                                                {/*            </div>*/}
                                                {/*        </div>*/}
                                                {/*    </div>*/}
                                                {/*    <div className="md:col-span-2 md:mt-5">*/}
                                                {/*        <div className="flex items-start">*/}
                                                {/*            <div className="flex h-5 items-center">*/}
                                                {/*                <input*/}
                                                {/*                    id="candidates"*/}
                                                {/*                    name="candidates"*/}
                                                {/*                    type="checkbox"*/}
                                                {/*                    className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500"*/}
                                                {/*                />*/}
                                                {/*            </div>*/}
                                                {/*            <div className="ml-3 text-sm">*/}
                                                {/*                <label htmlFor="candidates" className="font-medium text-gray-700">*/}
                                                {/*                    เคมีบำบัดแบบประคับประคองจำนวนครั้ง*/}
                                                {/*                </label>*/}
                                                {/*            </div>*/}
                                                {/*        </div>*/}
                                                {/*    </div>*/}
                                                {/*</div>*/}

                                                <div className="flex items-start">
                                                    <div className="flex h-5 items-center">
                                                        <input
                                                            id="offers"
                                                            name="offers"
                                                            type="checkbox"
                                                            className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500"
                                                        />
                                                    </div>
                                                    <div className="ml-3 text-sm">
                                                        <label htmlFor="offers" className="font-medium text-gray-700">
                                                            ฉายแสงแบบประคับประคองจำนวนครั้ง
                                                        </label>
                                                    </div>
                                                    <div className="flex w-full h-5 items-center">
                                                        <input
                                                            type="text"
                                                            name="first-name"
                                                            id="first-name"
                                                            autoComplete="given-name"

                                                            className="mt-1 block w-full rounded-md border border-gray-300 px-3 py-2 text-gray-900 shadow-sm focus:outline-none focus:border-blue-500 focus:ring-1 focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                                <div className="flex items-start">
                                                    <div className="flex h-5 items-center">
                                                        <input
                                                            id="offers"
                                                            name="offers"
                                                            type="checkbox"
                                                            className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500"
                                                        />
                                                    </div>
                                                    <div className="ml-3 text-sm">
                                                        <label htmlFor="offers" className="font-medium text-gray-700">
                                                            ยามะเร็งแบบมุ่งเป้า (Targeted therapy) ระบุ
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="flex items-start">
                                                    <div className="flex h-5 items-center">
                                                        <input
                                                            id="offers"
                                                            name="offers"
                                                            type="checkbox"
                                                            className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500"
                                                        />
                                                    </div>
                                                    <div className="ml-3 text-sm">
                                                        <label htmlFor="offers" className="font-medium text-gray-700">
                                                            อื่นๆ
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>


                                        </fieldset>
                                    </div>


                                    <div className="col-span-6 sm:col-span-1">
                                        {/*<div className="fixed top-16 w-72">*/}
                                        <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                            สถานะภาพสมรส
                                        </label>
                                        <Combobox value={selected} onChange={setSelected}>
                                            <div className="relative mt-1">
                                                <div
                                                    className="relative w-full cursor-default overflow-hidden rounded-lg bg-white text-left shadow-sm border border-gray-300 text-gray-900 shadow-sm focus:outline-none focus:border-blue-500 focus:ring-1 focus:ring-blue-500 focus-visible:border-blue-500 focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 focus-visible:ring-offset-2 focus-visible:ring-offset-blue-300  sm:text-sm">
                                                    <Combobox.Input
                                                        className="w-full border-none py-2 pl-3 pr-10 text-sm leading-5 text-gray-900 focus:ring-0"
                                                        displayValue={(person) => person.name}
                                                        onChange={(event) => setQuery(event.target.value)}
                                                    />
                                                    <Combobox.Button
                                                        className="absolute inset-y-0 right-0 flex items-center pr-2">
                                                        <ChevronUpDownIcon
                                                            className="h-5 w-5 text-gray-400"
                                                            aria-hidden="true"
                                                        />
                                                    </Combobox.Button>
                                                </div>
                                                <Transition
                                                    as={Fragment}
                                                    leave="transition ease-in duration-100"
                                                    leaveFrom="opacity-100"
                                                    leaveTo="opacity-0"
                                                    afterLeave={() => setQuery('')}
                                                >
                                                    <Combobox.Options
                                                        className="absolute mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                                                        {filteredPeople.length === 0 && query !== '' ? (
                                                            <div
                                                                className="relative cursor-default select-none py-2 px-4 text-gray-700">
                                                                Nothing found.
                                                            </div>
                                                        ) : (
                                                            filteredPeople.map((person) => (
                                                                <Combobox.Option
                                                                    key={person.id}
                                                                    className={({active}) =>
                                                                        `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                                                            active ? 'bg-blue-500 text-white' : 'text-gray-900'
                                                                        }`
                                                                    }
                                                                    value={person}
                                                                >
                                                                    {({selected, active}) => (
                                                                        <>
                        <span
                            className={`block truncate ${
                                selected ? 'font-medium' : 'font-normal'
                            }`}
                        >
                          {person.name}
                        </span>
                                                                            {selected ? (
                                                                                <span
                                                                                    className={`absolute inset-y-0 left-0 flex items-center pl-3 ${
                                                                                        active ? 'text-white' : 'text-blue-500'
                                                                                    }`}
                                                                                >
                            <CheckIcon className="h-5 w-5" aria-hidden="true"/>
                          </span>
                                                                            ) : null}
                                                                        </>
                                                                    )}
                                                                </Combobox.Option>
                                                            ))
                                                        )}
                                                    </Combobox.Options>
                                                </Transition>
                                            </div>
                                        </Combobox>
                                    </div>
                                    <div className="col-span-6 sm:col-span-1">
                                        {/*<div className="fixed top-16 w-72">*/}
                                        <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                            การศึกษา
                                        </label>
                                        <Combobox value={selected} onChange={setSelected}>
                                            <div className="relative mt-1">
                                                <div
                                                    className="relative w-full cursor-default overflow-hidden rounded-lg bg-white text-left shadow-sm border border-gray-300 text-gray-900 shadow-sm focus:outline-none focus:border-blue-500 focus:ring-1 focus:ring-blue-500 focus-visible:border-blue-500 focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 focus-visible:ring-offset-2 focus-visible:ring-offset-blue-300  sm:text-sm">
                                                    <Combobox.Input
                                                        className="w-full border-none py-2 pl-3 pr-10 text-sm leading-5 text-gray-900 focus:ring-0"
                                                        displayValue={(person) => person.name}
                                                        onChange={(event) => setQuery(event.target.value)}
                                                    />
                                                    <Combobox.Button
                                                        className="absolute inset-y-0 right-0 flex items-center pr-2">
                                                        <ChevronUpDownIcon
                                                            className="h-5 w-5 text-gray-400"
                                                            aria-hidden="true"
                                                        />
                                                    </Combobox.Button>
                                                </div>
                                                <Transition
                                                    as={Fragment}
                                                    leave="transition ease-in duration-100"
                                                    leaveFrom="opacity-100"
                                                    leaveTo="opacity-0"
                                                    afterLeave={() => setQuery('')}
                                                >
                                                    <Combobox.Options
                                                        className="absolute mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                                                        {filteredPeople.length === 0 && query !== '' ? (
                                                            <div
                                                                className="relative cursor-default select-none py-2 px-4 text-gray-700">
                                                                Nothing found.
                                                            </div>
                                                        ) : (
                                                            filteredPeople.map((person) => (
                                                                <Combobox.Option
                                                                    key={person.id}
                                                                    className={({active}) =>
                                                                        `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                                                            active ? 'bg-blue-500 text-white' : 'text-gray-900'
                                                                        }`
                                                                    }
                                                                    value={person}
                                                                >
                                                                    {({selected, active}) => (
                                                                        <>
                        <span
                            className={`block truncate ${
                                selected ? 'font-medium' : 'font-normal'
                            }`}
                        >
                          {person.name}
                        </span>
                                                                            {selected ? (
                                                                                <span
                                                                                    className={`absolute inset-y-0 left-0 flex items-center pl-3 ${
                                                                                        active ? 'text-white' : 'text-blue-500'
                                                                                    }`}
                                                                                >
                            <CheckIcon className="h-5 w-5" aria-hidden="true"/>
                          </span>
                                                                            ) : null}
                                                                        </>
                                                                    )}
                                                                </Combobox.Option>
                                                            ))
                                                        )}
                                                    </Combobox.Options>
                                                </Transition>
                                            </div>
                                        </Combobox>
                                    </div>

                                    <div className="col-span-6 sm:col-span-2">
                                        <fieldset>
                                            <legend
                                                className="contents text-sm font-medium text-gray-700">ประวัติการดื่มสุรา
                                            </legend>
                                            <div className="mt-2 space-x-8 flex items-center">
                                                <div className="flex items-center">
                                                    <input
                                                        id="push-everything"
                                                        name="push-notifications"
                                                        type="radio"
                                                        className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    />
                                                    <label htmlFor="push-everything"
                                                           className="ml-3 block text-sm font-medium text-gray-700">
                                                        ดื่มสุรา
                                                    </label>
                                                </div>
                                                <div className="flex items-center">
                                                    <input
                                                        id="push-email"
                                                        name="push-notifications"
                                                        type="radio"
                                                        className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    />
                                                    <label htmlFor="push-email"
                                                           className="ml-3 block text-sm font-medium text-gray-700">
                                                        ไม่ดื่มสุรา
                                                    </label>
                                                </div>

                                                <div className="flex items-center">
                                                    <input
                                                        id="push-email"
                                                        name="push-notifications"
                                                        type="radio"
                                                        className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    />
                                                    <label htmlFor="push-email"
                                                           className="ml-3 block text-sm font-medium text-gray-700">
                                                        เคยดื่มสุรา เลิกดื่มแล้ว
                                                    </label>
                                                </div>

                                            </div>
                                        </fieldset>


                                    </div>
                                    <div className="col-span-6 sm:col-span-1">
                                        <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                            &nbsp;
                                        </label>
                                        <div className="mt-1 flex rounded-md shadow-sm">
                                            <input
                                                type="text"
                                                name="company-website"
                                                id="company-website"
                                                className="block w-full flex-1 rounded-none rounded-l-md border border-gray-300 px-3 py-2 text-gray-900 shadow-sm focus:outline-none focus:border-blue-500 focus:ring-1 focus:ring-blue-500 sm:text-sm"
                                            />
                                            <span
                                                className="inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 px-3 text-sm text-gray-500">
                          ปี
                        </span>
                                        </div>
                                    </div>
                                    <div className="col-span-6 sm:col-span-2">
                                        <fieldset>
                                            <legend
                                                className="contents text-sm font-medium text-gray-700">ประวัติการสูบบุหรี่
                                            </legend>
                                            <div className="mt-2 space-x-4 flex items-center">
                                                <div className="flex items-center">
                                                    <input
                                                        id="push-everything"
                                                        name="push-notifications"
                                                        type="radio"
                                                        className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    />
                                                    <label htmlFor="push-everything"
                                                           className="ml-3 block text-sm font-medium text-gray-700">
                                                        สูบบุหรี่
                                                    </label>
                                                </div>
                                                <div className="flex items-center">
                                                    <input
                                                        id="push-email"
                                                        name="push-notifications"
                                                        type="radio"
                                                        className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    />
                                                    <label htmlFor="push-email"
                                                           className="ml-3 block text-sm font-medium text-gray-700">
                                                        ไม่สูบบุหรี่
                                                    </label>
                                                </div>

                                                <div className="flex items-center">
                                                    <input
                                                        id="push-email"
                                                        name="push-notifications"
                                                        type="radio"
                                                        className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    />
                                                    <label htmlFor="push-email"
                                                           className="ml-3 block text-sm font-medium text-gray-700">
                                                        เคยสูบบุหรี่ เลิกสูบบุหรี่แล้ว
                                                    </label>
                                                </div>

                                            </div>
                                        </fieldset>


                                    </div>
                                    <div className="col-span-6 sm:col-span-1">
                                        <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                            &nbsp;
                                        </label>
                                        <div className="mt-1 flex rounded-md shadow-sm">
                                            <input
                                                type="text"
                                                name="company-website"
                                                id="company-website"
                                                className="block w-full flex-1 rounded-none rounded-l-md border border-gray-300 px-3 py-2 text-gray-900 shadow-sm focus:outline-none focus:border-blue-500 focus:ring-1 focus:ring-blue-500 sm:text-sm"
                                            />
                                            <span
                                                className="inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 px-3 text-sm text-gray-500">
                          ปี
                        </span>
                                        </div>
                                    </div>
                                    <div className="col-span-6 mt-4">
                                        <h3 className="text-lg font-medium leading-6 text-gray-900">ประวัติการใช้กัญชา/สารสกัดจากกัญชา</h3>
                                    </div>
                                    <div className="col-span-6 sm:col-span-2">
                                        <fieldset>
                                            <legend className="contents text-sm font-medium text-gray-700">เคยใช้กัญชา
                                            </legend>
                                            <div className="mt-2 space-x-12 flex items-center">
                                                <div className="flex items-center">
                                                    <input
                                                        id="push-everything"
                                                        name="push-notifications"
                                                        type="radio"
                                                        className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    />
                                                    <label htmlFor="push-everything"
                                                           className="ml-3 block text-sm font-medium text-gray-700">
                                                        เคย
                                                    </label>
                                                </div>
                                                <div className="flex items-center">
                                                    <input
                                                        id="push-email"
                                                        name="push-notifications"
                                                        type="radio"
                                                        className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    />
                                                    <label htmlFor="push-email"
                                                           className="ml-3 block text-sm font-medium text-gray-700">
                                                        ไม่เคย
                                                    </label>
                                                </div>

                                            </div>
                                        </fieldset>
                                    </div>
                                    <div className="col-span-6 sm:col-span-2">
                                        <fieldset>
                                            <legend className="contents text-sm font-medium text-gray-700">1.
                                                เหตุผลของการใช้กัญชา
                                            </legend>
                                            <div className="mt-2 space-x-4 flex items-center">
                                                <div className="flex items-center">
                                                    <input
                                                        id="push-everything"
                                                        name="push-notifications"
                                                        type="radio"
                                                        className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    />
                                                    <label htmlFor="push-everything"
                                                           className="ml-3 block text-sm font-medium text-gray-700">
                                                        เพื่อรักษาโรค
                                                    </label>
                                                </div>
                                                <div className="flex items-center">
                                                    <input
                                                        id="push-email"
                                                        name="push-notifications"
                                                        type="radio"
                                                        className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    />
                                                    <label htmlFor="push-email"
                                                           className="ml-3 block text-sm font-medium text-gray-700">
                                                        เพื่อสันทนาการ
                                                    </label>
                                                </div>

                                                <div className="flex items-center">
                                                    <input
                                                        id="push-email"
                                                        name="push-notifications"
                                                        type="radio"
                                                        className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    />
                                                    <label htmlFor="push-email"
                                                           className="ml-3 block text-sm font-medium text-gray-700">
                                                        อื่นๆ ระบุ
                                                    </label>
                                                </div>

                                            </div>
                                        </fieldset>


                                    </div>
                                    <div className="col-span-6 sm:col-span-2">
                                        <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                            &nbsp;
                                        </label>
                                        <input
                                            type="text"
                                            name="first-name"
                                            id="first-name"
                                            autoComplete="given-name"

                                            className="mt-1 block w-full rounded-md border border-gray-300 px-3 py-2 text-gray-900 shadow-sm focus:outline-none focus:border-blue-500 focus:ring-1 focus:ring-blue-500 sm:text-sm"
                                        />
                                    </div>
                                    <div className="col-span-6 sm:col-span-2">
                                        <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                            2. วัน เวลา ที่ใช้ครั้งสุดท้าย
                                        </label>
                                        <input
                                            type="text"
                                            name="first-name"
                                            id="first-name"
                                            autoComplete="given-name"
                                            placeholder="วัน/เดือน/ปี พ.ศ."
                                            className="mt-1 block w-full rounded-md border border-gray-300 px-3 py-2 text-gray-900 shadow-sm focus:outline-none focus:border-blue-500 focus:ring-1 focus:ring-blue-500 sm:text-sm"
                                        />
                                    </div>
                                    <div className="col-span-6 sm:col-span-2">
                                        <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                            เวลาโดยประมาณ
                                        </label>
                                        <input
                                            type="text"
                                            name="first-name"
                                            id="first-name"
                                            autoComplete="given-name"
                                            placeholder="00:00"
                                            className="mt-1 block w-full rounded-md border border-gray-300 px-3 py-2 text-gray-900 shadow-sm focus:outline-none focus:border-blue-500 focus:ring-1 focus:ring-blue-500 sm:text-sm"
                                        />
                                    </div>
                                    <div className="col-span-6 sm:col-span-2">
                                        <fieldset>
                                            <legend className="contents text-sm font-medium text-gray-700">3.
                                                เคยได้รับผลข้างเคียงจากกัญชา
                                            </legend>
                                            <div className="mt-2 space-x-12 flex items-center">
                                                <div className="flex items-center">
                                                    <input
                                                        id="push-everything"
                                                        name="push-notifications"
                                                        type="radio"
                                                        className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    />
                                                    <label htmlFor="push-everything"
                                                           className="ml-3 block text-sm font-medium text-gray-700">
                                                        เคย
                                                    </label>
                                                </div>
                                                <div className="flex items-center">
                                                    <input
                                                        id="push-email"
                                                        name="push-notifications"
                                                        type="radio"
                                                        className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    />
                                                    <label htmlFor="push-email"
                                                           className="ml-3 block text-sm font-medium text-gray-700">
                                                        ไม่เคย
                                                    </label>
                                                </div>

                                            </div>
                                        </fieldset>
                                    </div>
                                    <div className="col-span-6 sm:col-span-2">
                                        <fieldset>
                                            <legend className="contents text-sm font-medium text-gray-700">4.
                                                รูปแบบของกัญชาที่ใช้
                                            </legend>
                                            <div className="mt-2 space-x-4 flex items-center">
                                                <div className="flex items-center">
                                                    <input
                                                        id="push-everything"
                                                        name="push-notifications"
                                                        type="radio"
                                                        className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    />
                                                    <label htmlFor="push-everything"
                                                           className="ml-3 block text-sm font-medium text-gray-700">
                                                        กัญชาสด
                                                    </label>
                                                </div>
                                                <div className="flex items-center">
                                                    <input
                                                        id="push-email"
                                                        name="push-notifications"
                                                        type="radio"
                                                        className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    />
                                                    <label htmlFor="push-email"
                                                           className="ml-3 block text-sm font-medium text-gray-700">
                                                        กัญชาแห้ง
                                                    </label>
                                                </div>

                                                <div className="flex items-center">
                                                    <input
                                                        id="push-email"
                                                        name="push-notifications"
                                                        type="radio"
                                                        className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    />
                                                    <label htmlFor="push-email"
                                                           className="ml-3 block text-sm font-medium text-gray-700">
                                                        ผลิตภัณฑ์กัญชา ระบุ
                                                    </label>
                                                </div>

                                            </div>
                                        </fieldset>


                                    </div>
                                    <div className="col-span-6 sm:col-span-2">
                                        <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                            &nbsp;
                                        </label>
                                        <input
                                            type="text"
                                            name="first-name"
                                            id="first-name"
                                            autoComplete="given-name"

                                            className="mt-1 block w-full rounded-md border border-gray-300 px-3 py-2 text-gray-900 shadow-sm focus:outline-none focus:border-blue-500 focus:ring-1 focus:ring-blue-500 sm:text-sm"
                                        />
                                    </div>
                                    <div className="col-span-6 sm:col-span-2">
                                        <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                            แหล่งที่ได้มาของผลิตภัณฑ์กัญชา
                                        </label>
                                        <input
                                            type="text"
                                            name="first-name"
                                            id="first-name"
                                            autoComplete="given-name"

                                            className="mt-1 block w-full rounded-md border border-gray-300 px-3 py-2 text-gray-900 shadow-sm focus:outline-none focus:border-blue-500 focus:ring-1 focus:ring-blue-500 sm:text-sm"
                                        />
                                    </div>

                                    <div className="col-span-6 sm:col-span-3">
                                        <fieldset>
                                            <legend
                                                className="contents text-sm font-medium text-gray-700">ประวัติการใช้สารเสพติดอื่น
                                                เช่น ยาบ้า กระท่อม เฮโรอีน
                                            </legend>
                                            <div className="mt-2 space-x-16 flex items-center">
                                                <div className="flex items-center">
                                                    <input
                                                        id="push-everything"
                                                        name="push-notifications"
                                                        type="radio"
                                                        className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    />
                                                    <label htmlFor="push-everything"
                                                           className="ml-3 block text-sm font-medium text-gray-700">
                                                        ไม่เคยใช้
                                                    </label>
                                                </div>
                                                <div className="flex items-center">
                                                    <input
                                                        id="push-email"
                                                        name="push-notifications"
                                                        type="radio"
                                                        className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    />
                                                    <label htmlFor="push-email"
                                                           className="ml-3 block text-sm font-medium text-gray-700">
                                                        เคยใช้
                                                    </label>
                                                </div>

                                                <div className="flex items-center">
                                                    <input
                                                        id="push-email"
                                                        name="push-notifications"
                                                        type="radio"
                                                        className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    />
                                                    <label htmlFor="push-email"
                                                           className="ml-3 block text-sm font-medium text-gray-700">
                                                        ปัจจุบันยังใช้อยู่
                                                    </label>
                                                </div>

                                                <div className="flex items-center">
                                                    <input
                                                        id="push-email"
                                                        name="push-notifications"
                                                        type="radio"
                                                        className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    />
                                                    <label htmlFor="push-email"
                                                           className="ml-3 block text-sm font-medium text-gray-700">
                                                        เลิกใช้แล้ว
                                                    </label>
                                                </div>

                                            </div>
                                        </fieldset>


                                    </div>
                                    <div className="col-span-6 sm:col-span-1">
                                        <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                            &nbsp;
                                        </label>
                                        <div className="mt-1 flex rounded-md shadow-sm">
                                            <input
                                                type="text"
                                                name="company-website"
                                                id="company-website"
                                                className="block w-full flex-1 rounded-none rounded-l-md border border-gray-300 px-3 py-2 text-gray-900 shadow-sm focus:outline-none focus:border-blue-500 focus:ring-1 focus:ring-blue-500 sm:text-sm"
                                            />
                                            <span
                                                className="inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 px-3 text-sm text-gray-500">
                          เดือน
                        </span>
                                        </div>
                                    </div>
                                    <div className="col-span-6 sm:col-span-2">


                                    </div>
                                    <div className="col-span-6 sm:col-span-2">
                                        <fieldset>
                                            <legend
                                                className="contents text-sm font-medium text-gray-700">ประวัติแพ้ยา
                                            </legend>
                                            <div className="mt-2 space-x-20 flex items-center">
                                                <div className="flex items-center">
                                                    <input
                                                        id="push-everything"
                                                        name="push-notifications"
                                                        type="radio"
                                                        className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    />
                                                    <label htmlFor="push-everything"
                                                           className="ml-3 block text-sm font-medium text-gray-700">
                                                        ไม่เคยแพ้
                                                    </label>
                                                </div>
                                                <div className="flex items-center">
                                                    <input
                                                        id="push-email"
                                                        name="push-notifications"
                                                        type="radio"
                                                        className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    />
                                                    <label htmlFor="push-email"
                                                           className="ml-3 block text-sm font-medium text-gray-700">
                                                        เคยแพ้ยา ระบุ
                                                    </label>
                                                </div>


                                            </div>
                                        </fieldset>


                                    </div>
                                    <div className="col-span-6 sm:col-span-2">
                                        <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                            &nbsp;
                                        </label>
                                        <input
                                            type="text"
                                            name="first-name"
                                            id="first-name"
                                            autoComplete="given-name"

                                            className="mt-1 block w-full rounded-md border border-gray-300 px-3 py-2 text-gray-900 shadow-sm focus:outline-none focus:border-blue-500 focus:ring-1 focus:ring-blue-500 sm:text-sm"
                                        />
                                    </div>

                                    <div className="col-span-6 sm:col-span-2">


                                    </div>
                                    <div className="col-span-6 sm:col-span-2">
                                        {/*<div className="fixed top-16 w-72">*/}
                                        <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                            โรคประจำตัว
                                        </label>
                                        <Combobox value={selected} onChange={setSelected}>
                                            <div className="relative mt-1">
                                                <div
                                                    className="relative w-full cursor-default overflow-hidden rounded-lg bg-white text-left shadow-sm border border-gray-300 text-gray-900 shadow-sm focus:outline-none focus:border-blue-500 focus:ring-1 focus:ring-blue-500 focus-visible:border-blue-500 focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 focus-visible:ring-offset-2 focus-visible:ring-offset-blue-300  sm:text-sm">
                                                    <Combobox.Input
                                                        className="w-full border-none py-2 pl-3 pr-10 text-sm leading-5 text-gray-900 focus:ring-0"
                                                        displayValue={(person) => person.name}
                                                        onChange={(event) => setQuery(event.target.value)}
                                                    />
                                                    <Combobox.Button
                                                        className="absolute inset-y-0 right-0 flex items-center pr-2">
                                                        <ChevronUpDownIcon
                                                            className="h-5 w-5 text-gray-400"
                                                            aria-hidden="true"
                                                        />
                                                    </Combobox.Button>
                                                </div>
                                                <Transition
                                                    as={Fragment}
                                                    leave="transition ease-in duration-100"
                                                    leaveFrom="opacity-100"
                                                    leaveTo="opacity-0"
                                                    afterLeave={() => setQuery('')}
                                                >
                                                    <Combobox.Options
                                                        className="absolute mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                                                        {filteredPeople.length === 0 && query !== '' ? (
                                                            <div
                                                                className="relative cursor-default select-none py-2 px-4 text-gray-700">
                                                                Nothing found.
                                                            </div>
                                                        ) : (
                                                            filteredPeople.map((person) => (
                                                                <Combobox.Option
                                                                    key={person.id}
                                                                    className={({active}) =>
                                                                        `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                                                            active ? 'bg-blue-500 text-white' : 'text-gray-900'
                                                                        }`
                                                                    }
                                                                    value={person}
                                                                >
                                                                    {({selected, active}) => (
                                                                        <>
                        <span
                            className={`block truncate ${
                                selected ? 'font-medium' : 'font-normal'
                            }`}
                        >
                          {person.name}
                        </span>
                                                                            {selected ? (
                                                                                <span
                                                                                    className={`absolute inset-y-0 left-0 flex items-center pl-3 ${
                                                                                        active ? 'text-white' : 'text-blue-500'
                                                                                    }`}
                                                                                >
                            <CheckIcon className="h-5 w-5" aria-hidden="true"/>
                          </span>
                                                                            ) : null}
                                                                        </>
                                                                    )}
                                                                </Combobox.Option>
                                                            ))
                                                        )}
                                                    </Combobox.Options>
                                                </Transition>
                                            </div>
                                        </Combobox>
                                    </div>

                                    <div className="col-span-6 sm:col-span-2">
                                        <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                            โรคประจำตัว อื่นๆ ระบุ
                                        </label>
                                        <input
                                            type="text"
                                            name="first-name"
                                            id="first-name"
                                            autoComplete="given-name"

                                            className="mt-1 block w-full rounded-md border border-gray-300 px-3 py-2 text-gray-900 shadow-sm focus:outline-none focus:border-blue-500 focus:ring-1 focus:ring-blue-500 sm:text-sm"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="bg-gray-50 px-4 py-3 text-right sm:px-6">
                                <button
                                    type="submit"
                                    className="inline-flex justify-center rounded-md border border-transparent bg-blue-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2"
                                >
                                    บันทึกข้อมูล
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default TreatmentHistoryOld;