import {toast} from "react-toastify";
import {CheckIcon, ChevronUpDownIcon, ExclamationCircleIcon} from "@heroicons/react/20/solid";
import {Combobox, Transition} from "@headlessui/react";
import React, {Fragment, useEffect, useState} from "react";
import {useLocation, useNavigate} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {listHospital} from "../redux/actions/hospitalActions";
import {listTitle} from "../redux/actions/titleActions";
import moment from "moment"
import 'moment/locale/th'

moment.locale('th')

function Profile() {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [username, setUserName] = useState("");
    const [password, setPassword] = useState("");
    const [newPassword, setNewPassword] = useState("");

    const [isCheck, setIsCheck] = useState(false);
    const hospitalList = useSelector((state) => state.hospitalList);
    const {error: errorHospitals, hospitals} = hospitalList;
    const titleList = useSelector((state) => state.titleList)
    const {error: errorTitles, titles} = titleList;

    const dispatch = useDispatch();
    const navigate = useNavigate();
    const location = useLocation();

    const redirect = location.search ? location.search.split("=")[1] : "/";

    const userLogin = useSelector((state) => state.userLogin);
    const {error, loading, userInfo} = userLogin;

    const userUpdateProfile = useSelector((state) => state.userUpdateProfile);
    const {loading: updateLoading} = userUpdateProfile;

    const [selectedHospital, setSelectedHospital] = useState(hospitals);
    const [queryHospital, setQueryHospital] = useState("");
    const [selectedTitle, setSelectedTitle] = useState(titles);
    const [queryTitle, setQueryTitle] = useState("");

    // console.log(selectedTitle)

    const filteredHospital =
        queryHospital === ''
            ? hospitals
            : hospitals.filter((hospital) =>
                hospital.name
                    .toLowerCase()
                    .replace(/\s+/g, '')
                    .includes(queryHospital.toLowerCase().replace(/\s+/g, ''))
            )

    const filteredTitle =
        queryTitle === ''
            ? titles
            : titles.filter((title) =>
                title.name
                    .toLowerCase()
                    .replace(/\s+/g, '')
                    .includes(queryTitle.toLowerCase().replace(/\s+/g, ''))
            )

    useEffect(() => {
        dispatch(listHospital());
        dispatch(listTitle());

        if (userInfo) {
            const hospital = {
                code: userInfo.hospital.code,
                name: userInfo.hospital.name
            }

            const title =
                {
                    code: userInfo.title.code,
                    name: userInfo.title.name
                }

            setSelectedHospital(hospital)
            setSelectedTitle(title)
            setFirstName(userInfo.firstName)
            setLastName(userInfo.lastName)
            setEmail(userInfo.email)
            setUserName(userInfo.username)

        }

    }, [dispatch, userInfo])

    const toastId = "";
    const submitHandler = (e) => {
        e.preventDefault();
        if (isCheck) {
            if (password !== newPassword) {

                if (!toast.isActive(toastId.current)) {
                    toastId.current = toast.error("รหัสผ่านไม่ตรงกัน",
                        {toastId: "รหัสผ่านไม่ตรงกัน"});
                }

            } else {
                dispatch(userUpdateProfile({
                    id: userInfo._id,
                    selectedHospital,
                    selectedTitle,
                    firstName,
                    lastName,
                    email,
                    username,
                    password
                }))

                if (!toast.isActive(toastId.current)) {
                    toastId.current = toast.success("เปลี่ยนรหัสผ่านใหม่สำเร็จ",
                        {toastId: "เปลี่ยนรหัสผ่านใหม่สำเร็จ"});
                }
            }
        } else {
            dispatch(userUpdateProfile({
                id: userInfo._id,
                selectedHospital,
                selectedTitle,
                firstName,
                lastName,
                email,
                username
            }))

            if (!toast.isActive(toastId.current)) {
                toastId.current = toast.success("ปรับปรุงข้อมูลส่วนตัวสำเร็จ",
                    {toastId: "ปรับปรุงข้อมูลส่วนตัวสำเร็จ"});
            }

        }
    }

    const checkedHandler = () => {
        setIsCheck(!isCheck);
    }

    return (
        <>
            {errorHospitals && toast.error(`${errorHospitals}`, {toastId: ""})}
            {errorTitles && toast.error(`${errorTitles}`, {toastId: ""})}
            <section className="bg-white dark:bg-gray-900">
                <div className="flex justify-center min-h-screen">
                    <div
                        className="hidden bg-cover lg:block lg:w-2/5 bg-[url('https://images.unsplash.com/photo-1494621930069-4fd4b2e24a11?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=715&q=80')]">
                    </div>

                    <div className="flex items-center w-full max-w-3xl p-8 mx-auto lg:px-12 lg:w-3/5">
                        <div className="w-full">

                            <h1 className="text-2xl font-semibold tracking-wider text-gray-800 capitalize dark:text-white">
                                ข้อมูลส่วนตัว
                            </h1>

                            <p className="mt-4 text-gray-500 dark:text-gray-400">
                                ลงทะเบียนในระบบ : {moment(userInfo.createdAt).format('LLLL')}
                            </p>

                            <form className="grid grid-cols-1 gap-6 my-8 md:grid-cols-2" onSubmit={submitHandler}>
                                <div>
                                    <label htmlFor="site"
                                           className="block mb-2 text-sm text-gray-600 dark:text-gray-200">
                                        หน่วยงานที่สังกัด
                                    </label>
                                    {
                                        errorHospitals ? (
                                            <div className="flex items-center text-red-500 mt-2">

                                                <ExclamationCircleIcon className="h-5 w-5 mr-2"
                                                                       aria-hidden="true"/>
                                                ไม่สามารถโหลดข้อมูลได้
                                            </div>
                                        ) : (
                                            <Combobox value={selectedHospital}
                                                      onChange={setSelectedHospital}>
                                                <div className="relative mt-1">
                                                    <div
                                                        className="relative w-full cursor-default rounded-md">
                                                        <Combobox.Input
                                                            className="pr-10 leading-5 block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-900 dark:text-gray-300 dark:border-gray-700 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500"
                                                            displayValue={(hospital) => hospital.code + " : " + hospital.name}
                                                            onChange={(event) => setQueryHospital(event.target.value)}
                                                        />
                                                        <Combobox.Button
                                                            className="absolute inset-y-0 right-0 flex items-center pr-2">
                                                            <ChevronUpDownIcon
                                                                className="h-5 w-5 text-gray-400"
                                                                aria-hidden="true"
                                                            />
                                                        </Combobox.Button>
                                                    </div>
                                                    <Transition
                                                        as={Fragment}
                                                        leave="transition ease-in duration-100"
                                                        leaveFrom="opacity-100"
                                                        leaveTo="opacity-0"
                                                        afterLeave={() => setQueryHospital('')}
                                                    >
                                                        <Combobox.Options
                                                            className="absolute z-50 mt-1 max-h-60 w-full overflow-auto py-1 text-base shadow-lg text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-900 dark:text-gray-300 dark:border-gray-700 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                                                            {filteredHospital.length === 0 && queryHospital !== '' ? (
                                                                <div
                                                                    className="relative cursor-default select-none py-2 px-4 text-gray-700">
                                                                    Nothing found.
                                                                </div>
                                                            ) : (
                                                                filteredHospital.map((hospital) => (
                                                                    <Combobox.Option
                                                                        key={hospital._id}
                                                                        className={({active}) =>
                                                                            `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                                                                active ? 'bg-blue-500 text-white' : 'text-gray-700 dark:text-gray-300'
                                                                            }`
                                                                        }
                                                                        value={hospital}
                                                                    >
                                                                        {({selected, active}) => (
                                                                            <>
                                                    <span
                                                        className={`block truncate ${
                                                            selected ? 'font-medium' : 'font-normal'
                                                        }`}
                                                    >
                                                    {hospital.code + " : " + hospital.name}
                                                    </span>
                                                                                {selected ? (
                                                                                    <span
                                                                                        className={`absolute inset-y-0 left-0 flex items-center pl-3 ${
                                                                                            active ? 'text-white' : 'text-blue-500'
                                                                                        }`}
                                                                                    >
                                                    <CheckIcon className="h-5 w-5" aria-hidden="true"/>
                                                    </span>
                                                                                ) : null}
                                                                            </>
                                                                        )}
                                                                    </Combobox.Option>
                                                                ))
                                                            )}
                                                        </Combobox.Options>
                                                    </Transition>
                                                </div>
                                            </Combobox>
                                        )
                                    }

                                </div>
                                <div>
                                    <label htmlFor="title"
                                           className="block mb-2 text-sm text-gray-600 dark:text-gray-200">
                                        คำนำหน้า
                                    </label>
                                    {
                                        errorTitles ? (
                                            <div className="flex items-center text-red-500 mt-2">

                                                <ExclamationCircleIcon className="h-5 w-5 mr-2"
                                                                       aria-hidden="true"/>
                                                ไม่สามารถโหลดข้อมูลได้
                                            </div>
                                        ) : (
                                            <Combobox value={selectedTitle} onChange={setSelectedTitle}>
                                                <div className="relative mt-1">
                                                    <div
                                                        className="relative w-full cursor-default rounded-md">
                                                        <Combobox.Input
                                                            className="pr-10 leading-5 block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-900 dark:text-gray-300 dark:border-gray-700 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500"
                                                            displayValue={(title) => title.name}
                                                            onChange={(event) => setQueryTitle(event.target.value)}
                                                        />
                                                        <Combobox.Button
                                                            className="absolute inset-y-0 right-0 flex items-center pr-2">
                                                            <ChevronUpDownIcon
                                                                className="h-5 w-5 text-gray-400"
                                                                aria-hidden="true"
                                                            />
                                                        </Combobox.Button>
                                                    </div>
                                                    <Transition
                                                        as={Fragment}
                                                        leave="transition ease-in duration-100"
                                                        leaveFrom="opacity-100"
                                                        leaveTo="opacity-0"
                                                        afterLeave={() => setQueryTitle('')}
                                                    >
                                                        {/*z-50 fix for no behind all element*/}
                                                        <Combobox.Options
                                                            className="absolute z-50 mt-1 max-h-60 w-full overflow-auto py-1 text-base shadow-lg text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-900 dark:text-gray-300 dark:border-gray-700 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                                                            {filteredTitle.length === 0 && queryTitle !== '' ? (
                                                                <div
                                                                    className="relative cursor-default select-none py-2 px-4 text-gray-700">
                                                                    Nothing found.
                                                                </div>
                                                            ) : (
                                                                filteredTitle.map((title) => (
                                                                    <Combobox.Option
                                                                        key={title._id}
                                                                        className={({active}) =>
                                                                            `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                                                                active ? 'bg-blue-500 text-white' : 'text-gray-700 dark:text-gray-300'
                                                                            }`
                                                                        }
                                                                        value={title}
                                                                    >
                                                                        {({selected, active}) => (
                                                                            <>
                                                    <span
                                                        className={`block truncate ${
                                                            selected ? 'font-medium' : 'font-normal'
                                                        }`}
                                                    >
                                                {title.name}
                                                    </span>
                                                                                {selected ? (
                                                                                    <span
                                                                                        className={`absolute inset-y-0 left-0 flex items-center pl-3 ${
                                                                                            active ? 'text-white' : 'text-blue-500'
                                                                                        }`}
                                                                                    >
                                                    <CheckIcon className="h-5 w-5" aria-hidden="true"/>
                                                    </span>
                                                                                ) : null}
                                                                            </>
                                                                        )}
                                                                    </Combobox.Option>
                                                                ))
                                                            )}
                                                        </Combobox.Options>
                                                    </Transition>
                                                </div>
                                            </Combobox>
                                        )
                                    }

                                </div>

                                <div>
                                    <label htmlFor="first-name"
                                           className="block mb-2 text-sm text-gray-600 dark:text-gray-200">
                                        ชื่อ
                                    </label>
                                    <input
                                        type="text"
                                        name="first-name"
                                        id="first-name"
                                        autoComplete="given-name"
                                        required
                                        className="block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-900 dark:text-gray-300 dark:border-gray-700 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500"
                                        value={firstName}
                                        onChange={(e) => setFirstName(e.target.value)}
                                    />

                                </div>

                                <div>
                                    <label htmlFor="family-name"
                                           className="block mb-2 text-sm text-gray-600 dark:text-gray-200">
                                        นามสกุล
                                    </label>
                                    <input
                                        type="text"
                                        name="family-name"
                                        id="family-name"
                                        autoComplete="family-name"
                                        required
                                        className="block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-900 dark:text-gray-300 dark:border-gray-700 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500"
                                        value={lastName}
                                        onChange={(e) => setLastName(e.target.value)}
                                    />
                                </div>

                                <div>
                                    <label htmlFor="email"
                                           className="block mb-2 text-sm text-gray-600 dark:text-gray-200">อีเมล {error === "emailExist" ?
                                        <span className="text-sm text-red-500">*</span> : ""}</label>
                                    <input
                                        type="email"
                                        name="email"
                                        id="email"
                                        autoComplete="email"
                                        required
                                        placeholder="example@email.com"
                                        className={`block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border rounded-md dark:bg-gray-900 dark:placeholder-gray-600 dark:text-gray-300 focus:ring-1 focus:outline-none ${error === "emailExist" ? "focus-within:ring-red-500 border-red-500 dark:border-red-900 focus:border-red-700 dark:focus:border-red-700" : "border-gray-300 dark:border-gray-700 focus:border-blue-500 dark:focus:border-blue-500"}`}
                                        value={email}
                                        onChange={(e) => setEmail(e.target.value)}
                                    />
                                    {error === "emailExist" ?
                                        <p className="text-sm text-red-500">อีเมลนี้ ลงทะเบียนแล้ว</p> : ""}
                                </div>

                                <div>
                                    <label htmlFor="username"
                                           className="block mb-2 text-sm text-gray-600 dark:text-gray-200">
                                        ชื่อผู้ใช้งาน (Username) {error === "userExist" ?
                                        <span className="text-sm text-red-500">*</span> : ""}
                                    </label>
                                    <input
                                        type="text"
                                        name="username"
                                        id="username"
                                        autoComplete="username"
                                        required
                                        className={`block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border rounded-md dark:bg-gray-900 dark:placeholder-gray-600 dark:text-gray-300 focus:ring-1 focus:outline-none ${error === "userExist" ? "focus-within:ring-red-500 border-red-500 dark:border-red-900 focus:border-red-700 dark:focus:border-red-700" : "border-gray-300 dark:border-gray-700 focus:border-blue-500 dark:focus:border-blue-500"}`}
                                        value={username}
                                        onChange={(e) => setUserName(e.target.value)}
                                    />
                                    {error === "userExist" ?
                                        <p className="text-sm text-red-500">ชื่อผู้ใช้งาน ลงทะเบียนแล้ว</p> : ""}
                                </div>

                                <div>
                                    <label htmlFor="password"
                                           className="block mb-2 text-sm text-gray-600 dark:text-gray-200">รหัสผ่านเดิม
                                        (Current password)</label>
                                    <input
                                        type="password"
                                        name="password"
                                        id="password"
                                        disabled={isCheck ? false : true}
                                        required={isCheck ? true : false}
                                        minLength="8"
                                        className="disabled:bg-gray-50 block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-900 dark:text-gray-300 dark:border-gray-700 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500"
                                        value={password}
                                        onChange={(e) => setPassword(e.target.value)}
                                    />
                                </div>

                                <div>
                                    <label htmlFor="confirm-password"
                                           className="block mb-2 text-sm text-gray-600 dark:text-gray-200">รหัสผ่านใหม่
                                        (New password) {error === "PasswordNotMatch" ?
                                            <span className="text-sm text-red-500">*</span> : ""}</label>
                                    <input
                                        type="password"
                                        name="password"
                                        id="new-password"
                                        disabled={isCheck ? false : true}
                                        required={isCheck ? true : false}
                                        minLength="8"
                                        className={`disabled:bg-gray-50 block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border rounded-md dark:bg-gray-900 dark:placeholder-gray-600 dark:text-gray-300 focus:ring-1 focus:outline-none ${error === "PasswordNotMatch" ? "focus-within:ring-red-500 border-red-500 dark:border-red-900 focus:border-red-700 dark:focus:border-red-700" : "border-gray-300 dark:border-gray-700 focus:border-blue-500 dark:focus:border-blue-500"}`}
                                        value={newPassword}
                                        onChange={(e) => setNewPassword(e.target.value)}
                                    />
                                    {error === "PasswordNotMatch" ?
                                        <p className="text-sm text-red-500">รหัสผ่านไม่ตรงกัน</p> : ""}
                                </div>
                                <div className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0">
                                    <input
                                        id="change-password"
                                        name="change-password"
                                        type="checkbox"
                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"
                                        checked={isCheck}
                                        onChange={checkedHandler}
                                    />
                                    <label htmlFor="change-password"
                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                        เปลี่ยนรหัสผ่าน
                                    </label>
                                </div>
                                <div className="col-span-2">
                                    <button
                                        type="submit"
                                        className="w-full px-6 py-3 tracking-wide text-sm text-white transition-colors duration-300 transform bg-blue-600 rounded-md hover:bg-blue-700 focus:outline-none focus:bg-blue-700 focus:ring focus:ring-blue-500 focus:ring-opacity-50">
                                        ปรับปรุงข้อมูล
                                    </button>

                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </>)
}

export default Profile;