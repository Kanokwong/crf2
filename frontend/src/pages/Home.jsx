import React, {useEffect} from "react";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {listHospital} from "../redux/actions/hospitalActions";
import Loading from "../components/Loading.jsx";
import {toast} from "react-toastify";
import Header from "../components/Header.jsx";

// #3be8b0 pk - color
// #1aafd0, #6a67ce, ffb900, #fc636b
// Read more use react-toastify https://fkhadra.github.io/react-toastify/remove-toast
function Home() {
    const dispatch = useDispatch();
    const hospitalList = useSelector((state) => state.hospitalList);
    const {loading, error, hospitals} = hospitalList;
    // console.log(hospitals)
    // const mountFlag = useRef(false);
    useEffect(() => {
        // if (!mountFlag.current) {
        dispatch(listHospital());
        // toast.success("แสดงข้อมูลทั้งหมด")
        // mountFlag.current = true
        // console.log()
        // }
    }, [dispatch])


    return (

        <>
            <Header/>
            <div>
                {

                    // loading ? (<p>Loading...</p>) : error ? (<p>Error : {error}</p>) :
                    loading ? Loading() : error ? toast.error(`${error}`, {toastId: `${error}`})
                        :
                        (
                            <>

                                {

                                    hospitals.map((hospital) => (
                                        <div key={hospital._id}>
                                            <Link to={`/hospitals/${hospital._id}`}>
                                                {hospital.hospitalCode}
                                            </Link>

                                        </div>
                                    ))
                                }
                            </>
                        )
                }

            </div>
        </>


    )


    // const [hospitalls, setHospitals] = useState([]);
    //
    // useEffect(() => {
    //     const fechHospitals = async () => {
    //         const {data} = await axios.get("/api/hospitals");
    //         setHospitals(data);
    //     }
    //     fechHospitals();
    // }, []);
    //
    // return (
    //     <div>
    //         {hospitalls.map((hospital) => (
    //             <div key={hospital._id}>
    //                 <Link to={`/hospitals/${hospital._id}`}>
    //                     {hospital.hospitalCode}
    //                 </Link>
    //             </div>
    //         ))}
    //         <Link to="/hospitals/5">
    //             Hospital5
    //         </Link>
    //     </div>
    //
    // )
}

export default Home;