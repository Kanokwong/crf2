import React, {useState} from "react";
import Header from "../components/Header.jsx";
import DatePicker from "react-datepicker";
import {CalendarDaysIcon} from "@heroicons/react/20/solid";
import {getDay} from "date-fns";

function LabResult() {
    const [dob, setDob] = useState(new Date());
    const isWeekday = (date) => {
        const day = getDay(date);
        return day !== 0 && day !== 6;
    };

    return (
        <>
            <Header/>
            <section className="mx-auto max-w-7xl px-2 py-10 sm:px-6 lg:px-8 dark:bg-gray-900">
                <div className="px-4 sm:px-0">
                    <h3 className="text-xl font-medium leading-6 text-gray-700 dark:text-white">แบบเก็บข้อมูล (Case
                        record form):
                        การศึกษาประสิทธิผลของแคนนาบินอยด์และ
                        การประเมินคุณภาพชีวิตในผู้ป่วยมะเร็งระยะแพร่กระจาย
                    </h3>
                    <div className="grid grid-cols-1 sm:grid-cols-2 mt-2">
                        <div>
                            <p className="text-md text-gray-600 dark:text-gray-200 dark:text-gray-400">
                                ส่วนที่ 3 : ผลการตรวจทางห้องปฏิบัติการ/ผลการตรวจเพิ่มเติม
                            </p>
                        </div>
                        <div className="flex flex-wrap sm:flex-auto">
                            <div
                                className="flex flex-auto sm:flex-1 items-center">
                                <p className="text-md text-gray-600 dark:text-gray-200 dark:text-gray-400">
                                    STUDY SITE :</p>
                            </div>
                            <div
                                className="flex flex-auto sm:flex-1 items-center">
                                <p className="text-md text-gray-600 dark:text-gray-200 dark:text-gray-400">
                                    STUDY ID :</p>
                            </div>

                        </div>

                    </div>

                    <form>
                        <div className="overflow-visible shadow rounded-md">
                            <div
                                className="grid grid-cols-1 sm:gap-6 gap-2 mt-5 sm:grid-cols-2 bg-white rounded-md rounded-b-none shadow-md dark:bg-gray-800 px-4 py-5 sm:p-6">
                                <div className="flex flex-wrap sm:flex-auto items-center col-span-2">
                                    <label htmlFor="lab-cbc-submit-date"
                                           className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0">
                                        CBC : วัน เดือน ปี (ค.ศ.) ที่ส่งตรวจ
                                    </label>
                                    <div
                                        className="relative sm:ml-2 flex flex-auto sm:grow-0 items-center lg:h-5 w-fit mt-2 sm:mt-0">
                                        <DatePicker selected={dob} onChange={(date) => setDob(date)}
                                                    locale="th"
                                                    peekNextMonth
                                                    showMonthDropdown
                                                    showYearDropdown
                                                    disabledKeyboardNavigation
                                                    dropdownMode="select"
                                                    dateFormat="dd/MM/yyyy"
                                                    filterDate={isWeekday}
                                                    className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    placeholderText="วัน/เดือน/ปี ค.ศ."
                                                    name="lab-result"
                                                    id="lab-cbc-submit-date"
                                        />
                                        <div
                                            className="flex absolute inset-y-0 right-0 items-center pr-3 mt-1 pointer-events-none">
                                            <CalendarDaysIcon
                                                className="h-6 w-6 text-gray-500 dark:text-gray-400 sm:h-5 sm:w-5"
                                                aria-hidden="true"/>
                                        </div>
                                    </div>
                                </div>
                                <div className="sm:pl-6 col-span-2 sm:mt-1">
                                    <div className="grid grid-cols-1 sm:grid-cols-8 sm:gap-x-4 sm:gap-y-6 gap-2">
                                        <div
                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                            <label htmlFor="lab-cbc-hb"
                                                   className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                Hb
                                            </label>
                                            <div
                                                className="sm:ml-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:w-fit">
                                                <input
                                                    type="number"
                                                    name="lab-result"
                                                    id="lab-cbc-hb"
                                                    min="0"
                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                />
                                                <span
                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          g/dl
                        </span>
                                            </div>
                                        </div>
                                        <div
                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                            <label htmlFor="lab-cbc-hct"
                                                   className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                Hct
                                            </label>
                                            <div
                                                className="sm:ml-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:w-fit">
                                                <input
                                                    type="number"
                                                    name="lab-result"
                                                    id="lab-cbc-hct"
                                                    min="0"
                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                />
                                                <span
                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          %
                        </span>
                                            </div>
                                        </div>
                                        <div
                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                            <label htmlFor="lab-cbc-platelets"
                                                   className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                platelets
                                            </label>
                                            <div
                                                className="sm:ml-2 flex flex-auto sm:basis-2/3 items-center lg:h-5 mt-2 sm:mt-0 w-full sm:w-fit">
                                                <input
                                                    type="number"
                                                    name="lab-result"
                                                    id="lab-cbc-platelets"
                                                    min="0"
                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                />
                                                <span
                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          cell/mm3
                        </span>
                                            </div>
                                        </div>
                                        <div
                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                            <label htmlFor="lab-cbc-wbc"
                                                   className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                WBC
                                            </label>
                                            <div
                                                className="sm:ml-2 flex flex-auto sm:basis-2/3 items-center lg:h-5 mt-2 sm:mt-0 w-full sm:w-fit">
                                                <input
                                                    type="number"
                                                    name="lab-result"
                                                    id="lab-cbc-wbc"
                                                    min="0"
                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                />
                                                <span
                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          cell/mm3
                        </span>
                                            </div>
                                        </div>
                                        <div className="flex flex-wrap sm:flex-auto sm:grow-0 items-center">
                                            <label htmlFor="lab-cbc-n"
                                                   className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                N
                                            </label>
                                            <div
                                                className="sm:ml-2 flex flex-auto sm:basis-2/3 items-center lg:h-5 mt-2 sm:mt-0 w-full sm:w-fit">
                                                <input
                                                    type="number"
                                                    name="lab-result"
                                                    id="lab-cbc-n"
                                                    min="0"
                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                />
                                                <span
                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          %
                        </span>
                                            </div>
                                        </div>
                                        <div className="flex flex-wrap sm:flex-auto sm:grow-0 items-center">
                                            <label htmlFor="lab-cbc-l"
                                                   className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                L
                                            </label>
                                            <div
                                                className="sm:ml-2 flex flex-auto sm:basis-2/3 items-center lg:h-5 mt-2 sm:mt-0 w-full sm:w-fit">
                                                <input
                                                    type="number"
                                                    name="lab-result"
                                                    id="lab-cbc-l"
                                                    min="0"
                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                />
                                                <span
                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          %
                        </span>
                                            </div>
                                        </div>
                                        <div className="flex flex-wrap sm:flex-auto sm:grow-0 items-center">
                                            <label htmlFor="lab-cbc-m"
                                                   className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                M
                                            </label>
                                            <div
                                                className="sm:ml-2 flex flex-auto sm:basis-2/3 items-center lg:h-5 mt-2 sm:mt-0 w-full sm:w-fit">
                                                <input
                                                    type="number"
                                                    name="lab-result"
                                                    id="lab-cbc-m"
                                                    min="0"
                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                />
                                                <span
                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          %
                        </span>
                                            </div>
                                        </div>
                                        <div className="flex flex-wrap sm:flex-auto sm:grow-0 items-center">
                                            <label htmlFor="lab-cbc-e"
                                                   className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                E
                                            </label>
                                            <div
                                                className="sm:ml-2 flex flex-auto sm:basis-2/3 items-center lg:h-5 mt-2 sm:mt-0 w-full sm:w-fit">
                                                <input
                                                    type="number"
                                                    name="lab-result"
                                                    id="lab-cbc-e"
                                                    min="0"
                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                />
                                                <span
                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          %
                        </span>
                                            </div>
                                        </div>
                                        <div
                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                            <label htmlFor="lab-cbc-anc"
                                                   className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                ANC
                                            </label>
                                            <div
                                                className="sm:ml-2 flex flex-auto sm:basis-2/3 items-center lg:h-5 mt-2 sm:mt-0 w-full sm:w-fit">
                                                <input
                                                    type="number"
                                                    name="lab-result"
                                                    id="lab-cbc-anc"
                                                    min="0"
                                                    className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-span-2 sm:mt-1">
                                    <div className="grid grid-cols-1 sm:grid-cols-3 sm:gap-x-4 sm:gap-y-6 gap-2">
                                        <div
                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center">
                                            <label htmlFor="lab-cbc-blood-sugar"
                                                   className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                Blood sugar
                                            </label>
                                            <div
                                                className="sm:ml-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:w-fit">
                                                <input
                                                    type="number"
                                                    name="lab-result"
                                                    id="lab-cbc-blood-sugar"
                                                    min="0"
                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                />
                                                <span
                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          mg/dl
                        </span>
                                            </div>
                                        </div>
                                        <div
                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center">
                                            <label htmlFor="lab-cbc-calcium"
                                                   className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                Calcium
                                            </label>
                                            <div
                                                className="sm:ml-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:w-fit">
                                                <input
                                                    type="number"
                                                    name="lab-result"
                                                    id="lab-cbc-calcium"
                                                    min="0"
                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                />
                                                <span
                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          mmol/L
                        </span>
                                            </div>
                                        </div>
                                        <div
                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center">
                                            <label htmlFor="lab-cbc-magnesium"
                                                   className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                Magnesium
                                            </label>
                                            <div
                                                className="sm:ml-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:w-fit">
                                                <input
                                                    type="number"
                                                    name="lab-result"
                                                    id="lab-cbc-magnesium"
                                                    min="0"
                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                />
                                                <span
                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          mmol/L
                        </span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div className="col-span-2 sm:mt-1">
                                    <div className="grid grid-cols-1 sm:grid-cols-3 sm:gap-x-4 sm:gap-y-6 gap-2">
                                        <div
                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center">
                                            <label htmlFor="lab-cbc-pt"
                                                   className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                PT
                                            </label>
                                            <div
                                                className="sm:ml-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:w-fit">
                                                <input
                                                    type="number"
                                                    name="lab-result"
                                                    id="lab-cbc-pt"
                                                    min="0"
                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                />
                                                <span
                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          sec
                        </span>
                                            </div>
                                        </div>
                                        <div
                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center">
                                            <label htmlFor="lab-cbc-ptt"
                                                   className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                PTT
                                            </label>
                                            <div
                                                className="sm:ml-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:w-fit">
                                                <input
                                                    type="number"
                                                    name="lab-result"
                                                    id="lab-cbc-ptt"
                                                    min="0"
                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                />
                                                <span
                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          sec
                        </span>
                                            </div>
                                        </div>
                                        <div
                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center">
                                            <label htmlFor="lab-cbc-inr"
                                                   className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                INR
                                            </label>
                                            <div
                                                className="sm:ml-2 flex flex-auto sm:basis-2/3 items-center lg:h-5 mt-2 sm:mt-0 w-full sm:w-fit">
                                                <input
                                                    type="number"
                                                    name="lab-result"
                                                    id="lab-cbc-inr"
                                                    min="0"
                                                    className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div className="flex flex-wrap sm:flex-auto items-center col-span-2">
                                    <label htmlFor="lab-ua-submit-date"
                                           className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0">
                                        UA : วัน เดือน ปี (ค.ศ.) ที่ส่งตรวจ
                                    </label>
                                    <div
                                        className="relative sm:ml-2 flex flex-auto sm:grow-0 items-center lg:h-5 w-fit mt-2 sm:mt-0">
                                        <DatePicker selected={dob} onChange={(date) => setDob(date)}
                                                    locale="th"
                                                    peekNextMonth
                                                    showMonthDropdown
                                                    showYearDropdown
                                                    disabledKeyboardNavigation
                                                    dropdownMode="select"
                                                    dateFormat="dd/MM/yyyy"
                                                    filterDate={isWeekday}
                                                    className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    placeholderText="วัน/เดือน/ปี ค.ศ."
                                                    name="lab-result"
                                                    id="lab-ua-submit-date"
                                        />
                                        <div
                                            className="flex absolute inset-y-0 right-0 items-center pr-3 mt-1 pointer-events-none">
                                            <CalendarDaysIcon
                                                className="h-6 w-6 text-gray-500 dark:text-gray-400 sm:h-5 sm:w-5"
                                                aria-hidden="true"/>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-span-2 sm:mt-1">
                                    <div className="grid grid-cols-1 sm:grid-cols-6 sm:gap-x-4 sm:gap-y-6 gap-2">
                                        <div
                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                            <label htmlFor="lab-ua-sp-gr"
                                                   className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                sp.gr
                                            </label>
                                            <div
                                                className="sm:ml-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:w-fit">
                                                <input
                                                    type="number"
                                                    name="lab-result"
                                                    id="lab-ua-sp-gr"
                                                    min="0"
                                                    className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                />
                                            </div>
                                        </div>
                                        <div
                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                            <label htmlFor="lab-ua-sugar"
                                                   className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                sugar
                                            </label>
                                            <div
                                                className="sm:ml-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:w-fit">
                                                <input
                                                    type="number"
                                                    name="lab-result"
                                                    id="lab-ua-sugar"
                                                    min="0"
                                                    className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                />
                                            </div>
                                        </div>
                                        <div
                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                            <label htmlFor="lab-ua-protein"
                                                   className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                protein
                                            </label>
                                            <div
                                                className="sm:ml-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:w-fit">
                                                <input
                                                    type="number"
                                                    name="lab-result"
                                                    id="lab-ua-protein"
                                                    min="0"
                                                    className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                />
                                            </div>
                                        </div>
                                        <div
                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                            <label htmlFor="lab-ua-wbc"
                                                   className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                WBC
                                            </label>
                                            <div
                                                className="sm:ml-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:w-fit">
                                                <input
                                                    type="number"
                                                    name="lab-result"
                                                    id="lab-ua-wbc"
                                                    min="0"
                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                />
                                                <span
                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          cells/HPF
                        </span>
                                            </div>
                                        </div>
                                        <div
                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                            <label htmlFor="lab-ua-rbc"
                                                   className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                RBC
                                            </label>
                                            <div
                                                className="sm:ml-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:w-fit">
                                                <input
                                                    type="number"
                                                    name="lab-result"
                                                    id="lab-ua-rbc"
                                                    min="0"
                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                />
                                                <span
                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          cells/HPF
                        </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-span-2 sm:mt-1">
                                    <div className="grid grid-cols-1 sm:grid-cols-6 sm:gap-x-4 sm:gap-y-6 gap-2">
                                        <div className="flex flex-wrap sm:flex-auto items-center col-span-2">
                                            <label htmlFor="lab-urine-submit-date"
                                                   className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0">
                                                Urine cannabinoid test :
                                            </label>
                                            <div
                                                className="relative sm:ml-2 flex flex-auto sm:grow-0 items-center lg:h-5 w-fit mt-2 sm:mt-0">
                                                <DatePicker selected={dob} onChange={(date) => setDob(date)}
                                                            locale="th"
                                                            peekNextMonth
                                                            showMonthDropdown
                                                            showYearDropdown
                                                            disabledKeyboardNavigation
                                                            dropdownMode="select"
                                                            dateFormat="dd/MM/yyyy"
                                                            filterDate={isWeekday}
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                            placeholderText="วัน/เดือน/ปี ค.ศ."
                                                            name="lab-urine"
                                                            id="lab-urine-submit-date"
                                                />
                                                <div
                                                    className="flex absolute inset-y-0 right-0 items-center pr-3 mt-1 pointer-events-none">
                                                    <CalendarDaysIcon
                                                        className="h-6 w-6 text-gray-500 dark:text-gray-400 sm:h-5 sm:w-5"
                                                        aria-hidden="true"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            className="col-span-4">
                                            <fieldset>
                                                <div className="flex flex-col sm:flex-row">
                                                    <legend
                                                        className="flex-wrap sm:flex-auto sm:grow-0 items-center text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0">
                                                        ผล
                                                    </legend>
                                                    <div
                                                        className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                        <input
                                                            id="lab-urine-negative"
                                                            name="lab-urine"
                                                            type="radio"
                                                            className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                        <label htmlFor="lab-urine-negative"
                                                               className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                            ปกติ
                                                        </label>
                                                    </div>
                                                    <div
                                                        className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                        <input
                                                            id="lab-urine-positive"
                                                            name="lab-urine"
                                                            type="radio"
                                                            className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                        <label htmlFor="lab-urine-positive"
                                                               className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                            ผิดปกติ ระบุ
                                                        </label>
                                                        <div
                                                            className="sm:ml-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 ml-6">
                                                            <input
                                                                type="text"
                                                                name="lab-urine"
                                                                id="lab-urine-positive-detail"
                                                                className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>

                                    </div>

                                </div>
                                <div
                                    className="col-span-2 grid grid-cols-1 md:grid-cols-9 border border-gray-300 border-separate bg-gray-50 dark:bg-gray-800 dark:border-gray-600">
                                    <div
                                        className="row-span-2 border-b border-slate-300 dark:bg-gray-800 dark:border-gray-600 py-8 sm:py-12 row-start-1 row-end-auto sm:row-span-2 text-center text-sm text-gray-600 dark:text-gray-200">
                                      Lab
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-slate-300 dark:bg-gray-800 dark:border-gray-600 py-4 row-start-2 sm:row-start-auto text-center text-sm text-gray-600 dark:text-gray-200">ครั้งที่
                                        1 (Baseline)
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-slate-300 dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto text-center text-sm text-gray-600 dark:text-gray-200">ครั้งที่
                                        2 (Day28-33)
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-slate-300 dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto text-center text-sm text-gray-600 dark:text-gray-200">ครั้งที่
                                        3 (Day56-61)
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-slate-300 dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto text-center text-sm text-gray-600 dark:text-gray-200">ครั้งที่
                                        4 (Day84-89)
                                    </div>
                                    <div
                                        className="sm:col-span-2 border sm:border-r-0 border-slate-300 dark:bg-gray-800 dark:border-gray-600 py-4 row-start-3 sm:row-start-auto">
                                        <div className="flex flex-wrap sm:flex-auto items-center sm:mx-0 sm:my-2 mx-2">
                                            <label htmlFor="lab-period1-date"
                                                   className="sm:ml-6 lg:ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0">
                                                วันที่
                                            </label>
                                            <div
                                                className="relative sm:ml-2 flex flex-auto items-center lg:h-5 w-fit mt-2 sm:mt-0 mx-2">
                                                <DatePicker selected={dob} onChange={(date) => setDob(date)}
                                                            locale="th"
                                                            peekNextMonth
                                                            showMonthDropdown
                                                            showYearDropdown
                                                            disabledKeyboardNavigation
                                                            dropdownMode="select"
                                                            dateFormat="dd/MM/yyyy"
                                                            filterDate={isWeekday}
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                            placeholderText="วัน/เดือน/ปี ค.ศ."
                                                            name="lab-result-lab"
                                                            id="lab-period1-date"
                                                />
                                                <div
                                                    className="flex absolute inset-y-0 right-0 items-center pr-3 mt-1 pointer-events-none">
                                                    <CalendarDaysIcon
                                                        className="h-6 w-6 text-gray-500 dark:text-gray-400 sm:h-5 sm:w-5"
                                                        aria-hidden="true"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border sm:border-r-0 border-slate-300 dark:bg-gray-800 dark:border-gray-600 py-4 row-start-3 sm:row-start-auto">
                                        <div className="flex flex-wrap sm:flex-auto items-center sm:mx-0 sm:my-2 mx-2">
                                            <label htmlFor="lab-period2-date"
                                                   className="sm:ml-6 lg:ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0">
                                                วันที่
                                            </label>
                                            <div
                                                className="relative sm:ml-2 flex flex-auto items-center lg:h-5 w-fit mt-2 sm:mt-0 mx-2">
                                                <DatePicker selected={dob} onChange={(date) => setDob(date)}
                                                            locale="th"
                                                            peekNextMonth
                                                            showMonthDropdown
                                                            showYearDropdown
                                                            disabledKeyboardNavigation
                                                            dropdownMode="select"
                                                            dateFormat="dd/MM/yyyy"
                                                            filterDate={isWeekday}
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                            placeholderText="วัน/เดือน/ปี ค.ศ."
                                                            name="lab-result-lab"
                                                            id="lab-period2-date"
                                                />
                                                <div
                                                    className="flex absolute inset-y-0 right-0 items-center pr-3 mt-1 pointer-events-none">
                                                    <CalendarDaysIcon
                                                        className="h-6 w-6 text-gray-500 dark:text-gray-400 sm:h-5 sm:w-5"
                                                        aria-hidden="true"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border sm:border-r-0 border-slate-300 dark:bg-gray-800 dark:border-gray-600 py-4 row-start-3 sm:row-start-auto">
                                        <div className="flex flex-wrap sm:flex-auto items-center sm:mx-0 sm:my-2 mx-2">
                                            <label htmlFor="lab-period3-date"
                                                   className="sm:ml-6 lg:ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0">
                                                วันที่
                                            </label>
                                            <div
                                                className="relative sm:ml-2 flex flex-auto items-center lg:h-5 w-fit mt-2 sm:mt-0 mx-2">
                                                <DatePicker selected={dob} onChange={(date) => setDob(date)}
                                                            locale="th"
                                                            peekNextMonth
                                                            showMonthDropdown
                                                            showYearDropdown
                                                            disabledKeyboardNavigation
                                                            dropdownMode="select"
                                                            dateFormat="dd/MM/yyyy"
                                                            filterDate={isWeekday}
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                            placeholderText="วัน/เดือน/ปี ค.ศ."
                                                            name="lab-result-lab"
                                                            id="lab-period3-date"
                                                />
                                                <div
                                                    className="flex absolute inset-y-0 right-0 items-center pr-3 mt-1 pointer-events-none">
                                                    <CalendarDaysIcon
                                                        className="h-6 w-6 text-gray-500 dark:text-gray-400 sm:h-5 sm:w-5"
                                                        aria-hidden="true"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border sm:border-r-0 border-slate-300 dark:bg-gray-800 dark:border-gray-600 py-4 row-start-3 sm:row-start-auto">
                                        <div className="flex flex-wrap sm:flex-auto items-center sm:mx-0 sm:my-2 mx-2">
                                            <label htmlFor="lab-period4-date"
                                                   className="sm:ml-6 lg:ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0">
                                                วันที่
                                            </label>
                                            <div
                                                className="relative sm:ml-2 flex flex-auto items-center lg:h-5 w-fit mt-2 sm:mt-0 mx-2">
                                                <DatePicker selected={dob} onChange={(date) => setDob(date)}
                                                            locale="th"
                                                            peekNextMonth
                                                            showMonthDropdown
                                                            showYearDropdown
                                                            disabledKeyboardNavigation
                                                            dropdownMode="select"
                                                            dateFormat="dd/MM/yyyy"
                                                            filterDate={isWeekday}
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                            placeholderText="วัน/เดือน/ปี ค.ศ."
                                                            name="lab-result-lab"
                                                            id="lab-period4-date"
                                                />
                                                <div
                                                    className="flex absolute inset-y-0 right-0 items-center pr-3 mt-1 pointer-events-none">
                                                    <CalendarDaysIcon
                                                        className="h-6 w-6 text-gray-500 dark:text-gray-400 sm:h-5 sm:w-5"
                                                        aria-hidden="true"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div
                                        className="bg-white py-8 sm:py-40 row-end-auto sm:row-span-8 border-b border-slate-300 dark:bg-gray-800 dark:border-gray-600">
                                        <h6 className="text-center text-sm text-gray-600 dark:text-gray-200">LFT</h6>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto bg-white">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <label htmlFor="lab-result-lft-tp"
                                                       className="sm:ml-2 lg:ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                    TP
                                                </label>
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="lab-result-lft-period1"
                                                        id="lab-result-lft-tp"
                                                        min="0"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          g/dl
                        </span>
                                                </div>
                                            </div>
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <label htmlFor="lab-result-lft-albumin"
                                                       className="sm:ml-2 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                    albumin
                                                </label>
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="lab-result-lft-period1"
                                                        id="lab-result-lft-albumin"
                                                        min="0"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          g/dl
                        </span>
                                                </div>
                                            </div>
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <label htmlFor="lab-result-lft-globulin"
                                                       className="sm:ml-2 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                    globulin
                                                </label>
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="lab-result-lft-period1"
                                                        id="lab-result-lft-globulin"
                                                        min="0"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          g/dl
                        </span>
                                                </div>
                                            </div>
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <label htmlFor="lab-result-lft-tb"
                                                       className="sm:ml-2 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                    TB
                                                </label>
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="lab-result-lft-period1"
                                                        id="lab-result-lft-tb"
                                                        min="0"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          mg/dl
                        </span>
                                                </div>
                                            </div>
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <label htmlFor="lab-result-lft-db"
                                                       className="sm:ml-2 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                    DB
                                                </label>
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="lab-result-lft-period1"
                                                        id="lab-result-lft-db"
                                                        min="0"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          mg/dl
                        </span>
                                                </div>
                                            </div>
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <label htmlFor="lab-result-lft-ast-period1"
                                                       className="sm:ml-2 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                    AST
                                                </label>
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="lab-result-lft-period1"
                                                        id="lab-result-lft-ast-period1"
                                                        min="0"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          U/L
                        </span>
                                                </div>
                                            </div>
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <label htmlFor="lab-result-lft-alt-period1"
                                                       className="sm:ml-2 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                    ALT
                                                </label>
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="lab-result-lft-period1"
                                                        id="lab-result-lft-alt-period1"
                                                        min="0"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          U/L
                        </span>
                                                </div>
                                            </div>
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <label htmlFor="lab-result-lft-alp"
                                                       className="sm:ml-2 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                    ALP
                                                </label>
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="lab-result-lft-period1"
                                                        id="lab-result-lft-alp-period1"
                                                        min="0"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          U/L
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto bg-white">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <label htmlFor="lab-result-lft-ast-period2"
                                                       className="sm:ml-2 lg:ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                    AST
                                                </label>
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="lab-result-lft-period2"
                                                        id="lab-result-lft-ast-period2"
                                                        min="0"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          U/L
                        </span>
                                                </div>
                                            </div>
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <label htmlFor="lab-result-lft-alt-period2"
                                                       className="sm:ml-2 lg:ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                    ALT
                                                </label>
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="lab-result-lft-period2"
                                                        id="lab-result-lft-alt-period2"
                                                        min="0"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          U/L
                        </span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto bg-white">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <label htmlFor="lab-result-lft-ast-period3"
                                                       className="sm:ml-2 lg:ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                    AST
                                                </label>
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="lab-result-lft-period3"
                                                        id="lab-result-lft-ast-period3"
                                                        min="0"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          U/L
                        </span>
                                                </div>
                                            </div>
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <label htmlFor="lab-result-lft-alt-period3"
                                                       className="sm:ml-2 lg:ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                    ALT
                                                </label>
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="lab-result-lft-period3"
                                                        id="lab-result-lft-alt-period3"
                                                        min="0"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          U/L
                        </span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto bg-white">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <label htmlFor="lab-result-lft-ast-period4"
                                                       className="sm:ml-2 lg:ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                    AST
                                                </label>
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="lab-result-lft-period4"
                                                        id="lab-result-lft-ast-period4"
                                                        min="0"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          U/L
                        </span>
                                                </div>
                                            </div>
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <label htmlFor="lab-result-lft-alt-period4"
                                                       className="sm:ml-2 lg:ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                    ALT
                                                </label>
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="lab-result-lft-period4"
                                                        id="lab-result-lft-alt-period4"
                                                        min="0"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          U/L
                        </span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                    <div
                                        className="bg-white py-8 sm:py-40 row-end-auto sm:row-span-8 dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto bg-white">
                                        <h6 className="text-center text-sm text-gray-600 dark:text-gray-200">BUNCr</h6>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t sm:border-t-0 border-slate-300 dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto bg-white">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <label htmlFor="lab-result-buncr-bun-period1"
                                                       className="sm:ml-2 lg:ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                    BUN
                                                </label>
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="lab-result-buncr"
                                                        id="lab-result-buncr-bun-period1"
                                                        min="0"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          mg/dl
                        </span>
                                                </div>
                                            </div>
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <label htmlFor="lab-result-buncr-cr-period1"
                                                       className="sm:ml-2 lg:ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                    Cr
                                                </label>
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="lab-result-buncr"
                                                        id="lab-result-buncr-cr-period1"
                                                        min="0"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          mg/dl
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t sm:border-t-0 border-slate-300 dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto bg-white">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <label htmlFor="lab-result-buncr-bun-period2"
                                                       className="sm:ml-2 lg:ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                    BUN
                                                </label>
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="lab-result-buncr-period2"
                                                        id="lab-result-buncr-bun-period2"
                                                        min="0"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          mg/dl
                        </span>
                                                </div>
                                            </div>
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <label htmlFor="lab-result-buncr-cr-period2"
                                                       className="sm:ml-2 lg:ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                    Cr
                                                </label>
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="lab-result-buncr-period2"
                                                        id="lab-result-buncr-cr-period2"
                                                        min="0"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          mg/dl
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t sm:border-t-0 border-slate-300 dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto bg-white">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <label htmlFor="lab-result-buncr-bun-period3"
                                                       className="sm:ml-2 lg:ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                    BUN
                                                </label>
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="lab-result-buncr-period3"
                                                        id="lab-result-buncr-bun-period3"
                                                        min="0"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          mg/dl
                        </span>
                                                </div>
                                            </div>
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <label htmlFor="lab-result-buncr-cr-period3"
                                                       className="sm:ml-2 lg:ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                    Cr
                                                </label>
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="lab-result-buncr-period3"
                                                        id="lab-result-buncr-cr-period3"
                                                        min="0"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          mg/dl
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t sm:border-t-0 border-slate-300 dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto bg-white">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <label htmlFor="lab-result-buncr-bun-period4"
                                                       className="sm:ml-2 lg:ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                    BUN
                                                </label>
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="lab-result-buncr-period4"
                                                        id="lab-result-buncr-bun-period4"
                                                        min="0"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          mg/dl
                        </span>
                                                </div>
                                            </div>
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <label htmlFor="lab-result-buncr-cr-period4"
                                                       className="sm:ml-2 lg:ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:w-fit w-full">
                                                    Cr
                                                </label>
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="lab-result-buncr-period4"
                                                        id="lab-result-buncr-cr-period4"
                                                        min="0"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          mg/dl
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div className="bg-gray-50 dark:bg-gray-700 rounded-b-md px-4 py-3 text-right sm:px-6">
                                {/*<button*/}
                                {/*    type="button"*/}
                                {/*    onClick={handleBack}*/}
                                {/*    className="inline-flex justify-center px-6 py-3 tracking-wide text-sm text-white transition-colors duration-300 transform bg-blue-600 rounded-md hover:bg-blue-700 focus:outline-none focus:bg-blue-700 focus:ring focus:ring-blue-500 focus:ring-opacity-50">*/}
                                {/*    ย้อนกลับ*/}
                                {/*</button>*/}


                                <button
                                    type="submit"
                                    className="inline-flex justify-center px-6 py-3 tracking-wide text-sm text-white transition-colors duration-300 transform bg-blue-600 rounded-md hover:bg-blue-700 focus:outline-none focus:bg-blue-700 focus:ring focus:ring-blue-500 focus:ring-opacity-50">
                                    บันทึกข้อมูล
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </section>

        </>
    )
}

export default LabResult;