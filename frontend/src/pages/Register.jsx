import React, {Fragment, useEffect, useState} from "react";
import {CheckIcon, ChevronUpDownIcon, ExclamationCircleIcon} from "@heroicons/react/20/solid";
import {Combobox, Transition} from "@headlessui/react";
import {useDispatch, useSelector} from "react-redux";
import {listHospital} from "../redux/actions/hospitalActions";
import {listTitle} from "../redux/actions/titleActions";
import {Link, useLocation, useNavigate} from "react-router-dom";
import {register} from "../redux/actions/userActions";

function Register() {
    // const [hospital, setHospital] = useState({
    //     code: "",
    //     name: "",
    // });
    // const [title, setTitle] = useState("");
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [username, setUserName] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");


    const dispatch = useDispatch();
    const navigate = useNavigate();
    const location = useLocation();

    const redirect = location.search ? location.search.split("=")[1] : "/";
    const userRegister = useSelector((state) => state.userRegister);
    const {error, userInfo} = userRegister;

    const hospitalList = useSelector((state) => state.hospitalList);
    const {error: errorHospitals, hospitals} = hospitalList;
    const titleList = useSelector((state) => state.titleList)
    const {error: errorTitles, titles} = titleList;

    useEffect(() => {
        dispatch(listHospital());
        dispatch(listTitle());
        if (userInfo) {
            navigate(redirect);
        }

    }, [dispatch, userInfo, navigate, redirect])


    const [selectedHospital, setSelectedHospital] = useState(hospitals);
    const [queryHospital, setQueryHospital] = useState("");
    const [selectedTitle, setSelectedTitle] = useState(titles);
    const [queryTitle, setQueryTitle] = useState("");

    const filteredHospital =
        queryHospital === ''
            ? hospitals
            : hospitals.filter((hospital) =>
                hospital.name
                    .toLowerCase()
                    .replace(/\s+/g, '')
                    .includes(queryHospital.toLowerCase().replace(/\s+/g, ''))
            )

    const filteredTitle =
        queryTitle === ''
            ? titles
            : titles.filter((title) =>
                title.name
                    .toLowerCase()
                    .replace(/\s+/g, '')
                    .includes(queryTitle.toLowerCase().replace(/\s+/g, ''))
            )

    const submitHandler = (e) => {
        e.preventDefault()
        dispatch(register(selectedHospital, selectedTitle, firstName, lastName, email, username, password, confirmPassword));
    }

    return (
        <>
            <section className="bg-white dark:bg-gray-900">
                <div className="flex justify-center min-h-screen">
                    <div
                        className="hidden bg-cover lg:block lg:w-2/5 bg-[url('https://images.unsplash.com/photo-1494621930069-4fd4b2e24a11?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=715&q=80')]">
                    </div>

                    <div className="flex items-center w-full max-w-3xl p-8 mx-auto lg:px-12 lg:w-3/5">
                        <div className="w-full">

                            <h1 className="text-2xl font-semibold tracking-wider text-gray-800 capitalize dark:text-white">
                                ลงทะเบียนเพื่อรับบัญชีส่วนตัวของคุณ
                            </h1>

                            <p className="mt-4 text-gray-500 dark:text-gray-400">
                                มาตั้งค่าทั้งหมดกันเพื่อให้คุณสามารถยืนยันบัญชีส่วนตัวของคุณและเริ่มตั้งค่าโปรไฟล์ของคุณได้
                            </p>

                            <form className="grid grid-cols-1 gap-6 my-8 md:grid-cols-2" onSubmit={submitHandler}>
                                <div>
                                    <label htmlFor="site"
                                           className="block mb-2 text-sm text-gray-600 dark:text-gray-200">
                                        หน่วยงานที่สังกัด
                                    </label>
                                    {
                                        errorHospitals ? (
                                            <div className="flex items-center text-red-500 mt-2">

                                                <ExclamationCircleIcon className="h-5 w-5 mr-2"
                                                                       aria-hidden="true"/>
                                                ไม่สามารถโหลดข้อมูลได้
                                            </div>
                                        ) : (
                                            <Combobox value={selectedHospital}
                                                      onChange={setSelectedHospital}>
                                                <div className="relative mt-1">
                                                    <div
                                                        className="relative w-full cursor-default rounded-md">
                                                        <Combobox.Input
                                                            className="pr-10 leading-5 block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-900 dark:text-gray-300 dark:border-gray-700 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500"
                                                            displayValue={(hospital) => hospital.name}
                                                            onChange={(event) => setQueryHospital(event.target.value)}
                                                        />
                                                        <Combobox.Button
                                                            className="absolute inset-y-0 right-0 flex items-center pr-2">
                                                            <ChevronUpDownIcon
                                                                className="h-5 w-5 text-gray-400"
                                                                aria-hidden="true"
                                                            />
                                                        </Combobox.Button>
                                                    </div>
                                                    <Transition
                                                        as={Fragment}
                                                        leave="transition ease-in duration-100"
                                                        leaveFrom="opacity-100"
                                                        leaveTo="opacity-0"
                                                        afterLeave={() => setQueryHospital('')}
                                                    >
                                                        <Combobox.Options
                                                            className="absolute z-50 mt-1 max-h-60 w-full overflow-auto py-1 text-base shadow-lg text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-900 dark:text-gray-300 dark:border-gray-700 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                                                            {filteredHospital.length === 0 && queryHospital !== '' ? (
                                                                <div
                                                                    className="relative cursor-default select-none py-2 px-4 text-gray-700">
                                                                    Nothing found.
                                                                </div>
                                                            ) : (
                                                                filteredHospital.map((hospital) => (
                                                                    <Combobox.Option
                                                                        key={hospital._id}
                                                                        className={({active}) =>
                                                                            `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                                                                active ? 'bg-blue-500 text-white' : 'text-gray-700 dark:text-gray-300'
                                                                            }`
                                                                        }
                                                                        value={hospital}
                                                                    >
                                                                        {({selected, active}) => (
                                                                            <>
                                                    <span
                                                        className={`block truncate ${
                                                            selected ? 'font-medium' : 'font-normal'
                                                        }`}
                                                    >
                                                   {hospital.code + " : " + hospital.name}
                                                    </span>
                                                                                {selected ? (
                                                                                    <span
                                                                                        className={`absolute inset-y-0 left-0 flex items-center pl-3 ${
                                                                                            active ? 'text-white' : 'text-blue-500'
                                                                                        }`}
                                                                                    >
                                                    <CheckIcon className="h-5 w-5" aria-hidden="true"/>
                                                    </span>
                                                                                ) : null}
                                                                            </>
                                                                        )}
                                                                    </Combobox.Option>
                                                                ))
                                                            )}
                                                        </Combobox.Options>
                                                    </Transition>
                                                </div>
                                            </Combobox>
                                        )
                                    }

                                </div>
                                <div>
                                    <label htmlFor="title"
                                           className="block mb-2 text-sm text-gray-600 dark:text-gray-200">
                                        คำนำหน้า
                                    </label>
                                    {
                                        errorTitles ? (
                                            <div className="flex items-center text-red-500 mt-2">

                                                <ExclamationCircleIcon className="h-5 w-5 mr-2"
                                                                       aria-hidden="true"/>
                                                ไม่สามารถโหลดข้อมูลได้
                                            </div>
                                        ) : (
                                            <Combobox value={selectedTitle} onChange={setSelectedTitle}>
                                                <div className="relative mt-1">
                                                    <div
                                                        className="relative w-full cursor-default rounded-md">
                                                        <Combobox.Input
                                                            className="pr-10 leading-5 block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-900 dark:text-gray-300 dark:border-gray-700 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500"
                                                            displayValue={(title) => title.name}
                                                            onChange={(event) => setQueryTitle(event.target.value)}
                                                        />
                                                        <Combobox.Button
                                                            className="absolute inset-y-0 right-0 flex items-center pr-2">
                                                            <ChevronUpDownIcon
                                                                className="h-5 w-5 text-gray-400"
                                                                aria-hidden="true"
                                                            />
                                                        </Combobox.Button>
                                                    </div>
                                                    <Transition
                                                        as={Fragment}
                                                        leave="transition ease-in duration-100"
                                                        leaveFrom="opacity-100"
                                                        leaveTo="opacity-0"
                                                        afterLeave={() => setQueryTitle('')}
                                                    >
                                                        {/*z-50 fix for no behind all element*/}
                                                        <Combobox.Options
                                                            className="absolute z-50 mt-1 max-h-60 w-full overflow-auto py-1 text-base shadow-lg text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-900 dark:text-gray-300 dark:border-gray-700 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                                                            {filteredTitle.length === 0 && queryTitle !== '' ? (
                                                                <div
                                                                    className="relative cursor-default select-none py-2 px-4 text-gray-700">
                                                                    Nothing found.
                                                                </div>
                                                            ) : (
                                                                filteredTitle.map((title) => (
                                                                    <Combobox.Option
                                                                        key={title._id}
                                                                        className={({active}) =>
                                                                            `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                                                                active ? 'bg-blue-500 text-white' : 'text-gray-700 dark:text-gray-300'
                                                                            }`
                                                                        }
                                                                        value={title}
                                                                    >
                                                                        {({selected, active}) => (
                                                                            <>
                                                    <span
                                                        className={`block truncate ${
                                                            selected ? 'font-medium' : 'font-normal'
                                                        }`}
                                                    >
                                                {title.name}
                                                    </span>
                                                                                {selected ? (
                                                                                    <span
                                                                                        className={`absolute inset-y-0 left-0 flex items-center pl-3 ${
                                                                                            active ? 'text-white' : 'text-blue-500'
                                                                                        }`}
                                                                                    >
                                                    <CheckIcon className="h-5 w-5" aria-hidden="true"/>
                                                    </span>
                                                                                ) : null}
                                                                            </>
                                                                        )}
                                                                    </Combobox.Option>
                                                                ))
                                                            )}
                                                        </Combobox.Options>
                                                    </Transition>
                                                </div>
                                            </Combobox>
                                        )
                                    }

                                </div>

                                <div>
                                    <label htmlFor="first-name"
                                           className="block mb-2 text-sm text-gray-600 dark:text-gray-200">
                                        ชื่อ
                                    </label>
                                    <input
                                        type="text"
                                        name="first-name"
                                        id="first-name"
                                        autoComplete="given-name"
                                        required
                                        className="block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-900 dark:text-gray-300 dark:border-gray-700 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500"
                                        value={firstName}
                                        onChange={(e) => setFirstName(e.target.value)}
                                    />

                                </div>

                                <div>
                                    <label htmlFor="family-name"
                                           className="block mb-2 text-sm text-gray-600 dark:text-gray-200">
                                        นามสกุล
                                    </label>
                                    <input
                                        type="text"
                                        name="family-name"
                                        id="family-name"
                                        autoComplete="family-name"
                                        required
                                        className="block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-900 dark:text-gray-300 dark:border-gray-700 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500"
                                        value={lastName}
                                        onChange={(e) => setLastName(e.target.value)}
                                    />
                                </div>

                                <div>
                                    <label htmlFor="email"
                                           className="block mb-2 text-sm text-gray-600 dark:text-gray-200">อีเมล {error === "emailExist" ?
                                        <span className="text-sm text-red-500">*</span> : ""}</label>
                                    <input
                                        type="email"
                                        name="email"
                                        id="email"
                                        autoComplete="email"
                                        required
                                        placeholder="example@email.com"
                                        className={`block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border rounded-md dark:bg-gray-900 dark:placeholder-gray-600 dark:text-gray-300 focus:ring-1 focus:outline-none ${error === "emailExist" ? "focus-within:ring-red-500 border-red-500 dark:border-red-900 focus:border-red-700 dark:focus:border-red-700" : "border-gray-300 dark:border-gray-700 focus:border-blue-500 dark:focus:border-blue-500"}`}
                                        value={email}
                                        onChange={(e) => setEmail(e.target.value)}
                                    />
                                    {error === "emailExist" ?
                                        <p className="text-sm text-red-500">อีเมลนี้ ลงทะเบียนแล้ว</p> : ""}
                                </div>

                                <div>
                                    <label htmlFor="username"
                                           className="block mb-2 text-sm text-gray-600 dark:text-gray-200">
                                        ชื่อผู้ใช้งาน (Username) {error === "userExist" ?
                                        <span className="text-sm text-red-500">*</span> : ""}
                                    </label>
                                    <input
                                        type="text"
                                        name="username"
                                        id="username"
                                        autoComplete="username"
                                        required
                                        className={`block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border rounded-md dark:bg-gray-900 dark:placeholder-gray-600 dark:text-gray-300 focus:ring-1 focus:outline-none ${error === "userExist" ? "focus-within:ring-red-500 border-red-500 dark:border-red-900 focus:border-red-700 dark:focus:border-red-700" : "border-gray-300 dark:border-gray-700 focus:border-blue-500 dark:focus:border-blue-500"}`}
                                        value={username}
                                        onChange={(e) => setUserName(e.target.value)}
                                    />
                                    {error === "userExist" ?
                                        <p className="text-sm text-red-500">ชื่อผู้ใช้งาน ลงทะเบียนแล้ว</p> : ""}
                                </div>

                                <div>
                                    <label htmlFor="password"
                                           className="block mb-2 text-sm text-gray-600 dark:text-gray-200">รหัสผ่าน
                                        (Password)</label>
                                    <input
                                        type="password"
                                        name="password"
                                        id="password"
                                        required
                                        minLength="8"
                                        className="block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-900 dark:text-gray-300 dark:border-gray-700 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500"
                                        value={password}
                                        onChange={(e) => setPassword(e.target.value)}

                                    />
                                </div>

                                <div>
                                    <label htmlFor="confirm-password"
                                           className="block mb-2 text-sm text-gray-600 dark:text-gray-200">ยืนยันรหัสผ่าน
                                        (Confirm password) {error === "PasswordNotMatch" ?
                                            <span className="text-sm text-red-500">*</span> : ""}</label>
                                    <input
                                        type="password"
                                        name="password"
                                        id="confirm-password"
                                        required
                                        minLength="8"
                                        className={`block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border rounded-md dark:bg-gray-900 dark:placeholder-gray-600 dark:text-gray-300 focus:ring-1 focus:outline-none ${error === "PasswordNotMatch" ? "focus-within:ring-red-500 border-red-500 dark:border-red-900 focus:border-red-700 dark:focus:border-red-700" : "border-gray-300 dark:border-gray-700 focus:border-blue-500 dark:focus:border-blue-500"}`}
                                        value={confirmPassword}
                                        onChange={(e) => setConfirmPassword(e.target.value)}
                                    />
                                    {error === "PasswordNotMatch" ?
                                        <p className="text-sm text-red-500">รหัสผ่านไม่ตรงกัน</p> : ""}
                                </div>

                                <button
                                    type="submit"
                                    className="w-full px-6 py-3 tracking-wide text-sm text-white transition-colors duration-300 transform bg-blue-600 rounded-md hover:bg-blue-700 focus:outline-none focus:bg-blue-700 focus:ring focus:ring-blue-500 focus:ring-opacity-50">
                                    ลงทะเบียน
                                </button>

                                <p className="mb-4 md:mt-4 text-sm text-center text-gray-400">มีบัญชีแล้ว? <Link
                                    to={redirect ? `/login?redirect=${redirect}` : "/login"}
                                    className="text-blue-600 focus:outline-none focus:underline hover:underline">ลงชื่อเข้าใช้</Link>.
                                </p>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default Register;