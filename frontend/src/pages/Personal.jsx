import "./styles/react-datepicker-custom.css";
import React, {Fragment, useEffect, useState} from "react";
import {Combobox, Transition} from "@headlessui/react";
import {
    CalendarDaysIcon,
    CheckIcon,
    ChevronUpDownIcon,
    ExclamationCircleIcon
} from "@heroicons/react/20/solid";
import {useDispatch, useSelector} from "react-redux";
import {listProvince} from "../redux/actions/provinceActions";
import {toast} from "react-toastify";
import {listNationality} from "../redux/actions/nationActions";
import DatePicker, {registerLocale} from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import th from 'date-fns/locale/th';
import {getDay} from "date-fns";
import moment from "moment";
import {listOccupation} from "../redux/actions/occupationActions";
import {listMarriage} from "../redux/actions/marriageActions";
import {listEducation} from "../redux/actions/educationActions";
import Header from "../components/Header.jsx";
import {listDisease} from "../redux/actions/diseaseActions";
import {listHospital} from "../redux/actions/hospitalActions";
import {useNavigate} from "react-router-dom";

registerLocale('th', th);

function Personal() {

    const userLogin = useSelector((state) => state.userLogin);
    const {userInfo} = userLogin;

    const [dob, setDob] = useState(new Date());
    const dateNow = moment(new Date());
    const calAge = moment.duration(dateNow.diff(moment(dob)));

    const [ageYear, setAgeYear] = useState("0");
    const [ageMonth, setAgeMonth] = useState("0");

    const isWeekday = (date) => {
        const day = getDay(date);
        return day !== 0 && day !== 6;
    };


    const dispatch = useDispatch();
    const navigate = useNavigate();


    const hospitalList = useSelector((state) => state.hospitalList);
    const {error: errorHospitals, hospitals} = hospitalList;
    const provinceList = useSelector((state) => state.provinceList);
    const {error: errorProvinces, provinces} = provinceList;
    const nationList = useSelector((state) => state.nationList);
    const {error: errorNations, nations} = nationList;
    const occupationList = useSelector((state) => state.occupationList)
    const {error: errorOccpations, occupations} = occupationList;
    const marriageList = useSelector((state) => state.marriageList)
    const {error: errorMarraiges, marriages} = marriageList;
    const educationList = useSelector((state) => state.educationList)
    const {error: errorEducations, educations} = educationList;
    const diseaseList = useSelector((state) => state.diseaseList)
    const {error: errorDiseases, diseases} = diseaseList;


    const [selectedHospital, setSelectedHospital] = useState(hospitals);
    const [queryHospital, setQueryHospital] = useState("");
    const [selectedProvince, setSeletedProvince] = useState(provinces);
    const [queryProvice, setQueryProvince] = useState("");
    const [selectedNation, setSeletedNation] = useState(nations);
    const [queryNation, setQueryNation] = useState("");
    const [selectedRace, setSeletedRace] = useState(nations);
    const [selectedOccupation, setSeletedOccupation] = useState(occupations);
    const [queryOccupation, setQueryOccupation] = useState("");
    const [selectedMarriage, setSeletedMarriage] = useState(marriages);
    const [queryMarriage, setQueryMarriage] = useState("");
    const [selectedEducation, setSeletedEducation] = useState(educations);
    const [queryEducation, setQueryEducation] = useState("");
    const [selectedDisease, setSelectedDisease] = useState(diseases)
    const [queryDisease, setQueryDisease] = useState('')

    const filteredHospital =
        queryHospital === ''
            ? hospitals
            : hospitals.filter((hospital) =>
                hospitals.name
                    .toLowerCase()
                    .replace(/\s+/g, '')
                    .includes(queryHospital.toLowerCase().replace(/\s+/g, ''))
            )

    const filteredDisease =
        queryDisease === ''
            ? diseases
            : diseases.filter((disease) =>
                disease.name
                    .toLowerCase()
                    .replace(/\s+/g, '')
                    .includes(queryDisease.toLowerCase().replace(/\s+/g, ''))
            )


    const filteredProvince =
        queryProvice === ''
            ? provinces
            : provinces.filter((province) =>
                province.name
                    .toLowerCase()
                    .replace(/\s+/g, '')
                    .includes(queryProvice.toLowerCase().replace(/\s+/g, ''))
            )

    const filteredNation =
        queryNation === ''
            ? nations
            : nations.filter((nation) =>
                nation.name
                    .toLowerCase()
                    .replace(/\s+/g, '')
                    .includes(queryNation.toLowerCase().replace(/\s+/g, ''))
            )

    const filteredOccupation =
        queryOccupation === ''
            ? occupations
            : occupations.filter((occupation) =>
                occupation.name
                    .toLowerCase()
                    .replace(/\s+/g, '')
                    .includes(queryOccupation.toLowerCase().replace(/\s+/g, ''))
            )

    const filteredMarriage =
        queryMarriage === ''
            ? marriages
            : marriages.filter((marriage) =>
                marriage.name
                    .toLowerCase()
                    .replace(/\s+/g, '')
                    .includes(queryMarriage.toLowerCase().replace(/\s+/g, ''))
            )

    const filteredEducation =
        queryEducation === ''
            ? educations
            : educations.filter((education) =>
                education.name
                    .toLowerCase()
                    .replace(/\s+/g, '')
                    .includes(queryEducation.toLowerCase().replace(/\s+/g, ''))
            )


    useEffect(() => {
        dispatch(listHospital());
        dispatch(listProvince());
        dispatch(listNationality());
        dispatch(listOccupation());
        dispatch(listMarriage());
        dispatch(listEducation());
        dispatch(listDisease());

        if (calAge.years() > 0) {
            setAgeYear(calAge.years().toString());
        } else if (calAge.months() > 0) {
            setAgeMonth(calAge.months().toString());
        } else {
            setAgeYear("0");
            setAgeMonth("0");
        }

        if (userInfo) {
            const hospital = {
                code: userInfo.hospital.code,
                name: userInfo.hospital.name
            }
            setSelectedHospital(hospital)
        }else {
            navigate("/login?redirect=/");
        }

    }, [dispatch, dob, userInfo])


    return (
        <>
            <Header/>

            {errorProvinces && toast.error(`${errorProvinces}`, {toastId: ""})}
            {errorNations && toast.error(`${errorNations}`, {toastId: ""})}
            {errorOccpations && toast.error(`${errorOccpations}`, {toastId: ""})}
            {errorDiseases && toast.error(`${errorDiseases}`, {toastId: ""})}
            <section className="mx-auto max-w-7xl px-2 py-10 sm:px-6 lg:px-8 dark:bg-gray-900">
                <div className="px-4 sm:px-0">
                    <h3 className="text-xl font-medium leading-6 text-gray-700 dark:text-white">แบบเก็บข้อมูล (Case
                        record form):
                        การศึกษาประสิทธิผลของแคนนาบินอยด์และ
                        การประเมินคุณภาพชีวิตในผู้ป่วยมะเร็งระยะแพร่กระจาย
                    </h3>
                    <p className="mt-2 text-md text-gray-600 dark:text-gray-200 dark:text-gray-400">ส่วนที่ 1 :
                        ข้อมูลทั่วไป</p>
                </div>
                {/*<div className="md:grid md:grid-cols-2 md:gap-6">*/}
                {/*<div className="mt-5 md:col-span-2 md:mt-5">*/}
                <form action="#" method="POST">
                    <div className="overflow-visible shadow rounded-md mt-5">
                        <div
                            className="bg-white rounded-md rounded-b-none shadow-md dark:bg-gray-800 px-4 py-5 sm:p-6">
                            <div className="grid grid-cols-6 gap-6">
                                <div className="col-span-2 sm:col-span-2">
                                    <label htmlFor="study-site"
                                           className="block text-sm text-gray-600 dark:text-gray-200">
                                        Study site:
                                    </label>
                                    {/*<input*/}
                                    {/*    type="text"*/}
                                    {/*    name="study-site"*/}
                                    {/*    id="study-site"*/}
                                    {/*    autoComplete="study-site"*/}
                                    {/*    required*/}
                                    {/*    className="block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"*/}
                                    {/*    value={studySite}*/}
                                    {/*    onChange={(e) => setStudySite(e.target.value)}*/}
                                    {/*/>*/}
                                    {
                                        errorHospitals ? (
                                            <div className="flex items-center text-red-500 mt-2">

                                                <ExclamationCircleIcon className="h-5 w-5 mr-2"
                                                                       aria-hidden="true"/>
                                                ไม่สามารถโหลดข้อมูลได้
                                            </div>
                                        ) : (
                                            <Combobox value={selectedHospital}
                                                      onChange={setSelectedHospital}>
                                                <div className="relative mt-1">
                                                    <div
                                                        className="relative w-full cursor-default rounded-md">
                                                        <Combobox.Input
                                                            className="pr-10 leading-5 block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-900 dark:text-gray-300 dark:border-gray-700 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500"
                                                            displayValue={(hospital) => hospital.code + " : " + hospital.name}
                                                            onChange={(event) => setQueryHospital(event.target.value)}
                                                        />
                                                        <Combobox.Button
                                                            className="absolute inset-y-0 right-0 flex items-center pr-2">
                                                            <ChevronUpDownIcon
                                                                className="h-5 w-5 text-gray-400"
                                                                aria-hidden="true"
                                                            />
                                                        </Combobox.Button>
                                                    </div>
                                                    <Transition
                                                        as={Fragment}
                                                        leave="transition ease-in duration-100"
                                                        leaveFrom="opacity-100"
                                                        leaveTo="opacity-0"
                                                        afterLeave={() => setQueryHospital('')}
                                                    >
                                                        <Combobox.Options
                                                            className="absolute z-50 mt-1 max-h-60 w-full overflow-auto py-1 text-base shadow-lg text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-900 dark:text-gray-300 dark:border-gray-700 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                                                            {filteredHospital.length === 0 && queryHospital !== '' ? (
                                                                <div
                                                                    className="relative cursor-default select-none py-2 px-4 text-gray-700">
                                                                    Nothing found.
                                                                </div>
                                                            ) : (
                                                                filteredHospital.map((hospital) => (
                                                                    <Combobox.Option
                                                                        key={hospital._id}
                                                                        className={({active}) =>
                                                                            `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                                                                active ? 'bg-blue-500 text-white' : 'text-gray-700 dark:text-gray-300'
                                                                            }`
                                                                        }
                                                                        value={hospital}
                                                                    >
                                                                        {({selected, active}) => (
                                                                            <>
                                                    <span
                                                        className={`block truncate ${
                                                            selected ? 'font-medium' : 'font-normal'
                                                        }`}
                                                    >
                                                {hospital.code + " : " + hospital.name}
                                                    </span>
                                                                                {selected ? (
                                                                                    <span
                                                                                        className={`absolute inset-y-0 left-0 flex items-center pl-3 ${
                                                                                            active ? 'text-white' : 'text-blue-500'
                                                                                        }`}
                                                                                    >
                                                    <CheckIcon className="h-5 w-5" aria-hidden="true"/>
                                                    </span>
                                                                                ) : null}
                                                                            </>
                                                                        )}
                                                                    </Combobox.Option>
                                                                ))
                                                            )}
                                                        </Combobox.Options>
                                                    </Transition>
                                                </div>
                                            </Combobox>
                                        )
                                    }
                                </div>
                                {/*<div className="col-span-4 sm:col-span-1">*/}
                                {/*    <label htmlFor="study-site-name"*/}
                                {/*           className="block text-sm text-gray-600 dark:text-gray-200">*/}
                                {/*        &nbsp;*/}
                                {/*    </label>*/}
                                {/*    <input*/}
                                {/*        type="text"*/}
                                {/*        name="study-site-name"*/}
                                {/*        id="study-site-name"*/}
                                {/*        autoComplete="study-site-name"*/}
                                {/*        required*/}
                                {/*        className="block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"*/}
                                {/*    />*/}
                                {/*</div>*/}
                                <div className="col-span-6 sm:col-span-1">
                                    <label htmlFor="study-id"
                                           className="block text-sm text-gray-600 dark:text-gray-200">
                                        Study id:
                                    </label>
                                    <input
                                        type="number"
                                        name="study-id"
                                        id="study-id"
                                        autoComplete="study-id"
                                        required
                                        className="block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                    />
                                </div>
                                <div className="col-span-6 sm:col-span-1">
                                    <label htmlFor="study-group"
                                           className="block text-sm text-gray-600 dark:text-gray-200">
                                        Study group:
                                    </label>
                                    <input
                                        type="number"
                                        name="study-group"
                                        id="study-group"
                                        autoComplete="study-group"
                                        required
                                        className="block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                    />
                                </div>
                                <div className="col-span-6 sm:col-span-2">
                                    <label htmlFor="phone-number"
                                           className="block text-sm text-gray-600 dark:text-gray-200">
                                        เบอร์โทรศัพท์
                                    </label>
                                    <input
                                        type="tel"
                                        name="phone-number"
                                        id="phone-number"
                                        placeholder="099-9999999"
                                        required
                                        // autoComplete="study-site"
                                        className="block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                    />
                                </div>


                                <div className="col-span-6 sm:col-span-2">
                                    <label htmlFor="birthday"
                                           className="block text-sm text-gray-600 dark:text-gray-200">
                                        วันเกิด
                                    </label>

                                    <div className="relative flex items-center">
                                        <DatePicker selected={dob} onChange={(date) => setDob(date)}
                                                    locale="th"
                                                    peekNextMonth
                                                    showMonthDropdown
                                                    showYearDropdown
                                                    disabledKeyboardNavigation
                                                    dropdownMode="select"
                                                    dateFormat="dd/MM/yyyy"
                                                    filterDate={isWeekday}
                                                    className="block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    placeholderText="วัน/เดือน/ปี ค.ศ."
                                        />
                                        <div
                                            className="flex absolute inset-y-0 right-0 items-center pr-3 mt-1 pointer-events-none">
                                            <CalendarDaysIcon
                                                className="h-6 w-6 text-gray-500 dark:text-gray-400 sm:h-5 sm:w-5"
                                                aria-hidden="true"/>
                                        </div>
                                    </div>


                                </div>
                                <div className="col-span-3 sm:col-span-1">
                                    <label htmlFor="age-year"
                                           className="block text-sm text-gray-600 dark:text-gray-200">
                                        อายุ
                                    </label>
                                    <div className="flex">
                                        <input
                                            type="text"
                                            name="age-year"
                                            id="age-year"
                                            className="disabled:bg-gray-50 dark:disabled:bg-gray-800 flex-1 rounded-none rounded-l-md block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                            disabled
                                            value={ageYear}
                                            onChange={(e) => setAgeYear(e.target.value)}
                                        />
                                        <span
                                            className="mt-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 px-3 text-sm text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          ปี
                        </span>
                                    </div>
                                </div>
                                <div className="col-span-3 sm:col-span-1">
                                    <label htmlFor="age-month"
                                           className="block text-sm text-gray-600 dark:text-gray-200">
                                        &nbsp;
                                    </label>
                                    <div className="flex">
                                        <input
                                            type="text"
                                            name="age-month"
                                            id="age-month"
                                            className="disabled:bg-gray-50 dark:disabled:bg-gray-800 flex-1 rounded-none rounded-l-md block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                            disabled
                                            value={ageMonth}
                                            onChange={(e) => setAgeMonth(e.target.value)}
                                        />
                                        <span
                                            className="mt-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 px-3 text-sm text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          เดือน
                        </span>
                                    </div>
                                </div>
                                <div className="col-span-6 sm:col-span-2">
                                    <fieldset>
                                        <legend
                                            className="contents text-sm text-gray-600 dark:text-gray-200">เพศ
                                        </legend>
                                        <div className="flex flex-auto items-center mt-2 xl:mt-3">
                                            <div className="flex flex-auto items-center">
                                                <input
                                                    id="sex-male"
                                                    name="sex"
                                                    type="radio"
                                                    className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    value="1"
                                                />
                                                <label htmlFor="sex-male"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    ชาย
                                                </label>
                                            </div>
                                            <div className="flex flex-auto items-center">
                                                <input
                                                    id="sex-female"
                                                    name="sex"
                                                    type="radio"
                                                    className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    value="2"
                                                />
                                                <label htmlFor="sex-female"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    หญิง
                                                </label>
                                            </div>

                                        </div>
                                    </fieldset>


                                </div>
                                <div className="col-span-6 sm:col-span-2">
                                    <label htmlFor="province"
                                           className="block text-sm text-gray-600 dark:text-gray-200">
                                        จังหวัด
                                    </label>
                                    {
                                        errorProvinces ? (
                                            <div className="flex items-center text-red-500 mt-2">

                                                <ExclamationCircleIcon className="h-5 w-5 mr-2"
                                                                       aria-hidden="true"/>
                                                ไม่สามารถโหลดข้อมูลได้
                                            </div>
                                        ) : (
                                            <Combobox value={selectedProvince} onChange={setSeletedProvince}>
                                                <div className="relative mt-1">
                                                    <div
                                                        className="relative w-full cursor-default rounded-md">
                                                        <Combobox.Input
                                                            className="pr-10 leading-5 block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                            displayValue={(province) => province.name}
                                                            onChange={(event) => setQueryProvince(event.target.value)}
                                                        />
                                                        <Combobox.Button
                                                            className="absolute inset-y-0 right-0 flex items-center pr-2">
                                                            <ChevronUpDownIcon
                                                                className="h-5 w-5 text-gray-400"
                                                                aria-hidden="true"
                                                            />
                                                        </Combobox.Button>
                                                    </div>
                                                    <Transition
                                                        as={Fragment}
                                                        leave="transition ease-in duration-100"
                                                        leaveFrom="opacity-100"
                                                        leaveTo="opacity-0"
                                                        afterLeave={() => setQueryProvince('')}
                                                    >
                                                        <Combobox.Options
                                                            className="absolute z-50 mt-1 max-h-60 w-full overflow-auto py-1 text-base shadow-lg text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                                                            {filteredProvince.length === 0 && queryProvice !== '' ? (
                                                                <div
                                                                    className="relative cursor-default select-none py-2 px-4 text-gray-700 dark:text-gray-300">
                                                                    Nothing found.
                                                                </div>
                                                            ) : (
                                                                filteredProvince.map((province) => (
                                                                    <Combobox.Option
                                                                        key={province._id}
                                                                        className={({active}) =>
                                                                            `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                                                                active ? 'bg-blue-500 text-white' : 'text-gray-700 dark:text-gray-300'
                                                                            }`
                                                                        }
                                                                        value={province}

                                                                    >
                                                                        {({selected, active}) => (
                                                                            <>
                        <span
                            className={`block truncate ${
                                selected ? 'font-medium' : 'font-normal'
                            }`}
                        >
                          {province.name}
                        </span>
                                                                                {selected ? (
                                                                                    <span
                                                                                        className={`absolute inset-y-0 left-0 flex items-center pl-3 ${
                                                                                            active ? 'text-white' : 'text-blue-500'
                                                                                        }`}
                                                                                    >
                            <CheckIcon className="h-5 w-5" aria-hidden="true"/>
                          </span>
                                                                                ) : null}
                                                                            </>
                                                                        )}
                                                                    </Combobox.Option>
                                                                ))
                                                            )}
                                                        </Combobox.Options>
                                                    </Transition>
                                                </div>
                                            </Combobox>
                                        )
                                    }

                                </div>
                                <div className="col-span-6 sm:col-span-1">
                                    <label htmlFor="nation"
                                           className="block text-sm text-gray-600 dark:text-gray-200">
                                        เชื้อชาติ
                                    </label>
                                    {
                                        errorNations ? (
                                                <div className="flex items-center text-red-500 mt-2">

                                                    <ExclamationCircleIcon className="h-5 w-5 mr-2"
                                                                           aria-hidden="true"/>
                                                    ไม่สามารถโหลดข้อมูลได้
                                                </div>
                                            ) :
                                            (
                                                <Combobox value={selectedNation} onChange={setSeletedNation}>
                                                    <div className="relative mt-1">
                                                        <div
                                                            className="relative w-full cursor-default rounded-md">
                                                            <Combobox.Input
                                                                className="pr-10 leading-5 block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                displayValue={(nation) => nation.name}
                                                                onChange={(event) => setQueryNation(event.target.value)}
                                                            />
                                                            <Combobox.Button
                                                                className="absolute inset-y-0 right-0 flex items-center pr-2">
                                                                <ChevronUpDownIcon
                                                                    className="h-5 w-5 text-gray-400"
                                                                    aria-hidden="true"
                                                                />
                                                            </Combobox.Button>
                                                        </div>
                                                        <Transition
                                                            as={Fragment}
                                                            leave="transition ease-in duration-100"
                                                            leaveFrom="opacity-100"
                                                            leaveTo="opacity-0"
                                                            afterLeave={() => setQueryNation('')}
                                                        >
                                                            <Combobox.Options
                                                                className="absolute z-50 mt-1 max-h-60 w-full overflow-auto py-1 text-base shadow-lg text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                                                                {filteredNation.length === 0 && queryNation !== '' ? (
                                                                    <div
                                                                        className="relative cursor-default select-none py-2 px-4 text-gray-700 dark:text-gray-300">
                                                                        Nothing found.
                                                                    </div>
                                                                ) : (
                                                                    filteredNation.map((nation) => (
                                                                        <Combobox.Option
                                                                            key={nation._id}
                                                                            className={({active}) =>
                                                                                `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                                                                    active ? 'bg-blue-500 text-white' : 'text-gray-700 dark:text-gray-300'
                                                                                }`
                                                                            }
                                                                            value={nation}

                                                                        >
                                                                            {({selected, active}) => (
                                                                                <>
                        <span
                            className={`block truncate ${
                                selected ? 'font-medium' : 'font-normal'
                            }`}
                        >
                          {nation.name}
                        </span>
                                                                                    {selected ? (
                                                                                        <span
                                                                                            className={`absolute inset-y-0 left-0 flex items-center pl-3 ${
                                                                                                active ? 'text-white' : 'text-blue-500'
                                                                                            }`}
                                                                                        >
                            <CheckIcon className="h-5 w-5" aria-hidden="true"/>
                          </span>
                                                                                    ) : null}
                                                                                </>
                                                                            )}
                                                                        </Combobox.Option>
                                                                    ))
                                                                )}
                                                            </Combobox.Options>
                                                        </Transition>
                                                    </div>
                                                </Combobox>

                                            )
                                    }

                                </div>
                                <div className="col-span-6 sm:col-span-1">
                                    {/*<div className="fixed top-16 w-72">*/}
                                    <label htmlFor="race"
                                           className="block text-sm text-gray-600 dark:text-gray-200">
                                        สัญชาติ
                                    </label>
                                    {
                                        errorNations ? (
                                            <div className="flex items-center text-red-500 mt-2">

                                                <ExclamationCircleIcon className="h-5 w-5 mr-2"
                                                                       aria-hidden="true"/>
                                                ไม่สามารถโหลดข้อมูลได้
                                            </div>
                                        ) : (
                                            <Combobox value={selectedRace} onChange={setSeletedRace}>
                                                <div className="relative mt-1">
                                                    <div
                                                        className="relative w-full cursor-default rounded-md">
                                                        <Combobox.Input
                                                            className="pr-10 leading-5 block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                            displayValue={(nation) => nation.name}
                                                            onChange={(event) => setQueryNation(event.target.value)}
                                                        />
                                                        <Combobox.Button
                                                            className="absolute inset-y-0 right-0 flex items-center pr-2">
                                                            <ChevronUpDownIcon
                                                                className="h-5 w-5 text-gray-400"
                                                                aria-hidden="true"
                                                            />
                                                        </Combobox.Button>
                                                    </div>
                                                    <Transition
                                                        as={Fragment}
                                                        leave="transition ease-in duration-100"
                                                        leaveFrom="opacity-100"
                                                        leaveTo="opacity-0"
                                                        afterLeave={() => setQueryNation('')}
                                                    >
                                                        <Combobox.Options
                                                            className="absolute z-50 mt-1 max-h-60 w-full overflow-auto py-1 text-base shadow-lg text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                                                            {filteredNation.length === 0 && queryNation !== '' ? (
                                                                <div
                                                                    className="relative cursor-default select-none py-2 px-4 text-gray-700 dark:text-gray-300">
                                                                    Nothing found.
                                                                </div>
                                                            ) : (
                                                                filteredNation.map((nation) => (
                                                                    <Combobox.Option
                                                                        key={nation._id}
                                                                        className={({active}) =>
                                                                            `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                                                                active ? 'bg-blue-500 text-white' : 'text-gray-700 dark:text-gray-300'
                                                                            }`
                                                                        }
                                                                        value={nation}

                                                                    >
                                                                        {({selected, active}) => (
                                                                            <>
                        <span
                            className={`block truncate ${
                                selected ? 'font-medium' : 'font-normal'
                            }`}
                        >
                          {nation.name}
                        </span>
                                                                                {selected ? (
                                                                                    <span
                                                                                        className={`absolute inset-y-0 left-0 flex items-center pl-3 ${
                                                                                            active ? 'text-white' : 'text-blue-500'
                                                                                        }`}
                                                                                    >
                            <CheckIcon className="h-5 w-5" aria-hidden="true"/>
                          </span>
                                                                                ) : null}
                                                                            </>
                                                                        )}
                                                                    </Combobox.Option>
                                                                ))
                                                            )}
                                                        </Combobox.Options>
                                                    </Transition>
                                                </div>
                                            </Combobox>)}
                                </div>
                                <div className="col-span-6 sm:col-span-2">
                                    <label htmlFor="occupation"
                                           className="block text-sm text-gray-600 dark:text-gray-200">
                                        อาชีพ
                                    </label>
                                    {
                                        errorOccpations ? (
                                            <div className="flex items-center text-red-500 mt-2">

                                                <ExclamationCircleIcon className="h-5 w-5 mr-2"
                                                                       aria-hidden="true"/>
                                                ไม่สามารถโหลดข้อมูลได้
                                            </div>
                                        ) : (
                                            <Combobox value={selectedOccupation}
                                                      onChange={setSeletedOccupation}>
                                                <div className="relative mt-1">
                                                    <div
                                                        className="relative w-full cursor-default rounded-md">
                                                        <Combobox.Input
                                                            className="pr-10 leading-5 block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                            displayValue={(occupation) => occupation.name}
                                                            onChange={(event) => setQueryOccupation(event.target.value)}
                                                        />
                                                        <Combobox.Button
                                                            className="absolute inset-y-0 right-0 flex items-center pr-2">
                                                            <ChevronUpDownIcon
                                                                className="h-5 w-5 text-gray-400"
                                                                aria-hidden="true"
                                                            />
                                                        </Combobox.Button>
                                                    </div>
                                                    <Transition
                                                        as={Fragment}
                                                        leave="transition ease-in duration-100"
                                                        leaveFrom="opacity-100"
                                                        leaveTo="opacity-0"
                                                        afterLeave={() => setQueryOccupation('')}
                                                    >
                                                        <Combobox.Options
                                                            className="absolute z-50 mt-1 max-h-60 w-full overflow-auto py-1 text-base shadow-lg text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                                                            {filteredOccupation.length === 0 && queryOccupation !== '' ? (
                                                                <div
                                                                    className="relative cursor-default select-none py-2 px-4 text-gray-700 dark:text-gray-300">
                                                                    Nothing found.
                                                                </div>
                                                            ) : (
                                                                filteredOccupation.map((occupation) => (
                                                                    <Combobox.Option
                                                                        key={occupation._id}
                                                                        className={({active}) =>
                                                                            `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                                                                active ? 'bg-blue-500 text-white' : 'text-gray-700 dark:text-gray-300'
                                                                            }`
                                                                        }
                                                                        value={occupation}

                                                                    >
                                                                        {({selected, active}) => (
                                                                            <>
                        <span
                            className={`block truncate ${
                                selected ? 'font-medium' : 'font-normal'
                            }`}
                        >
                          {occupation.name}
                        </span>
                                                                                {selected ? (
                                                                                    <span
                                                                                        className={`absolute inset-y-0 left-0 flex items-center pl-3 ${
                                                                                            active ? 'text-white' : 'text-blue-500'
                                                                                        }`}
                                                                                    >
                            <CheckIcon className="h-5 w-5" aria-hidden="true"/>
                          </span>
                                                                                ) : null}
                                                                            </>
                                                                        )}
                                                                    </Combobox.Option>
                                                                ))
                                                            )}
                                                        </Combobox.Options>
                                                    </Transition>
                                                </div>
                                            </Combobox>
                                        )
                                    }

                                </div>
                                <div className="col-span-6 sm:col-span-2">
                                    <label htmlFor="other-occupation"
                                           className="block text-sm text-gray-600 dark:text-gray-200">
                                        อาชีพ อื่นๆ ระบุ
                                    </label>
                                    <input
                                        type="text"
                                        name="other-occupation"
                                        id="other-occupation"
                                        autoComplete="given-name"
                                        className="block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                    />
                                </div>
                                <div className="col-span-6 sm:col-span-2">
                                    <label htmlFor="mstatus"
                                           className="block text-sm text-gray-600 dark:text-gray-200">
                                        สถานะภาพสมรส
                                    </label>
                                    {
                                        errorMarraiges ? (
                                            <div className="flex items-center text-red-500 mt-2">

                                                <ExclamationCircleIcon className="h-5 w-5 mr-2"
                                                                       aria-hidden="true"/>
                                                ไม่สามารถโหลดข้อมูลได้
                                            </div>
                                        ) : (
                                            <Combobox value={selectedMarriage}
                                                      onChange={setSeletedMarriage}>
                                                <div className="relative mt-1">
                                                    <div
                                                        className="relative w-full cursor-default rounded-md">
                                                        <Combobox.Input
                                                            className="pr-10 leading-5 block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                            displayValue={(marriage) => marriage.name}
                                                            onChange={(event) => setQueryMarriage(event.target.value)}
                                                        />
                                                        <Combobox.Button
                                                            className="absolute inset-y-0 right-0 flex items-center pr-2">
                                                            <ChevronUpDownIcon
                                                                className="h-5 w-5 text-gray-400"
                                                                aria-hidden="true"
                                                            />
                                                        </Combobox.Button>
                                                    </div>
                                                    <Transition
                                                        as={Fragment}
                                                        leave="transition ease-in duration-100"
                                                        leaveFrom="opacity-100"
                                                        leaveTo="opacity-0"
                                                        afterLeave={() => setQueryMarriage('')}
                                                    >
                                                        <Combobox.Options
                                                            className="absolute z-50 mt-1 max-h-60 w-full overflow-auto py-1 text-base shadow-lg text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                                                            {filteredMarriage.length === 0 && queryMarriage !== '' ? (
                                                                <div
                                                                    className="relative cursor-default select-none py-2 px-4 text-gray-700 dark:text-gray-300">
                                                                    Nothing found.
                                                                </div>
                                                            ) : (
                                                                filteredMarriage.map((marriage) => (
                                                                    <Combobox.Option
                                                                        key={marriage._id}
                                                                        className={({active}) =>
                                                                            `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                                                                active ? 'bg-blue-500 text-white' : 'text-gray-700 dark:text-gray-300'
                                                                            }`
                                                                        }
                                                                        value={marriage}

                                                                    >
                                                                        {({selected, active}) => (
                                                                            <>
                        <span
                            className={`block truncate ${
                                selected ? 'font-medium' : 'font-normal'
                            }`}
                        >
                          {marriage.name}
                        </span>
                                                                                {selected ? (
                                                                                    <span
                                                                                        className={`absolute inset-y-0 left-0 flex items-center pl-3 ${
                                                                                            active ? 'text-white' : 'text-blue-500'
                                                                                        }`}
                                                                                    >
                            <CheckIcon className="h-5 w-5" aria-hidden="true"/>
                          </span>
                                                                                ) : null}
                                                                            </>
                                                                        )}
                                                                    </Combobox.Option>
                                                                ))
                                                            )}
                                                        </Combobox.Options>
                                                    </Transition>
                                                </div>
                                            </Combobox>
                                        )
                                    }
                                </div>
                                <div className="col-span-6 sm:col-span-2">
                                    <label htmlFor="education"
                                           className="block text-sm text-gray-600 dark:text-gray-200">
                                        การศึกษา
                                    </label>
                                    {
                                        errorEducations ? (
                                            <div className="flex items-center text-red-500 mt-2">

                                                <ExclamationCircleIcon className="h-5 w-5 mr-2"
                                                                       aria-hidden="true"/>
                                                ไม่สามารถโหลดข้อมูลได้
                                            </div>
                                        ) : (
                                            <Combobox value={selectedEducation}
                                                      onChange={setSeletedEducation}>
                                                <div className="relative mt-1">
                                                    <div
                                                        className="relative w-full cursor-default rounded-md">
                                                        <Combobox.Input
                                                            className="pr-10 leading-5 block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                            displayValue={(education) => education.name}
                                                            onChange={(event) => setQueryEducation(event.target.value)}
                                                        />
                                                        <Combobox.Button
                                                            className="absolute inset-y-0 right-0 flex items-center pr-2">
                                                            <ChevronUpDownIcon
                                                                className="h-5 w-5 text-gray-400"
                                                                aria-hidden="true"
                                                            />
                                                        </Combobox.Button>
                                                    </div>
                                                    <Transition
                                                        as={Fragment}
                                                        leave="transition ease-in duration-100"
                                                        leaveFrom="opacity-100"
                                                        leaveTo="opacity-0"
                                                        afterLeave={() => setQueryEducation('')}
                                                    >
                                                        <Combobox.Options
                                                            className="absolute z-50 mt-1 max-h-60 w-full overflow-auto py-1 text-base shadow-lg text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                                                            {filteredEducation.length === 0 && queryEducation !== '' ? (
                                                                <div
                                                                    className="relative cursor-default select-none py-2 px-4 text-gray-700 dark:text-gray-300">
                                                                    Nothing found.
                                                                </div>
                                                            ) : (
                                                                filteredEducation.map((education) => (
                                                                    <Combobox.Option
                                                                        key={education._id}
                                                                        className={({active}) =>
                                                                            `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                                                                active ? 'bg-blue-500 text-white' : 'text-gray-700 dark:text-gray-300'
                                                                            }`
                                                                        }
                                                                        value={education}

                                                                    >
                                                                        {({selected, active}) => (
                                                                            <>
                        <span
                            className={`block truncate ${
                                selected ? 'font-medium' : 'font-normal'
                            }`}
                        >
                          {education.name}
                        </span>
                                                                                {selected ? (
                                                                                    <span
                                                                                        className={`absolute inset-y-0 left-0 flex items-center pl-3 ${
                                                                                            active ? 'text-white' : 'text-blue-500'
                                                                                        }`}
                                                                                    >
                            <CheckIcon className="h-5 w-5" aria-hidden="true"/>
                          </span>
                                                                                ) : null}
                                                                            </>
                                                                        )}
                                                                    </Combobox.Option>
                                                                ))
                                                            )}
                                                        </Combobox.Options>
                                                    </Transition>
                                                </div>
                                            </Combobox>
                                        )
                                    }
                                </div>

                                <div className="col-span-6 md:col-span-3 lg:col-span-2">
                                    <fieldset>
                                        <legend
                                            className="contents text-sm text-gray-600 dark:text-gray-200">ประวัติการดื่มสุรา
                                        </legend>
                                        <div
                                            className="space-y-2 /*xl:space-x-4*/ flex /*xl:flex-row*/ flex-col">
                                            <div className="flex flex-auto items-center mt-2">
                                                <input
                                                    id="drinking"
                                                    name="history-drinking"
                                                    type="radio"
                                                    className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                />
                                                <label htmlFor="drinking"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    ดื่มสุรา
                                                </label>
                                            </div>
                                            <div className="flex flex-auto items-center">
                                                <input
                                                    id="not-drinking"
                                                    name="history-drinking"
                                                    type="radio"
                                                    className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                />
                                                <label htmlFor="not-drinking"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    ไม่ดื่มสุรา
                                                </label>
                                            </div>

                                            <div className="flex flex-auto items-center">
                                                <input
                                                    id="stop-drinking"
                                                    name="history-drinking"
                                                    type="radio"
                                                    className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                />
                                                <label htmlFor="stop-drinking"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    เคยดื่มสุรา เลิกดื่มแล้ว
                                                </label>
                                                <div className="ml-2 flex flex-auto w-20 h-5 items-center">
                                                    <input
                                                        type="number"
                                                        name="history-drinking"
                                                        id="how-many-stop-drinking"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          ปี
                        </span>
                                                </div>
                                            </div>

                                        </div>
                                    </fieldset>
                                </div>
                                <div className="col-span-6 md:col-span-3 lg:col-span-2">
                                    <fieldset>
                                        <legend
                                            className="contents text-sm text-gray-600 dark:text-gray-200">ประวัติการสูบบุหรี่
                                        </legend>
                                        <div
                                            className="space-y-2 /*xl:space-x-4*/ flex /*xl:flex-row*/ flex-col">
                                            <div className="flex flex-auto items-center mt-2">
                                                <input
                                                    id="smoke"
                                                    name="history-smoke"
                                                    type="radio"
                                                    className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                />
                                                <label htmlFor="drinking"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    สูบบุหรี่
                                                </label>
                                            </div>
                                            <div className="flex flex-auto items-center">
                                                <input
                                                    id="not-smoke"
                                                    name="history-smoke"
                                                    type="radio"
                                                    className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                />
                                                <label htmlFor="not-drinking"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    ไม่สูบบุหรี่
                                                </label>
                                            </div>

                                            <div className="flex flex-auto items-center">
                                                <input
                                                    id="stop-smoke"
                                                    name="history-smoke"
                                                    type="radio"
                                                    className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                />
                                                <label htmlFor="stop-smoke"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    เคยสูบบุหรี่ เลิกสูบบุหรี่แล้ว
                                                </label>
                                                <div className="ml-2 flex flex-auto w-20 h-5 items-center">
                                                    <input
                                                        type="number"
                                                        name="history-smoke"
                                                        id="how-many-stop-smoke"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          ปี
                        </span>
                                                </div>
                                            </div>

                                        </div>
                                    </fieldset>


                                </div>
                                {/*<div className="col-span-6 my-5">*/}
                                {/*    <hr/>*/}
                                {/*</div>*/}
                                <div className="col-span-6 mt-2">
                                    <h3 className="text-sm font-medium text-gray-700 dark:text-gray-300">ประวัติการใช้กัญชา/สารสกัดจากกัญชา</h3>
                                </div>
                                <div className="col-span-6 sm:col-span-2">
                                    <fieldset>
                                        <legend
                                            className="contents text-sm text-gray-600 dark:text-gray-200">เคยใช้กัญชา
                                        </legend>
                                        <div className="flex flex-auto items-center mt-2">
                                            <div className="flex flex-auto items-center">
                                                <input
                                                    id="use-cannabis"
                                                    name="history-cannabis"
                                                    type="radio"
                                                    className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    value="1"
                                                />
                                                <label htmlFor="use-cannabis"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    เคย
                                                </label>
                                            </div>
                                            <div className="flex flex-auto items-center">
                                                <input
                                                    id="notuse-cannabis"
                                                    name="history-cannabis"
                                                    type="radio"
                                                    className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    value="2"
                                                />
                                                <label htmlFor="notuse-cannabis"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    ไม่เคย
                                                </label>
                                            </div>

                                        </div>
                                    </fieldset>


                                </div>


                                <div className="col-span-6 md:col-span-4 lg:col-span-2">
                                    <fieldset>
                                        <legend
                                            className="contents text-sm text-gray-600 dark:text-gray-200">1.
                                            เหตุผลของการใช้กัญชา
                                        </legend>
                                        <div
                                            className="space-y-2 /*xl:space-x-4*/ flex /*xl:flex-row*/ flex-col">
                                            <div className="flex flex-auto items-center mt-2">
                                                <input
                                                    id="treat-disease"
                                                    name="history-cannabis"
                                                    type="radio"
                                                    className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                />
                                                <label htmlFor="treat-disease"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    เพื่อรักษาโรค
                                                </label>
                                            </div>
                                            <div className="flex flex-auto items-center">
                                                <input
                                                    id="recreational"
                                                    name="history-cannabis"
                                                    type="radio"
                                                    className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                />
                                                <label htmlFor="recreational"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    เพื่อสันทนาการ
                                                </label>
                                            </div>

                                            <div className="flex flex-auto items-center">
                                                <input
                                                    id="other"
                                                    name="history-cannabis"
                                                    type="radio"
                                                    className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                />
                                                <label htmlFor="other"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    อื่นๆ ระบุ
                                                </label>
                                                <div className="ml-2 flex flex-auto w-fit h-5 items-center">
                                                    <input
                                                        type="text"
                                                        name="history-cannabis"
                                                        id="other-details"
                                                        className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                </div>
                                            </div>

                                        </div>
                                    </fieldset>
                                </div>
                                <div className="col-span-6 sm:col-span-2">
                                    <label htmlFor="last-datetime-use-cannabis"
                                           className="block text-sm text-gray-600 dark:text-gray-200">
                                        2. วัน เวลา ที่ใช้ครั้งสุดท้าย
                                    </label>
                                    <div className="relative flex items-center">
                                        <DatePicker selected={dob} onChange={(date) => setDob(date)}
                                                    locale="th"
                                                    peekNextMonth
                                                    showMonthDropdown
                                                    showYearDropdown
                                                    disabledKeyboardNavigation
                                                    showTimeSelect
                                                    dropdownMode="select"
                                                    dateFormat="dd/MM/yyyy HH:mm"
                                                    filterDate={isWeekday}
                                                    className="block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    placeholderText="วัน/เดือน/ปี ค.ศ."

                                        />
                                        <div
                                            className="flex absolute inset-y-0 right-0 items-center pr-3 mt-1 pointer-events-none">
                                            <CalendarDaysIcon
                                                className="h-6 w-6 text-gray-500 dark:text-gray-400 sm:h-5 sm:w-5"
                                                aria-hidden="true"/>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-span-6 sm:col-span-2">
                                    <fieldset>
                                        <legend
                                            className="contents text-sm text-gray-600 dark:text-gray-200">3.
                                            เคยได้รับผลข้างเคียงจากกัญชา
                                        </legend>
                                        <div className="flex flex-auto items-center mt-2">
                                            <div className="flex flex-auto items-center">
                                                <input
                                                    id="get-side-effects"
                                                    name="side-effects-cannabis"
                                                    type="radio"
                                                    className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    value="1"
                                                />
                                                <label htmlFor="get-side-effects"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    เคย
                                                </label>
                                            </div>
                                            <div className="flex flex-auto items-center">
                                                <input
                                                    id="no-side-effects"
                                                    name="side-effects-cannabis"
                                                    type="radio"
                                                    className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                    value="2"
                                                />
                                                <label htmlFor="no-side-effects"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    ไม่เคย
                                                </label>
                                            </div>

                                        </div>
                                    </fieldset>


                                </div>
                                <div className="col-span-6 lg:col-span-4">
                                    <fieldset>
                                        <legend
                                            className="contents text-sm text-gray-600 dark:text-gray-200">4.
                                            รูปแบบของกัญชาที่ใช้
                                        </legend>
                                        <div
                                            className="space-y-2 /*xl:space-x-4*/ flex /*xl:flex-row*/ flex-col">
                                            <div className="flex flex-auto items-center mt-2">
                                                <input
                                                    id="fresh-cannabis"
                                                    name="cannabis-type"
                                                    type="radio"
                                                    className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                />
                                                <label htmlFor="treat-disease"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    กัญชาสด
                                                </label>
                                            </div>
                                            <div className="flex flex-auto items-center">
                                                <input
                                                    id="dried-cannabis"
                                                    name="cannabis-type"
                                                    type="radio"
                                                    className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                />
                                                <label htmlFor="recreational"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    กัญชาแห้ง
                                                </label>
                                            </div>

                                            <div className="flex flex-auto items-center">
                                                <input
                                                    id="cannabis-product"
                                                    name="cannabis-type"
                                                    type="radio"
                                                    className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                />
                                                <label htmlFor="other"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    ผลิตภัณฑ์กัญชา ระบุ
                                                </label>
                                                <div className="ml-2 flex flex-auto w-fit h-5 items-center">
                                                    <input
                                                        type="text"
                                                        name="cannabis-type"
                                                        id="cannabis-product-details"
                                                        className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                </div>
                                                <label htmlFor="other"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    แหล่งที่ได้มาของผลิตภัณฑ์กัญชา
                                                </label>
                                                <div className="ml-2 flex flex-auto w-fit h-5 items-center">
                                                    <input
                                                        type="text"
                                                        name="cannabis-type"
                                                        id="cannabis-product-details"
                                                        className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                </div>


                                            </div>

                                        </div>
                                    </fieldset>
                                </div>

                                <div className="col-span-6 sm:col-span-2">
                                    <fieldset>
                                        <legend
                                            className="contents text-sm text-gray-600 dark:text-gray-200">ประวัติการใช้สารเสพติดอื่น
                                            เช่น ยาบ้า กระท่อม เฮโรอีน
                                        </legend>
                                        <div
                                            className="space-y-2.5 /*xl:space-x-4*/ flex /*xl:flex-row*/ flex-col">
                                            <div className="flex flex-auto items-center mt-2">
                                                <input
                                                    id="notuse-narcotic"
                                                    name="history-narcotic"
                                                    type="radio"
                                                    className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                />
                                                <label htmlFor="notuse-narcotic"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    ไม่เคยใช้
                                                </label>
                                            </div>
                                            <div className="flex flex-auto items-center">
                                                <input
                                                    id="use-narcotic"
                                                    name="history-narcotic"
                                                    type="radio"
                                                    className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                />
                                                <label htmlFor="use-narcotic"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    เคยใช้
                                                </label>
                                            </div>
                                            <div className="flex flex-auto items-center">
                                                <input
                                                    id="currently-use-narcotic"
                                                    name="history-narcotic"
                                                    type="radio"
                                                    className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                />
                                                <label htmlFor="currently-use-narcotic"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    ปัจจุบันยังใช้อยู่
                                                </label>
                                            </div>

                                            <div className="flex flex-auto items-center">
                                                <input
                                                    id="stop-narcotic"
                                                    name="history-narcotic"
                                                    type="radio"
                                                    className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                />
                                                <label htmlFor="stop-narcotic"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    เลิกใช้แล้ว
                                                </label>
                                                <div className="ml-2 flex flex-auto w-fit h-5 items-center">
                                                    <input
                                                        type="number"
                                                        name="history-narcotic"
                                                        id="how-many-stop-narcotic"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          เดือน
                        </span>
                                                </div>
                                            </div>

                                        </div>
                                    </fieldset>


                                </div>
                                <div className="col-span-6 sm:col-span-4">
                                    <fieldset>
                                        <legend
                                            className="contents text-sm text-gray-600 dark:text-gray-200">ประวัติแพ้ยา
                                        </legend>
                                        <div
                                            className="space-y-2 /*xl:space-x-4*/ flex /*xl:flex-row*/ flex-col">
                                            <div className="flex flex-auto items-center mt-2 xl:mt-3">
                                                <input
                                                    id="never-drug-allergy"
                                                    name="history-drug-allergy"
                                                    type="radio"
                                                    className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                />
                                                <label htmlFor="never-drug-allergy"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    ไม่เคยแพ้
                                                </label>
                                            </div>

                                            <div className="flex flex-auto items-center">
                                                <input
                                                    id="drug-allergy"
                                                    name="history-drug-allergy"
                                                    type="radio"
                                                    className="h-4 w-4 border-gray-300 text-blue-600 focus:ring-blue-500"
                                                />
                                                <label htmlFor="drug-allergy-details"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    เคยแพ้ยา
                                                </label>
                                                <div className="ml-2 flex flex-auto w-fit h-5 items-center">
                                                            <textarea
                                                                name="history-drug-allergy"
                                                                id="drug-allergy-details"
                                                                className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                            />

                                                </div>
                                            </div>

                                        </div>
                                    </fieldset>


                                </div>
                                <div className="col-span-6 sm:col-span-2">
                                    {/*<div className="fixed top-16 w-72">*/}
                                    <label htmlFor="disease"
                                           className="block text-sm text-gray-600 dark:text-gray-200">
                                        โรคประจำตัว
                                    </label>

                                    {
                                        errorProvinces ? (
                                            <div className="flex items-center text-red-500 mt-2">

                                                <ExclamationCircleIcon className="h-5 w-5 mr-2"
                                                                       aria-hidden="true"/>
                                                ไม่สามารถโหลดข้อมูลได้
                                            </div>
                                        ) : (

                                            <Combobox value={selectedDisease} onChange={setSelectedDisease}>
                                                <div className="relative mt-1">
                                                    <div
                                                        className="relative w-full cursor-default rounded-md">
                                                        <Combobox.Input
                                                            className="pr-10 leading-5 block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                            displayValue={(disease) => disease.name}
                                                            onChange={(event) => setQueryDisease(event.target.value)}
                                                        />
                                                        <Combobox.Button
                                                            className="absolute inset-y-0 right-0 flex items-center pr-2">
                                                            <ChevronUpDownIcon
                                                                className="h-5 w-5 text-gray-400"
                                                                aria-hidden="true"
                                                            />
                                                        </Combobox.Button>
                                                    </div>
                                                    <Transition
                                                        as={Fragment}
                                                        leave="transition ease-in duration-100"
                                                        leaveFrom="opacity-100"
                                                        leaveTo="opacity-0"
                                                        afterLeave={() => setQueryDisease('')}
                                                    >
                                                        <Combobox.Options
                                                            className="absolute z-50 mt-1 max-h-60 w-full overflow-auto py-1 text-base shadow-lg text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                                                            {filteredDisease.length === 0 && queryDisease !== '' ? (
                                                                <div
                                                                    className="relative cursor-default select-none py-2 px-4 text-gray-700 dark:text-gray-300">
                                                                    Nothing found.
                                                                </div>
                                                            ) : (
                                                                filteredDisease.map((disease) => (
                                                                    <Combobox.Option
                                                                        key={disease._id}
                                                                        className={({active}) =>
                                                                            `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                                                                active ? 'bg-blue-500 text-white' : 'text-gray-700 dark:text-gray-300'
                                                                            }`
                                                                        }
                                                                        value={disease}

                                                                    >
                                                                        {({selected, active}) => (
                                                                            <>
                        <span
                            className={`block truncate ${
                                selected ? 'font-medium' : 'font-normal'
                            }`}
                        >
                          {disease.name}
                        </span>
                                                                                {selected ? (
                                                                                    <span
                                                                                        className={`absolute inset-y-0 left-0 flex items-center pl-3 ${
                                                                                            active ? 'text-white' : 'text-blue-500'
                                                                                        }`}
                                                                                    >
                            <CheckIcon className="h-5 w-5" aria-hidden="true"/>
                          </span>
                                                                                ) : null}
                                                                            </>
                                                                        )}
                                                                    </Combobox.Option>
                                                                ))
                                                            )}
                                                        </Combobox.Options>
                                                    </Transition>
                                                </div>
                                            </Combobox>
                                        )
                                    }
                                </div>

                                <div className="col-span-6 sm:col-span-4">
                                    <label htmlFor="other-disease-details"
                                           className="block text-sm text-gray-600 dark:text-gray-200">
                                        โรคประจำตัว อื่นๆ ระบุ
                                    </label>
                                    <input
                                        type="text"
                                        name="history-disease"
                                        id="other-disease-details"
                                        autoComplete="other-disease-details"
                                        className="block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="bg-gray-50 dark:bg-gray-700 rounded-b-md px-4 py-3 text-right sm:px-6">
                            <button
                                type="submit"
                                className="inline-flex justify-center px-6 py-3 tracking-wide text-sm text-white transition-colors duration-300 transform bg-blue-600 rounded-md hover:bg-blue-700 focus:outline-none focus:bg-blue-700 focus:ring focus:ring-blue-500 focus:ring-opacity-50">
                                บันทึกข้อมูล
                            </button>
                        </div>
                    </div>
                </form>
                {/*</div>*/}
                {/*</div>*/}
            </section>
        </>
    )
}


export default Personal;