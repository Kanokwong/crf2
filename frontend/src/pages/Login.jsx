import React, {useEffect, useState} from "react";
import logo from "../logo.png";
import {Link, useLocation, useNavigate} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {login} from "../redux/actions/userActions";
import {toast} from "react-toastify";
import Loading from "../components/Loading.jsx";

function Login() {

    const [username, setUserName] = useState("");
    const [password, setPassword] = useState("");

    const navigate = useNavigate();
    const location = useLocation();

    const dispatch = useDispatch();

    const redirect = location.search ? location.search.split("=")[1] : "/";

    const userLogin = useSelector((state) => state.userLogin);
    const {error, loading, userInfo} = userLogin;

    const submitHandler = (e) => {
        e.preventDefault();
        dispatch(login(username, password));
    }

    useEffect(() => {
        if (userInfo) {
            navigate(redirect);
        }
    }, [userInfo, navigate, redirect])
    return (
        <>
            {error && toast.error(`${error}`, {toastId: ""})}
            {loading && userInfo ? Loading() :
                (
                    <div className="bg-white dark:bg-gray-900">
                        <div className="flex justify-center h-screen">
                            <div
                                className="hidden bg-cover lg:block lg:w-2/3 bg-[url('https://images.unsplash.com/photo-1616763355603-9755a640a287?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80')]">
                                <div className="flex items-center h-full px-20 bg-gray-900 bg-opacity-40">
                                    <div>
                                        <h2 className="text-4xl font-bold text-white">CANNABINOIDS</h2>

                                        <p className="max-w-xl mt-3 text-gray-300">
                                            แบบเก็บข้อมูล (Case record form) : การศึกษาประสิทธิผลของแคนนาบินอยด์
                                            <br></br>และการประเมินคุณภาพชีวิตในผู้ป่วยมะเร็งระยะแพร่กระจาย
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div className="flex items-center w-full max-w-md px-6 mx-auto lg:w-2/6">
                                <div className="flex-1">
                                    <div className="text-center">
                                        <img
                                            className="mx-auto h-14 w-auto"
                                            src={logo}
                                            alt="CRF"
                                        />
                                        <h2 className="mt-3 text-4xl font-bold text-center text-gray-700 dark:text-white">CANNABINOIDS</h2>

                                        <p className="mt-3 text-gray-500 dark:text-gray-300">ลงชื่อเข้าใช้เพื่อเข้าถึงบัญชีของคุณ</p>
                                    </div>

                                    <div className="mt-8">
                                        <form onSubmit={submitHandler}>
                                            <div>
                                                <label htmlFor="username"
                                                       className="block mb-2 text-sm text-gray-600 dark:text-gray-200">
                                                    ชื่อผู้ใช้งาน (Username)
                                                </label>
                                                <input type="username" name="username" id="username"
                                                       autoComplete="username"
                                                       required
                                                       className="block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-900 dark:text-gray-300 dark:border-gray-700 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500"
                                                       value={username}
                                                       onChange={(e) => setUserName(e.target.value)}/>
                                            </div>

                                            <div className="mt-6">
                                                <div className="flex justify-between mb-2">
                                                    <label htmlFor="password"
                                                           className="text-sm text-gray-600 dark:text-gray-200">รหัสผ่าน
                                                        (Password)</label>

                                                    <Link to="#"
                                                          className="text-sm text-gray-400 focus:text-blue-500 hover:text-blue-500 hover:underline">ลืมรหัสผ่าน?</Link>
                                                </div>

                                                <input type="password" name="password" id="password"
                                                       required
                                                       className="block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-900 dark:text-gray-300 dark:border-gray-700 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500"
                                                       value={password}
                                                       onChange={(e) => setPassword(e.target.value)}/>
                                            </div>

                                            <div className="mt-6">
                                                <button
                                                    type="submit"
                                                    className="w-full px-6 py-3 tracking-wide text-sm text-white transition-colors duration-300 transform bg-blue-600 rounded-md hover:bg-blue-700 focus:outline-none focus:bg-blue-700 focus:ring focus:ring-blue-500 focus:ring-opacity-50">
                                                    เข้าสู่ระบบ
                                                </button>
                                            </div>

                                        </form>
                                        <p className="mt-6 text-sm text-center text-gray-400">ยังไม่มีบัญชี? <Link
                                            to={redirect ? `/register?redirect=${redirect}` : "/register"}
                                            className="text-blue-600 focus:outline-none focus:underline hover:underline">ลงทะเบียนเพื่อรับบัญชี</Link>.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                )
            }
        </>

    )
}

export default Login;