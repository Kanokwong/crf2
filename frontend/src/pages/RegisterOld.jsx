import React, {Fragment, useEffect, useState} from "react";
import logo from "../logo.png";
import {
    CheckIcon,
    ChevronUpDownIcon,
    ExclamationCircleIcon,
    UserPlusIcon
} from "@heroicons/react/20/solid";
import {Combobox, Transition} from "@headlessui/react";
import {useDispatch, useSelector} from "react-redux";
import {listHospital} from "../redux/actions/hospitalActions";
import {toast} from "react-toastify";
import {listTitle} from "../redux/actions/titleActions";

function RegisterOld() {

    const dispatch = useDispatch();
    const hospitalList = useSelector((state) => state.hospitalList);
    const {error: errorHospitals, hospitals} = hospitalList;
    const titleList = useSelector((state) => state.titleList)
    const {error: errorTitles, titles} = titleList;

    useEffect(() => {
        dispatch(listHospital());
        dispatch(listTitle());
    }, [dispatch])

    const [username, setUserName] = useState("");
    const [password, setPassword] = useState("");

    const [selectedTitle, setSelectedTitle] = useState(titles);
    const [selectedHospital, setSelectedHospital] = useState(hospitals);
    const [queryTitle, setQueryTitle] = useState("");
    const [queryHospital, setQueryHospital] = useState("");

    const filteredTitle =
        queryTitle === ''
            ? titles
            : titles.filter((title) =>
                title.name
                    .toLowerCase()
                    .replace(/\s+/g, '')
                    .includes(queryTitle.toLowerCase().replace(/\s+/g, ''))
            )

    const filteredHospital =
        queryHospital === ''
            ? hospitals
            : hospitals.filter((hospital) =>
                hospital.name
                    .toLowerCase()
                    .replace(/\s+/g, '')
                    .includes(queryHospital.toLowerCase().replace(/\s+/g, ''))
            )
    const submitHandler = (e) => {
        e.preventDefault();
    }
    return (
        <>
            {errorHospitals && toast.error(`${errorHospitals}`, {toastId: ""})}
            {errorTitles && toast.error(`${errorTitles}`, {toastId: ""})}

            {/*{loading && Loading()}*/}
            <div className="flex min-h-full items-center justify-center py-6 my-8 px-0 lg:mx-16">
                <div className="w-full space-y-8">
                    <div>
                        <img
                            className="mx-auto h-14 w-auto"
                            src={logo}
                            alt="CRF"
                        />
                        <h2 className="mt-6 text-center text-2xl tracking-normal font-bold tracking-tight text-gray-900">
                            สมัครสมาชิก
                        </h2>
                    </div>

                    <div className="md:grid md:grid-cols-2 md:gap-6">
                        <div className="mt-5 md:col-span-2 md:mt-0">
                            <form onSubmit={submitHandler}>
                                <div className="overflow-visible shadow rounded-md">
                                    <div className="bg-white px-4 py-5 sm:p-6 rounded-md">
                                        <div className="grid grid-cols-6 gap-6">
                                            <div className="col-span-6 sm:col-span-2">
                                                <label htmlFor="first-name"
                                                       className="block text-sm font-medium text-gray-700">
                                                    คำนำหน้า
                                                </label>
                                                {
                                                    errorTitles ? (
                                                        <div className="flex items-center text-red-500 mt-2">

                                                            <ExclamationCircleIcon className="h-5 w-5 mr-2"
                                                                                   aria-hidden="true"/>
                                                            ไม่สามารถโหลดข้อมูลได้
                                                        </div>
                                                    ) : (
                                                        <Combobox value={selectedTitle} onChange={setSelectedTitle}>
                                                            <div className="relative mt-1">
                                                                <div
                                                                    className="relative w-full cursor-default rounded-md">
                                                                    <Combobox.Input
                                                                        className="w-full rounded-md py-2 pl-3 pr-10 text-sm leading-5 text-gray-900 border border-gray-300 shadow-sm focus:outline-none focus:border-blue-500 focus:ring-1 focus:ring-blue-500"
                                                                        displayValue={(title) => title.name}
                                                                        onChange={(event) => setQueryTitle(event.target.value)}
                                                                    />
                                                                    <Combobox.Button
                                                                        className="absolute inset-y-0 right-0 flex items-center pr-2">
                                                                        <ChevronUpDownIcon
                                                                            className="h-5 w-5 text-gray-400"
                                                                            aria-hidden="true"
                                                                        />
                                                                    </Combobox.Button>
                                                                </div>
                                                                <Transition
                                                                    as={Fragment}
                                                                    leave="transition ease-in duration-100"
                                                                    leaveFrom="opacity-100"
                                                                    leaveTo="opacity-0"
                                                                    afterLeave={() => setQueryTitle('')}
                                                                >
                                                                    {/*z-50 fix for no behind all element*/}
                                                                    <Combobox.Options
                                                                        className="absolute z-50 mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                                                                        {filteredTitle.length === 0 && queryTitle !== '' ? (
                                                                            <div
                                                                                className="relative cursor-default select-none py-2 px-4 text-gray-700">
                                                                                Nothing found.
                                                                            </div>
                                                                        ) : (
                                                                            filteredTitle.map((title) => (
                                                                                <Combobox.Option
                                                                                    key={title._id}
                                                                                    className={({active}) =>
                                                                                        `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                                                                            active ? 'bg-blue-500 text-white' : 'text-gray-900'
                                                                                        }`
                                                                                    }
                                                                                    value={title}
                                                                                >
                                                                                    {({selected, active}) => (
                                                                                        <>
                                                    <span
                                                        className={`block truncate ${
                                                            selected ? 'font-medium' : 'font-normal'
                                                        }`}
                                                    >
                                                {title.name}
                                                    </span>
                                                                                            {selected ? (
                                                                                                <span
                                                                                                    className={`absolute inset-y-0 left-0 flex items-center pl-3 ${
                                                                                                        active ? 'text-white' : 'text-blue-500'
                                                                                                    }`}
                                                                                                >
                                                    <CheckIcon className="h-5 w-5" aria-hidden="true"/>
                                                    </span>
                                                                                            ) : null}
                                                                                        </>
                                                                                    )}
                                                                                </Combobox.Option>
                                                                            ))
                                                                        )}
                                                                    </Combobox.Options>
                                                                </Transition>
                                                            </div>
                                                        </Combobox>
                                                    )
                                                }

                                            </div>
                                            <div className="col-span-6 sm:col-span-2">
                                                <label htmlFor="first-name"
                                                       className="block text-sm font-medium text-gray-700">
                                                    ชื่อ
                                                </label>
                                                <input
                                                    type="text"
                                                    name="first-name"
                                                    id="first-name"
                                                    autoComplete="given-name"
                                                    required
                                                    className="mt-1 block w-full rounded-md border border-gray-300 px-3 py-2 text-gray-900 shadow-sm focus:outline-none focus:border-blue-500 focus:ring-1 focus:ring-blue-500 sm:text-sm"
                                                />
                                            </div>
                                            <div className="col-span-6 sm:col-span-2">
                                                <label htmlFor="family-name"
                                                       className="block text-sm font-medium text-gray-700">
                                                    สกุล
                                                </label>
                                                <input
                                                    type="text"
                                                    name="family-name"
                                                    id="family-name"
                                                    autoComplete="family-name"
                                                    required
                                                    className="mt-1 block w-full rounded-md border border-gray-300 px-3 py-2 text-gray-900 shadow-sm focus:outline-none focus:border-blue-500 focus:ring-1 focus:ring-blue-500 sm:text-sm"
                                                />
                                            </div>
                                            <div className="col-span-6 sm:col-span-2">
                                                <label htmlFor="email"
                                                       className="block text-sm font-medium text-gray-700">
                                                    อีเมล
                                                </label>
                                                <input
                                                    type="email"
                                                    name="email"
                                                    id="email"
                                                    autoComplete="email"
                                                    required
                                                    placeholder="example@email.com"
                                                    className="mt-1 block w-full rounded-md border border-gray-300 px-3 py-2 text-gray-900 shadow-sm focus:outline-none focus:border-blue-500 focus:ring-1 focus:ring-blue-500 sm:text-sm"
                                                />
                                            </div>
                                            <div className="col-span-6 sm:col-span-2">
                                                <label htmlFor="username"
                                                       className="block text-sm font-medium text-gray-700">
                                                    ชื่อผู้ใช้งาน (Username)
                                                </label>
                                                <input
                                                    type="text"
                                                    name="username"
                                                    id="username"
                                                    autoComplete="username"
                                                    required
                                                    className="mt-1 block w-full rounded-md border border-gray-300 px-3 py-2 text-gray-900 shadow-sm focus:outline-none focus:border-blue-500 focus:ring-1 focus:ring-blue-500 sm:text-sm"
                                                    value={username}
                                                    onChange={(e) => setUserName(e.target.value)}
                                                />
                                            </div>
                                            <div className="col-span-6 sm:col-span-2">
                                                <label htmlFor="password"
                                                       className="block text-sm font-medium text-gray-700">
                                                    รหัสผ่าน (Password)
                                                </label>
                                                <input
                                                    type="password"
                                                    name="password"
                                                    id="password"
                                                    required
                                                    minLength="8"
                                                    className="mt-1 block w-full rounded-md border border-gray-300 px-3 py-2 text-gray-900 shadow-sm focus:outline-none focus:border-blue-500 focus:ring-1 focus:ring-blue-500 sm:text-sm"
                                                    value={password}
                                                    onChange={(e) => setPassword(e.target.value)}
                                                />
                                            </div>
                                            <div className="col-span-6 sm:col-span-2">
                                                <label htmlFor="site"
                                                       className="block text-sm font-medium text-gray-700">
                                                    โรงพยาบาล
                                                </label>
                                                {
                                                    errorHospitals ? (
                                                        <div className="flex items-center text-red-500 mt-2">

                                                            <ExclamationCircleIcon className="h-5 w-5 mr-2"
                                                                                   aria-hidden="true"/>
                                                            ไม่สามารถโหลดข้อมูลได้
                                                        </div>
                                                    ) : (
                                                        <Combobox value={selectedHospital}
                                                                  onChange={setSelectedHospital}>
                                                            <div className="relative mt-1">
                                                                <div
                                                                    className="relative w-full cursor-default rounded-md">
                                                                    <Combobox.Input
                                                                        className="w-full rounded-md py-2 pl-3 pr-10 text-sm leading-5 text-gray-900 border border-gray-300 shadow-sm focus:outline-none focus:border-blue-500 focus:ring-1 focus:ring-blue-500"
                                                                        displayValue={(hospital) => hospital.name}
                                                                        onChange={(event) => setQueryHospital(event.target.value)}
                                                                    />
                                                                    <Combobox.Button
                                                                        className="absolute inset-y-0 right-0 flex items-center pr-2">
                                                                        <ChevronUpDownIcon
                                                                            className="h-5 w-5 text-gray-400"
                                                                            aria-hidden="true"
                                                                        />
                                                                    </Combobox.Button>
                                                                </div>
                                                                <Transition
                                                                    as={Fragment}
                                                                    leave="transition ease-in duration-100"
                                                                    leaveFrom="opacity-100"
                                                                    leaveTo="opacity-0"
                                                                    afterLeave={() => setQueryHospital('')}
                                                                >
                                                                    <Combobox.Options
                                                                        className="absolute z-50 mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                                                                        {filteredHospital.length === 0 && queryHospital !== '' ? (
                                                                            <div
                                                                                className="relative cursor-default select-none py-2 px-4 text-gray-700">
                                                                                Nothing found.
                                                                            </div>
                                                                        ) : (
                                                                            filteredHospital.map((hospital) => (
                                                                                <Combobox.Option
                                                                                    key={hospital._id}
                                                                                    className={({active}) =>
                                                                                        `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                                                                            active ? 'bg-blue-500 text-white' : 'text-gray-900'
                                                                                        }`
                                                                                    }
                                                                                    value={hospital}
                                                                                >
                                                                                    {({selected, active}) => (
                                                                                        <>
                                                    <span
                                                        className={`block truncate ${
                                                            selected ? 'font-medium' : 'font-normal'
                                                        }`}
                                                    >
                                                {hospital.name}
                                                    </span>
                                                                                            {selected ? (
                                                                                                <span
                                                                                                    className={`absolute inset-y-0 left-0 flex items-center pl-3 ${
                                                                                                        active ? 'text-white' : 'text-blue-500'
                                                                                                    }`}
                                                                                                >
                                                    <CheckIcon className="h-5 w-5" aria-hidden="true"/>
                                                    </span>
                                                                                            ) : null}
                                                                                        </>
                                                                                    )}
                                                                                </Combobox.Option>
                                                                            ))
                                                                        )}
                                                                    </Combobox.Options>
                                                                </Transition>
                                                            </div>
                                                        </Combobox>
                                                    )
                                                }

                                            </div>

                                        </div>

                                    </div>
                                    <div
                                        className="px-4 py-4 sm:px-6 sm:py-3 sm:gap-5 items-center bg-gray-50 rounded-md">
                                        <button
                                            type="submit"
                                            className="group relative flex w-full justify-center rounded-md border border-transparent bg-blue-600 py-2 px-4 text-md font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2 sm:text-sm"
                                        >
                <span className="absolute inset-y-0 left-0 flex items-center pl-3">
                  <UserPlusIcon className="h-5 w-5 text-blue-500 group-hover:text-blue-400" aria-hidden="true"/>
                </span>
                                            สมัครสมาชิก
                                        </button>
                                    </div>

                                    {/*<div*/}
                                    {/*    className="grid grid-cols-2 gap-4 px-4 py-4 sm:px-6 sm:py-3 sm:gap-5 items-center bg-gray-50 rounded-md">*/}
                                    {/*    <div className="col-span-2 sm:col-span-1">*/}
                                    {/*        <button*/}
                                    {/*            type="submit"*/}
                                    {/*            className="w-full justify-center rounded-md border border-transparent bg-blue-600 py-2 px-4 text-sm font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2"*/}
                                    {/*        >*/}
                                    {/*            สมัครสมาชิก*/}
                                    {/*        </button>*/}
                                    {/*    </div>*/}
                                    {/*    <div className="col-span-2 sm:col-span-1">*/}
                                    {/*        <Link to="/login"*/}
                                    {/*              className="w-full inline-flex items-center justify-center rounded-md border border-green-500 bg-gray-50 px-4 py-2 text-sm font-medium text-green-500 shadow-sm hover:bg-green-500 hover:text-white focus:outline-none focus:ring-2 focus:ring-green-400 focus:ring-offset-2">*/}
                                    {/*            เข้าสู่ระบบ*/}
                                    {/*        </Link>*/}
                                    {/*    </div>*/}
                                    {/*</div>*/}


                                </div>

                            </form>
                        </div>


                    </div>

                </div>
            </div>

        </>
    )
}

export default RegisterOld;