import React, {useEffect, useState} from "react";
import {LockClosedIcon} from '@heroicons/react/20/solid'
import logo from "../logo.png"
import {Link, useLocation, useNavigate} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {login} from "../redux/actions/userActions";
import {toast} from "react-toastify";
import Loading from "../components/Loading.jsx";


function LoginOld() {
    const navigate = useNavigate();
    const location = useLocation();

    const [username, setUserName] = useState("");
    const [password, setPassword] = useState("")

    const dispatch = useDispatch();

    const redirect = location.search ? location.search.split("=")[1] : "/";

    const userLogin = useSelector((state) => state.userLogin);
    const {error, loading, userInfo} = userLogin;

    const submitHandler = (e) => {
        e.preventDefault();
        dispatch(login(username, password));
    }

    useEffect(() => {
        if (userInfo) {
            navigate(redirect);
        }
    }, [userInfo, navigate, redirect])

    return (
        <>
            {error && toast.error(`${error}`, {toastId: ""})}
            {loading ? Loading() :
                (<div className="flex min-h-full items-center justify-center py-12 px-4 my-8 sm:px-6 lg:px-8">
                        <div className="w-full max-w-md space-y-8">
                            <div>
                                <img
                                    className="mx-auto h-14 w-auto"
                                    src={logo}
                                    alt="CRF"
                                />
                                <h2 className="mt-6 text-center text-2xl tracking-normal font-bold tracking-tight text-gray-900">
                                    ลงชื่อเข้าใช้งาน
                                </h2>
                            </div>
                            <form className="mt-8 space-y-6" onSubmit={submitHandler}>
                                {/*<input type="hidden" name="remember" defaultValue="true"/>*/}
                                {/*<div className="rounded-md shadow-sm -space-y-px bg-white px-5 py-5 sm:p-7">*/}
                                <div className="-space-y-px rounded-md shadow-sm">
                                    <div>
                                        <label htmlFor="username" className="sr-only">
                                            ชื่อผู้ใช้งาน (Username)
                                        </label>
                                        <input
                                            id="username"
                                            name="username"
                                            autoComplete="username"
                                            required
                                            className="relative block w-full appearance-none rounded-none rounded-t-md border border-gray-300 px-3 py-3 text-gray-900 placeholder-gray-500 focus:z-10 focus:ring-1 focus:border-blue-500 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                            placeholder="ชื่อผู้ใช้งาน (Username)"
                                            value={username}
                                            onChange={(e) => setUserName(e.target.value)}
                                        />
                                    </div>
                                    <div>
                                        <label htmlFor="password" className="sr-only">
                                            รหัสผ่าน (Password)
                                        </label>
                                        <input
                                            id="password"
                                            name="password"
                                            type="password"
                                            autoComplete="current-password"
                                            required
                                            className="relative block w-full appearance-none rounded-none rounded-b-md border border-gray-300 px-3 py-3 text-gray-900 placeholder-gray-500 focus:z-10 focus:ring-1 focus:border-blue-500 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                            placeholder="รหัสผ่าน (Password)"
                                            value={password}
                                            onChange={(e) => setPassword(e.target.value)}
                                        />
                                    </div>
                                </div>

                                <div className="text-center">
                                    <Link to={redirect ? `/register?redirect=${redirect}` : "/register"}
                                          className="font-medium text-green-500 hover:text-green-500 sm:text-sm">
                                        หากคุณยังไม่มีบัญชี สามารถสมัครสมาชิกได้ที่นี่
                                    </Link>
                                </div>

                                {/*<div className="flex items-center justify-between">*/}
                                {/*    <div className="flex items-center">*/}
                                {/*        <input*/}
                                {/*            id="remember-me"*/}
                                {/*            name="remember-me"*/}
                                {/*            type="checkbox"*/}
                                {/*            className="h-4 w-4 rounded border-gray-300 text-blue-700 focus:ring-blue-500"*/}
                                {/*        />*/}
                                {/*        <label htmlFor="remember-me" className="ml-2 block text-sm text-gray-900">*/}
                                {/*            Remember me*/}
                                {/*        </label>*/}
                                {/*    </div>*/}

                                {/*    <div className="text-sm">*/}
                                {/*        <a href="#" className="font-medium text-blue-600 hover:text-blue-500">*/}
                                {/*            Forgot your password?*/}
                                {/*        </a>*/}
                                {/*    </div>*/}
                                {/*</div>*/}

                                <div>
                                    <button
                                        type="submit"
                                        className="group relative flex w-full justify-center rounded-md border border-transparent bg-blue-600 py-2 px-4 text-md font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2 sm:text-sm"
                                    >
                <span className="absolute inset-y-0 left-0 flex items-center pl-3">
                  <LockClosedIcon className="h-5 w-5 text-blue-500 group-hover:text-blue-400" aria-hidden="true"/>
                </span>
                                        เข้าสู่ระบบ
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                )

            }
        </>
    )
}

export default LoginOld;