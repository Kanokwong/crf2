import React, {Fragment, useState} from "react";
import Header from "../components/Header.jsx";
import {Combobox, Transition} from "@headlessui/react";
import {CalendarDaysIcon, CheckIcon, ChevronUpDownIcon} from "@heroicons/react/20/solid";
import DatePicker from "react-datepicker";
import {getDay} from "date-fns";
// import Stepper from "../components/Stepper";

const people = [
    {id: 1, name: 'Wade Cooper'},
    {id: 2, name: 'Arlene Mccoy'},
    {id: 3, name: 'Devon Webb'},
    {id: 4, name: 'Tom Cook'},
    {id: 5, name: 'Tanya Fox'},
    {id: 6, name: 'Hellen Schmidt'},
]

function TreatmentHistory() {
    const [selected, setSelected] = useState(people[0])
    const [query, setQuery] = useState('')
    // const [activeStep, setActiveStep] = useState(0);
    //
    // const handleNext = () => {
    //     setActiveStep((prevActiveStep) => prevActiveStep + 1);
    // };
    //
    // const handleBack = () => {
    //     setActiveStep((prevActiveStep) => prevActiveStep - 1);
    // };
    //
    // const handleReset = () => {
    //     setActiveStep(0);
    // };


    const [dob, setDob] = useState(new Date());
    const isWeekday = (date) => {
        const day = getDay(date);
        return day !== 0 && day !== 6;
    };


    const filteredPeople =
        query === ''
            ? people
            : people.filter((person) =>
                person.name
                    .toLowerCase()
                    .replace(/\s+/g, '')
                    .includes(query.toLowerCase().replace(/\s+/g, ''))
            )


    return (
        <>
            <Header/>
            <section className="mx-auto max-w-7xl px-2 py-10 sm:px-6 lg:px-8 dark:bg-gray-900">
                <div className="px-4 sm:px-0">
                    <h3 className="text-xl font-medium leading-6 text-gray-700 dark:text-white">แบบเก็บข้อมูล (Case
                        record form):
                        การศึกษาประสิทธิผลของแคนนาบินอยด์และ
                        การประเมินคุณภาพชีวิตในผู้ป่วยมะเร็งระยะแพร่กระจาย
                    </h3>
                    <div className="grid grid-cols-1 sm:grid-cols-2 mt-2">
                        <div>
                            <p className="text-md text-gray-600 dark:text-gray-200 dark:text-gray-400">
                                ส่วนที่ 2 : ประวัติการเจ็บป่วยปัจจุบัน</p>
                        </div>
                        <div className="flex flex-wrap sm:flex-auto">
                            <div
                                className="flex flex-auto sm:flex-1 items-center">
                                <p className="text-md text-gray-600 dark:text-gray-200 dark:text-gray-400">
                                    STUDY SITE :</p>
                            </div>
                            <div
                                className="flex flex-auto sm:flex-1 items-center">
                                <p className="text-md text-gray-600 dark:text-gray-200 dark:text-gray-400">
                                    STUDY ID :</p>
                            </div>

                        </div>

                    </div>

                    <form>
                        <div className="overflow-visible shadow rounded-md">
                            <div
                                className="grid grid-cols-1 gap-6 mt-5 sm:grid-cols-2 bg-white rounded-md rounded-b-none shadow-md dark:bg-gray-800 px-4 py-5 sm:p-6">
                                <div className="col-span-2 sm:col-auto">
                                    <label htmlFor="diagnosis"
                                           className="block text-sm text-gray-600 dark:text-gray-200">
                                        การวินิจฉัย
                                    </label>
                                    <Combobox value={selected} onChange={setSelected}>
                                        <div className="relative mt-1">
                                            <div
                                                className="relative w-full cursor-default rounded-md">
                                                <Combobox.Input
                                                    className="pr-10 leading-5 block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    displayValue={(person) => person.name}
                                                    onChange={(event) => setQuery(event.target.value)}
                                                />
                                                <Combobox.Button
                                                    className="absolute inset-y-0 right-0 flex items-center pr-2">
                                                    <ChevronUpDownIcon
                                                        className="h-5 w-5 text-gray-400"
                                                        aria-hidden="true"
                                                    />
                                                </Combobox.Button>
                                            </div>
                                            <Transition
                                                as={Fragment}
                                                leave="transition ease-in duration-100"
                                                leaveFrom="opacity-100"
                                                leaveTo="opacity-0"
                                                afterLeave={() => setQuery('')}
                                            >
                                                <Combobox.Options
                                                    className="absolute z-50 mt-1 max-h-60 w-full overflow-auto py-1 text-base shadow-lg text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                                                    {filteredPeople.length === 0 && query !== '' ? (
                                                        <div
                                                            className="relative cursor-default select-none py-2 px-4 text-gray-700">
                                                            Nothing found.
                                                        </div>
                                                    ) : (
                                                        filteredPeople.map((person) => (
                                                            <Combobox.Option
                                                                key={person.id}
                                                                className={({active}) =>
                                                                    `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                                                        active ? 'bg-blue-500 text-white' : 'text-gray-900'
                                                                    }`
                                                                }
                                                                value={person}
                                                            >
                                                                {({selected, active}) => (
                                                                    <>
                        <span
                            className={`block truncate ${
                                selected ? 'font-medium' : 'font-normal'
                            }`}
                        >
                          {person.name}
                        </span>
                                                                        {selected ? (
                                                                            <span
                                                                                className={`absolute inset-y-0 left-0 flex items-center pl-3 ${
                                                                                    active ? 'text-white' : 'text-blue-500'
                                                                                }`}
                                                                            >
                            <CheckIcon className="h-5 w-5" aria-hidden="true"/>
                          </span>
                                                                        ) : null}
                                                                    </>
                                                                )}
                                                            </Combobox.Option>
                                                        ))
                                                    )}
                                                </Combobox.Options>
                                            </Transition>
                                        </div>
                                    </Combobox>
                                </div>
                                <div className="col-span-2 sm:col-auto">
                                    <label htmlFor="other-diagnosis"
                                           className="block text-sm text-gray-600 dark:text-gray-200">
                                        การวินิจฉัย อื่นๆ ระบุ
                                    </label>
                                    <input
                                        type="text"
                                        name="other-diagnosis"
                                        id="other-diagnosis"
                                        className="block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                    />
                                </div>
                                <div className="col-span-2">
                                    <fieldset>
                                        <legend className="block text-sm text-gray-600 dark:text-gray-200">
                                            ตำแหน่งการแพร่กระจายของมะเร็ง (ตอบได้มากกว่า 1 ข้อ)
                                        </legend>
                                        <div className="grid grid-cols-1 sm:grid-cols-4">
                                            <div>
                                                <div className="flex flex-auto items-center mt-2 xl:mt-3">
                                                    <input
                                                        id="bone"
                                                        name="location-of-cancer"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="bone"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                        กระดูก
                                                    </label>
                                                </div>
                                                <div className="flex flex-auto items-center mt-2 xl:mt-3">
                                                    <input
                                                        id="lung"
                                                        name="location-of-cancer"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="lung"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                        ปอด
                                                    </label>
                                                </div>
                                                <div className="flex flex-auto items-center mt-2 xl:mt-3">
                                                    <input
                                                        id="lymph-node"
                                                        name="location-of-cancer"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="lymph-node"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                        ต่อมน้ำเหลือง
                                                    </label>
                                                </div>
                                                <div className="flex flex-auto items-center mt-2 xl:mt-3">
                                                    <input
                                                        id="liver"
                                                        name="location-of-cancer"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="liver"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                        ตับ
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="col-span-3">
                                                <div className="flex flex-auto items-center mt-2 xl:mt-3">
                                                    <input
                                                        id="peritoneum"
                                                        name="location-of-cancer"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="peritoneum"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                        เยื่อบุช่องท้อง
                                                    </label>
                                                </div>
                                                <div className="flex flex-auto items-center mt-2 xl:mt-3">
                                                    <input
                                                        id="pleura"
                                                        name="location-of-cancer"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="pleura"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                        เยื่อหุ้มปอด
                                                    </label>
                                                </div>
                                                <div className="flex flex-auto items-center mt-2 xl:mt-3">
                                                    <input
                                                        id="other-location"
                                                        name="location-of-cancer"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="other-location"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                        อื่นๆ
                                                    </label>
                                                    <div className="ml-2 flex flex-auto w-fit h-5 items-center">
                                                        <input
                                                            type="text"
                                                            name="location-of-cancer"
                                                            id="other-location-details"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div className="col-span-2">
                                    <fieldset>
                                        <legend className="block text-sm text-gray-600 dark:text-gray-200">
                                            แผนการรักษาของแพทย์ (ตอบได้มากกว่า 1 ข้อ)
                                        </legend>
                                        <div className="space-y-6">
                                            <div className="flex flex-auto items-center mt-2 sm:mt-4">
                                                <input
                                                    id="palliative-care"
                                                    name="doctor-treatment-plan"
                                                    type="checkbox"
                                                    className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                <label htmlFor="palliative-care"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    การรักษาแบบประคับประคอง(Palliative care)
                                                </label>
                                            </div>
                                            <div className="flex flex-wrap lg:flex-auto items-center mt-2 sm:mt-0">
                                                <input
                                                    id="chemotherapy"
                                                    name="doctor-treatment-plan"
                                                    type="checkbox"
                                                    className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                <label htmlFor="chemotherapy"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    เคมีบำบัดแบบประคับประคองจำนวนครั้ง
                                                </label>
                                                <div
                                                    className="sm:ml-2 flex flex-auto sm:grow-0 items-center lg:h-5 mt-2 sm:mt-0 ml-6">
                                                    <input
                                                        type="number"
                                                        name="doctor-treatment-plan"
                                                        id="number-of-chemotherapy"
                                                        min="0"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          ครั้ง
                        </span>
                                                </div>
                                                <div className="flex flex-wrap sm:flex-auto items-center">
                                                    <label htmlFor="last-time-of-chemotherapy"
                                                           className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 ml-6">
                                                        ครั้งล่าสุด วัน/เดือน/ปี
                                                    </label>
                                                    <div
                                                        className="relative sm:ml-2 flex flex-auto sm:grow-0 items-center lg:h-5 w-fit mt-2 sm:mt-0 ml-6">
                                                        <DatePicker selected={dob} onChange={(date) => setDob(date)}
                                                                    locale="th"
                                                                    peekNextMonth
                                                                    showMonthDropdown
                                                                    showYearDropdown
                                                                    disabledKeyboardNavigation
                                                                    dropdownMode="select"
                                                                    dateFormat="dd/MM/yyyy"
                                                                    filterDate={isWeekday}
                                                                    className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                    placeholderText="วัน/เดือน/ปี ค.ศ."
                                                                    name="doctor-treatment-plan"
                                                                    id="last-time-of-chemotherapy"
                                                        />
                                                        <div
                                                            className="flex absolute inset-y-0 right-0 items-center pr-3 mt-1 pointer-events-none">
                                                            <CalendarDaysIcon
                                                                className="h-6 w-6 text-gray-500 dark:text-gray-400 sm:h-5 sm:w-5"
                                                                aria-hidden="true"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="flex flex-wrap lg:flex-auto items-center mt-2 sm:mt-0">
                                                <input
                                                    id="radiation"
                                                    name="doctor-treatment-plan"
                                                    type="checkbox"
                                                    className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                <label htmlFor="chemotherapy"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    ฉายแสงแบบประคับประคองจำนวนครั้ง
                                                </label>
                                                <div
                                                    className="sm:ml-2 flex flex-auto sm:grow-0 items-center lg:h-5 mt-2 sm:mt-0 ml-6">
                                                    <input
                                                        type="number"
                                                        name="radiation"
                                                        id="number-of-radiation"
                                                        min="0"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          ครั้ง
                        </span>
                                                </div>
                                                <div className="flex flex-wrap sm:flex-auto items-center">
                                                    <label htmlFor="last-time-of-radiation"
                                                           className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 ml-6">
                                                        ครั้งล่าสุด วัน/เดือน/ปี
                                                    </label>
                                                    <div
                                                        className="relative sm:ml-2 flex flex-auto sm:grow-0 items-center lg:h-5 w-fit mt-2 sm:mt-0 ml-6">
                                                        <DatePicker selected={dob} onChange={(date) => setDob(date)}
                                                                    locale="th"
                                                                    peekNextMonth
                                                                    showMonthDropdown
                                                                    showYearDropdown
                                                                    disabledKeyboardNavigation
                                                                    dropdownMode="select"
                                                                    dateFormat="dd/MM/yyyy"
                                                                    filterDate={isWeekday}
                                                                    className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                    placeholderText="วัน/เดือน/ปี ค.ศ."
                                                                    name="doctor-treatment-plan"
                                                                    id="last-time-of-radiation"

                                                        />
                                                        <div
                                                            className="flex absolute inset-y-0 right-0 items-center pr-3 mt-1 pointer-events-none">
                                                            <CalendarDaysIcon
                                                                className="h-6 w-6 text-gray-500 dark:text-gray-400 sm:h-5 sm:w-5"
                                                                aria-hidden="true"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div className="flex flex-wrap lg:flex-auto items-center mt-2 sm:mt-0">
                                                <input
                                                    id="targeted-therapy"
                                                    name="location-of-cancer"
                                                    type="checkbox"
                                                    className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                <label htmlFor="other-location"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    ยามะเร็งแบบมุ่งเป้า (Targeted therapy) ระบุ
                                                </label>
                                                <div
                                                    className="sm:ml-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 ml-6">
                                                    <input
                                                        type="text"
                                                        name="location-of-cancer"
                                                        id="targeted-therapy-details"
                                                        className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                </div>
                                            </div>
                                            <div className="flex flex-auto items-center mt-2 sm:mt-0">
                                                <input
                                                    id="other-location"
                                                    name="location-of-cancer"
                                                    type="checkbox"
                                                    className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                <label htmlFor="other-location"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    อื่นๆ
                                                </label>
                                                <div className="ml-2 flex flex-auto w-fit h-5 items-center">
                                                    <input
                                                        type="text"
                                                        name="location-of-cancer"
                                                        id="other-location-details"
                                                        className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>

                                <div className="col-span-2">
                                    <fieldset>
                                        <legend className="block text-sm text-gray-600 dark:text-gray-200">

                                            อาการทั่วไป (ตอบได้มากกว่า 1 ข้อ)
                                        </legend>

                                        <div className="space-y-3 mt-2">
                                            <div className="flex flex-wrap lg:flex-auto items-center mt-2 sm:mt-4">
                                                <input
                                                    id="location-of-pain"
                                                    name="general-symptoms"
                                                    type="checkbox"
                                                    className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                <label htmlFor="location-of-pain"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    ปวด ตำแหน่งที่ปวด
                                                </label>
                                                <div
                                                    className="sm:ml-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 ml-6">
                                                    <input
                                                        type="text"
                                                        name="general-symptoms"
                                                        id="location-of-pain"
                                                        className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                </div>
                                                <div className="flex flex-wrap sm:grow-0 items-center">
                                                    <label htmlFor="highest-level-of-pain"
                                                           className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 ml-6">
                                                        ระดับความปวดที่มากที่สุด
                                                    </label>
                                                    <div
                                                        className="sm:ml-2 flex flex-auto sm:grow-0 items-center lg:h-5 mt-2 sm:mt-0 ml-6">
                                                        <input
                                                            type="number"
                                                            name="general-symptoms"
                                                            id="highest-level-of-pain"
                                                            min="0"
                                                            className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                        <span
                                                            className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="pl-6">
                                                <fieldset>
                                                    <legend className="block text-sm text-gray-600 dark:text-gray-200">
                                                        ลักษณะอาการปวด (ตอบได้มากกว่า 1 ข้อ)
                                                    </legend>

                                                    <div className="grid grid-rows-2 sm:grid-flow-col">
                                                        <div className="flex flex-auto items-center mt-2 xl:mt-3">
                                                            <input
                                                                id="severe-pain"
                                                                name="pain-characteristics"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="severe-pain"
                                                                   className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                                ปวดหนัก
                                                            </label>
                                                        </div>
                                                        <div className="flex flex-auto items-center mt-2 xl:mt-3">
                                                            <input
                                                                id="nipple-pain"
                                                                name="location-of-cancer"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="nipple-pain"
                                                                   className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                                ปวดจุก
                                                            </label>
                                                        </div>
                                                        <div className="flex flex-auto items-center mt-2 xl:mt-3">
                                                            <input
                                                                id="persistent-pain"
                                                                name="location-of-cancer"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="persistent-pain"
                                                                   className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                                ปวดตื้อ
                                                            </label>
                                                        </div>
                                                        <div className="flex flex-auto items-center mt-2 xl:mt-3">
                                                            <input
                                                                id="intense-pain"
                                                                name="location-of-cancer"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="intense-pain"
                                                                   className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                                ปวดแน่นๆ
                                                            </label>
                                                        </div>
                                                        <div className="flex flex-auto items-center mt-2 xl:mt-3">
                                                            <input
                                                                id="sharp-pain"
                                                                name="location-of-cancer"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="sharp-pain"
                                                                   className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                                ปวดจี๊ด
                                                            </label>
                                                        </div>
                                                        <div className="flex flex-auto items-center mt-2 xl:mt-3">
                                                            <input
                                                                id="burning-pain"
                                                                name="location-of-cancer"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="burning-pain"
                                                                   className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                                ปวดแสบปวดร้อน
                                                            </label>
                                                        </div>
                                                        <div className="flex flex-auto items-center mt-2 xl:mt-3">
                                                            <input
                                                                id="numb-pain"
                                                                name="location-of-cancer"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="pleura"
                                                                   className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                                ปวดชา
                                                            </label>
                                                        </div>
                                                        <div className="flex flex-auto items-center mt-2 xl:mt-3">
                                                            <input
                                                                id="anguish-pain"
                                                                name="location-of-cancer"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="anguish-pain"
                                                                   className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                                ปวดร้าว
                                                            </label>
                                                        </div>
                                                        <div className="flex flex-auto items-center mt-2 xl:mt-3">
                                                            <input
                                                                id="abrupt-pain"
                                                                name="location-of-cancer"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="abrupt-pain"
                                                                   className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                                ปวดตุ๊บ
                                                            </label>
                                                        </div>
                                                        <div className="flex flex-auto items-center mt-2 xl:mt-3">
                                                            <input
                                                                id="needle-like-pain"
                                                                name="location-of-cancer"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="needle-like-pain"
                                                                   className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                                ปวดเหมือนเข็มแทง
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div className="grid grid-rows-2 sm:grid-flow-col">
                                                <div className="flex flex-auto items-center mt-2 xl:mt-3">
                                                    <input
                                                        id="constipation"
                                                        name="general-symptoms"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="constipation"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                        ท้องผูก
                                                    </label>
                                                </div>
                                                <div className="flex flex-auto items-center mt-2 xl:mt-3">
                                                    <input
                                                        id="diarrhea"
                                                        name="general-symptoms"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="diarrhea"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                        ท้องเสีย
                                                    </label>
                                                </div>
                                                <div className="flex flex-auto items-center mt-2 xl:mt-3">
                                                    <input
                                                        id="seep-confused"
                                                        name="general-symptoms"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="seep-confused"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                        ซึม สับสน
                                                    </label>
                                                </div>
                                                <div className="flex flex-auto items-center mt-2 xl:mt-3">
                                                    <input
                                                        id="panting-difficulty-breathing"
                                                        name="general-symptoms"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="panting-difficulty-breathing"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                        หอบเหนื่อย/หายใจลำบาก
                                                    </label>
                                                </div>
                                                <div className="flex flex-auto items-center mt-2 xl:mt-3">
                                                    <input
                                                        id="nausea-vomiting"
                                                        name="general-symptoms"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="nausea-vomiting"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                        คลื่นไส้/อาเจียน
                                                    </label>
                                                </div>
                                                <div className="flex flex-auto items-center mt-2 xl:mt-3">
                                                    <input
                                                        id="depression-anxiety"
                                                        name="general-symptoms"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="depression-anxiety"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                        ซึมเศร้า วิตกกังวล
                                                    </label>
                                                </div>
                                                <div className="flex flex-auto items-center mt-2 xl:mt-3">
                                                    <input
                                                        id="cannot-sleep"
                                                        name="general-symptoms"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="cannot-sleep"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                        นอนไม่หลับ
                                                    </label>
                                                </div>
                                                <div className="flex flex-auto items-center mt-2 xl:mt-3">
                                                    <input
                                                        id="anorexia"
                                                        name="general-symptoms"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="anorexia"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                        เบื่ออาหาร
                                                    </label>
                                                </div>

                                            </div>

                                        </div>
                                    </fieldset>
                                </div>

                                <div className="col-span-2">

                                    <fieldset>
                                        <legend className="block text-sm text-gray-800 dark:text-gray-400">
                                            ประวัติยาปัจจุบัน
                                        </legend>
                                        <p className="block text-sm text-gray-600 dark:text-gray-200 mt-2">
                                            1. ยาแก้ปวด(ตอบได้มากกว่า 1 ข้อ) ระบุ
                                        </p>

                                        <div className="space-y-6 mt-2">
                                            <div className="flex flex-wrap lg:flex-auto items-center mt-2 sm:mt-4">
                                                <input
                                                    id="pain-medication-paracetamol"
                                                    name="current-drug-history"
                                                    type="checkbox"
                                                    className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                <label htmlFor="pain-medication-paracetamol"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    Paracetamol ขนาดยา
                                                </label>
                                                <div
                                                    className="sm:ml-2 flex flex-auto sm:grow-0 items-center lg:h-5 mt-2 sm:mt-0 ml-6">
                                                    <input
                                                        type="text"
                                                        name="pain-medication-paracetamol"
                                                        id="paracetamol-dosage"
                                                        className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                </div>
                                                <div className="flex flex-wrap sm:grow-0 items-center">
                                                    <label htmlFor="pain-score-of-paracetamol"
                                                           className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 ml-6">
                                                        คะแนนปวดลดลงเหลือ
                                                    </label>
                                                    <div
                                                        className="sm:ml-2 flex flex-auto sm:grow-0 items-center lg:h-5 mt-2 sm:mt-0 ml-6">
                                                        <input
                                                            type="number"
                                                            name="general-symptoms"
                                                            id="pain-score-of-paracetamol"
                                                            min="0"
                                                            className="block w-full rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                        <span
                                                            className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 px-3 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="flex flex-wrap lg:flex-auto items-center mt-2 sm:mt-4">
                                                <input
                                                    id="pain-medication-nsaids"
                                                    name="current-drug-history"
                                                    type="checkbox"
                                                    className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                <label htmlFor="pain-medication-nsaids"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    NSAIDs ระบุ ชนิด/ขนาดยา
                                                </label>
                                                <div
                                                    className="sm:ml-2 flex flex-auto sm:grow-0 items-center lg:h-5 mt-2 sm:mt-0 ml-6">
                                                    <input
                                                        type="text"
                                                        name="pain-medication-nsaids"
                                                        id="nsaids-dosage"
                                                        className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                </div>
                                                <div className="flex flex-wrap sm:grow-0 items-center">
                                                    <label htmlFor="pain-score-of-nsaids"
                                                           className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 ml-6">
                                                        คะแนนปวดลดลงเหลือ
                                                    </label>
                                                    <div
                                                        className="sm:ml-2 flex flex-auto sm:grow-0 items-center lg:h-5 mt-2 sm:mt-0 ml-6">
                                                        <input
                                                            name="pain-medication-nsaids"
                                                            id="pain-score-of-nsaids"
                                                            min="0"
                                                            className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                        <span
                                                            className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="flex flex-wrap lg:flex-auto items-center mt-2 sm:mt-4">
                                                <input
                                                    id="pain-medication-tramadol"
                                                    name="current-drug-history"
                                                    type="checkbox"
                                                    className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                <label htmlFor="pain-medication-tramadol"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    Tramadol ขนาดยา
                                                </label>
                                                <div
                                                    className="sm:ml-2 flex flex-auto sm:grow-0 items-center lg:h-5 mt-2 sm:mt-0 ml-6">
                                                    <input
                                                        type="text"
                                                        name="pain-medication-tramadol"
                                                        id="tramadol-dosage"
                                                        className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                </div>
                                                <div className="flex flex-wrap sm:grow-0 items-center">
                                                    <label htmlFor="pain-score-of-tramadol"
                                                           className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 ml-6">
                                                        คะแนนปวดลดลงเหลือ
                                                    </label>
                                                    <div
                                                        className="sm:ml-2 flex flex-auto sm:grow-0 items-center lg:h-5 mt-2 sm:mt-0 ml-6">
                                                        <input
                                                            type="number"
                                                            name="pain-medication-tramadol"
                                                            id="pain-score-of-tramadol"
                                                            min="0"
                                                            className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                        <span
                                                            className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="flex flex-wrap lg:flex-auto items-center mt-2 sm:mt-4">
                                                <input
                                                    id="pain-medication-codeine"
                                                    name="current-drug-history"
                                                    type="checkbox"
                                                    className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                <label htmlFor="pain-medication-codeine"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    Codeine ขนาดยา
                                                </label>
                                                <div
                                                    className="sm:ml-2 flex flex-auto sm:grow-0 items-center lg:h-5 mt-2 sm:mt-0 ml-6">
                                                    <input
                                                        type="text"
                                                        name="pain-medication-codeine"
                                                        id="codeine-dosage"
                                                        className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                </div>
                                                <div className="flex flex-wrap sm:grow-0 items-center">
                                                    <label htmlFor="pain-score-of-codeine"
                                                           className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 ml-6">
                                                        คะแนนปวดลดลงเหลือ
                                                    </label>
                                                    <div
                                                        className="sm:ml-2 flex flex-auto sm:grow-0 items-center lg:h-5 mt-2 sm:mt-0 ml-6">
                                                        <input
                                                            type="number"
                                                            name="pain-medication-codeine"
                                                            id="pain-score-of-codeine"
                                                            min="0"
                                                            className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                        <span
                                                            className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="flex flex-wrap lg:flex-auto items-center mt-2 sm:mt-4">
                                                <input
                                                    id="pain-medication-mst-kapanol"
                                                    name="current-drug-history"
                                                    type="checkbox"
                                                    className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                <label htmlFor="pain-medication-mst-kapanol"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    MST/Kapanol ขนาดยา
                                                </label>
                                                <div
                                                    className="sm:ml-2 flex flex-auto sm:grow-0 items-center lg:h-5 mt-2 sm:mt-0 ml-6">
                                                    <input
                                                        type="text"
                                                        name="pain-medication-mst-kapanol"
                                                        id="mst-kapanol-dosage"
                                                        className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                </div>
                                                <div className="flex flex-wrap sm:grow-0 items-center">
                                                    <label htmlFor="pain-score-of-mst-kapanol"
                                                           className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 ml-6">
                                                        คะแนนปวดลดลงเหลือ
                                                    </label>
                                                    <div
                                                        className="sm:ml-2 flex flex-auto sm:grow-0 items-center lg:h-5 mt-2 sm:mt-0 ml-6">
                                                        <input
                                                            type="number"
                                                            name="pain-medication-mst-kapanol"
                                                            id="pain-score-of-mst-kapanol"
                                                            min="0"
                                                            className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                        <span
                                                            className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="flex flex-wrap lg:flex-auto items-center mt-2 sm:mt-4">
                                                <input
                                                    id="pain-medication-morphine"
                                                    name="gcurrent-drug-history"
                                                    type="checkbox"
                                                    className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                <label htmlFor="morphine-dosage"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    Morphine syrup/Morphine IR ขนาดยา
                                                </label>
                                                <div
                                                    className="sm:ml-2 flex flex-auto sm:grow-0 items-center lg:h-5 mt-2 sm:mt-0 ml-6">
                                                    <input
                                                        type="text"
                                                        name="pain-medication-morphine"
                                                        id="morphine-dosage"
                                                        className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                </div>
                                                <div className="flex flex-wrap sm:grow-0 items-center">
                                                    <label htmlFor="pain-score-of-morphine"
                                                           className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 ml-6">
                                                        คะแนนปวดลดลงเหลือ
                                                    </label>
                                                    <div
                                                        className="sm:ml-2 flex flex-auto sm:grow-0 items-center lg:h-5 mt-2 sm:mt-0 ml-6">
                                                        <input
                                                            type="number"
                                                            name="pain-medication-morphine"
                                                            id="pain-score-of-morphine"
                                                            min="0"
                                                            className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                        <span
                                                            className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="flex flex-wrap lg:flex-auto items-center mt-2 sm:mt-4">
                                                <input
                                                    id="pain-medication-fentanyl"
                                                    name="current-drug-history"
                                                    type="checkbox"
                                                    className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                <label htmlFor="fentanyl-dosage"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    Fentanyl ขนาดยา
                                                </label>
                                                <div
                                                    className="sm:ml-2 flex flex-auto sm:grow-0 items-center lg:h-5 mt-2 sm:mt-0 ml-6">
                                                    <input
                                                        type="text"
                                                        name="pain-medication-fentanyl"
                                                        id="fentanyl-dosage"
                                                        className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                </div>
                                                <div className="flex flex-wrap sm:grow-0 items-center">
                                                    <label htmlFor="pain-score-of-fentanyl"
                                                           className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 ml-6">
                                                        คะแนนปวดลดลงเหลือ
                                                    </label>
                                                    <div
                                                        className="sm:ml-2 flex flex-auto sm:grow-0 items-center lg:h-5 mt-2 sm:mt-0 ml-6">
                                                        <input
                                                            type="number"
                                                            name="pain-medication-fentanyl"
                                                            id="pain-score-of-fentanyl"
                                                            min="0"
                                                            className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                        <span
                                                            className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="flex flex-wrap lg:flex-auto items-center mt-2 sm:mt-4">
                                                <input
                                                    id="pain-medication-methadone"
                                                    name="current-drug-history"
                                                    type="checkbox"
                                                    className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                <label htmlFor="methadone-dosage"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    Methadone ขนาดยา
                                                </label>
                                                <div
                                                    className="sm:ml-2 flex flex-auto sm:grow-0 items-center lg:h-5 mt-2 sm:mt-0 ml-6">
                                                    <input
                                                        type="text"
                                                        name="pain-medication-methadone"
                                                        id="methadone-dosage"
                                                        className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                </div>
                                                <div className="flex flex-wrap sm:grow-0 items-center">
                                                    <label htmlFor="pain-score-of-methadone"
                                                           className="sm:ml-6 lg:ml-2 block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 ml-6">
                                                        คะแนนปวดลดลงเหลือ
                                                    </label>
                                                    <div
                                                        className="sm:ml-2 flex flex-auto sm:grow-0 items-center lg:h-5 mt-2 sm:mt-0 ml-6">
                                                        <input
                                                            type="number"
                                                            name="pain-medication-methadone"
                                                            id="pain-score-of-methadone"
                                                            min="0"
                                                            className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                        <span
                                                            className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="flex flex-wrap lg:flex-auto items-center mt-2 sm:mt-4">
                                                <input
                                                    id="pain-medication-gabapentin-pregabalin"
                                                    name="current-drug-history"
                                                    type="checkbox"
                                                    className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                <label htmlFor="gabapentin-pregabalin-dosage"
                                                       className="ml-2 block text-sm text-gray-600 dark:text-gray-200">
                                                    Gabapentin/Pregabalin ขนาดยา
                                                </label>
                                                <div
                                                    className="sm:ml-2 flex flex-auto sm:grow-0 items-center lg:h-5 mt-2 sm:mt-0 ml-6">
                                                    <input
                                                        type="number"
                                                        name="pain-medication-gabapentin-pregabalin"
                                                        id="gabapentin-pregabalin-dosage"
                                                        min="0"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /วัน
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div className="col-span-2">
                                    <label htmlFor="other-pain-medication"
                                           className="block text-sm text-gray-600 dark:text-gray-200">
                                        2. ยาบรรเทาอาการอื่นๆ(ตอบได้มากกว่า 1 ข้อ) ระบุ ชนิด/ขนาดยา
                                    </label>
                                    <textarea
                                        name="other-pain-medication"
                                        id="other-pain-medication"
                                        autoComplete="other-pain-medication"
                                        required
                                        className="block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                    />
                                </div>
                                <div className="col-span-2">
                                    <label htmlFor="personal-medication"
                                           className="block text-sm text-gray-600 dark:text-gray-200">
                                        3. ยารักษาโรคประจำตัว ระบุ ชนิด/ขนาดยา
                                    </label>
                                    <textarea
                                        name="personal-medication"
                                        id="personal-medication"
                                        autoComplete="personal-medication"
                                        required
                                        className="block w-full px-3 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                    />
                                </div>

                            </div>
                            <div className="bg-gray-50 dark:bg-gray-700 rounded-b-md px-4 py-3 text-right sm:px-6">
                                {/*<button*/}
                                {/*    type="button"*/}
                                {/*    onClick={handleBack}*/}
                                {/*    className="inline-flex justify-center px-6 py-3 tracking-wide text-sm text-white transition-colors duration-300 transform bg-blue-600 rounded-md hover:bg-blue-700 focus:outline-none focus:bg-blue-700 focus:ring focus:ring-blue-500 focus:ring-opacity-50">*/}
                                {/*    ย้อนกลับ*/}
                                {/*</button>*/}


                                <button
                                    type="submit"
                                    className="inline-flex justify-center px-6 py-3 tracking-wide text-sm text-white transition-colors duration-300 transform bg-blue-600 rounded-md hover:bg-blue-700 focus:outline-none focus:bg-blue-700 focus:ring focus:ring-blue-500 focus:ring-opacity-50">
                                    บันทึกข้อมูล
                                </button>

                            </div>
                        </div>

                    </form>
                </div>

                {/*<Stepper steps={['Step 1', 'Step 2', 'Step 3']}>*/}
                {/*    <div>Content for step 1</div>*/}
                {/*    <div>Content for step 2</div>*/}
                {/*    <div>Content for step 3</div>*/}
                {/*</Stepper>*/}

            </section>
        </>
    )

}

export default TreatmentHistory;