import React, {useEffect, useState} from "react";
import Header from "../components/Header.jsx";
import DatePicker from "react-datepicker";
import {CalendarDaysIcon} from "@heroicons/react/20/solid";
import {getDay} from "date-fns";
import moment from "moment";
import {Link} from "react-router-dom";


function ResearchData() {

    const [dob, setDob] = useState(new Date());
    const [startTime, setStartTime] = useState(new Date());
    const [endTime, setEndTime] = useState(new Date());
    const [sleepTime, setSleepTime] = useState("0");
    // console.log(endTime)
    // console.log(startTime)
    // console.log(moment.duration(moment(endTime).diff(moment(startTime))).asHours());
    const calHours = moment.duration(moment(endTime).diff(moment(startTime))).asHours();
    const isWeekday = (date) => {
        const day = getDay(date);
        return day !== 0 && day !== 6;
    };

    useEffect(() => {
        setSleepTime(calHours.toString());
    }, [startTime, endTime])

    return (
        <>
            <Header/>
            <section className="mx-auto max-w-7xl px-2 py-10 sm:px-6 lg:px-8 dark:bg-gray-900">
                <div className="px-4 sm:px-0">
                    <h3 className="text-xl font-medium leading-6 text-gray-700 dark:text-white">แบบเก็บข้อมูล (Case
                        record form):
                        การศึกษาประสิทธิผลของแคนนาบินอยด์และ
                        การประเมินคุณภาพชีวิตในผู้ป่วยมะเร็งระยะแพร่กระจาย
                    </h3>
                    <div className="grid grid-cols-1 sm:grid-cols-2 mt-2">
                        <div>
                            <p className="text-md text-gray-600 dark:text-gray-200 dark:text-gray-400">
                                ส่วนที่ 4 : แบบบันทึกข้อมูลการวิจัย
                            </p>
                        </div>
                        <div className="flex flex-wrap sm:flex-auto">
                            <div
                                className="flex flex-auto sm:flex-1 items-center">
                                <p className="text-md text-gray-600 dark:text-gray-200 dark:text-gray-400">
                                    STUDY SITE :</p>
                            </div>
                            <div
                                className="flex flex-auto sm:flex-1 items-center">
                                <p className="text-md text-gray-600 dark:text-gray-200 dark:text-gray-400">
                                    STUDY ID :</p>
                            </div>

                        </div>

                    </div>
                    <div
                        className="mt-5 w-full bg-white shadow-md rounded-md rounded-b-none px-1 pt-1 dark:bg-gray-800">
                        <div className="flex flex-row gap-3 px-4">
                            <div>
                                <Link to="/telephoneInquiry">
                                    <button
                                        className="text-blue-600 hover:text-blue-700 duration-300 font-medium border-b-2 border-blue-600 hover:border-blue-700 px-3 py-3 text-center text-sm dark:text-gray-200">
                                        4.1 แบบบันทึกการสอบถามข้อมูลทางโทรศัพท์
                                    </button>
                                </Link>

                            </div>
                            <div>
                                <Link to="/visitInquiry">
                                    <button
                                        className="border-b-2 border-transparent rounded-t-lg text-gray-500 hover:text-gray-600 hover:border-gray-300 px-3 py-3 text-center text-sm dark:text-gray-200">
                                        4.2 แบบบันทึกข้อมูลการมาตรวจติดตามที่โรงพยาบาล
                                    </button>
                                </Link>
                            </div>
                        </div>

                    </div>

                    <form>
                        <div className="overflow-visible shadow rounded-md">
                            <div
                                className="bg-white rounded-md rounded-b-none rounded-t-none shadow-md dark:bg-gray-800 px-4 py-5 sm:p-6">
                                {/*<div className="block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0">*/}
                                {/*    <h6>4.1 แบบบันทึกการสอบถามข้อมูลทางโทรศัพท์</h6>*/}
                                {/*</div>*/}
                                <div
                                    className="grid grid-cols-1 md:grid-cols-12 border border-gray-300 border-separate bg-gray-50 dark:bg-gray-700 dark:border-gray-600">
                                    <div
                                        className="sm:col-span-2 py-4 row-start-1 row-end-auto text-center text-sm text-gray-600 dark:text-gray-200">
                                        Day
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-slate-300 dark:bg-gray-700 dark:border-gray-600 py-4 row-start-2 sm:row-start-auto text-center text-sm text-gray-600 dark:text-gray-200">
                                        3
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-slate-300 dark:bg-gray-700 dark:border-gray-600 py-4 sm:row-start-auto text-center text-sm text-gray-600 dark:text-gray-200">
                                        5
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-slate-300 dark:bg-gray-700 dark:border-gray-600 py-4 sm:row-start-auto text-center text-sm text-gray-600 dark:text-gray-200">
                                        7
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-slate-300 dark:bg-gray-700 dark:border-gray-600 py-4 sm:row-start-auto text-center text-sm text-gray-600 dark:text-gray-200">
                                        9
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-slate-300 dark:bg-gray-700 dark:border-gray-600 py-4 sm:row-start-auto text-center text-sm text-gray-600 dark:text-gray-200">
                                        11
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-t border-b border-slate-300 dark:bg-gray-700 dark:border-gray-600 py-6 text-center text-sm text-gray-600 dark:text-gray-200">
                                        วันที่
                                    </div>
                                    <div
                                        className="sm:col-span-2 border sm:border-r-0 border-slate-300 dark:bg-gray-700 dark:border-gray-600 py-4 row-start-3 sm:row-start-auto">
                                        <div className="flex flex-wrap sm:flex-auto items-center sm:mx-0 sm:my-2 mx-2">
                                            <div
                                                className="relative sm:ml-2 flex flex-auto items-center lg:h-5 w-fit mt-2 sm:mt-0 mx-2">
                                                <DatePicker selected={dob} onChange={(date) => setDob(date)}
                                                            locale="th"
                                                            peekNextMonth
                                                            showMonthDropdown
                                                            showYearDropdown
                                                            disabledKeyboardNavigation
                                                            dropdownMode="select"
                                                            dateFormat="dd/MM/yyyy"
                                                            filterDate={isWeekday}
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                            placeholderText="วัน/เดือน/ปี ค.ศ."
                                                            name="telephone-inquiry-day3"
                                                            id="telephone-inquiry-day3"
                                                />
                                                <div
                                                    className="flex absolute inset-y-0 right-0 items-center pr-3 mt-1 pointer-events-none">
                                                    <CalendarDaysIcon
                                                        className="h-6 w-6 text-gray-500 dark:text-gray-400 sm:h-5 sm:w-5"
                                                        aria-hidden="true"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border sm:border-r-0 border-slate-300 dark:bg-gray-700 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="flex flex-wrap sm:flex-auto items-center sm:mx-0 sm:my-2 mx-2">
                                            <div
                                                className="relative sm:ml-2 flex flex-auto items-center lg:h-5 w-fit mt-2 sm:mt-0 mx-2">
                                                <DatePicker selected={dob} onChange={(date) => setDob(date)}
                                                            locale="th"
                                                            peekNextMonth
                                                            showMonthDropdown
                                                            showYearDropdown
                                                            disabledKeyboardNavigation
                                                            dropdownMode="select"
                                                            dateFormat="dd/MM/yyyy"
                                                            filterDate={isWeekday}
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                            placeholderText="วัน/เดือน/ปี ค.ศ."
                                                            name="telephone-inquiry-day5"
                                                            id="telephone-inquiry-day5"
                                                />
                                                <div
                                                    className="flex absolute inset-y-0 right-0 items-center pr-3 mt-1 pointer-events-none">
                                                    <CalendarDaysIcon
                                                        className="h-6 w-6 text-gray-500 dark:text-gray-400 sm:h-5 sm:w-5"
                                                        aria-hidden="true"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border sm:border-r-0 border-slate-300 dark:bg-gray-700 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="flex flex-wrap sm:flex-auto items-center sm:mx-0 sm:my-2 mx-2">
                                            <div
                                                className="relative sm:ml-2 flex flex-auto items-center lg:h-5 w-fit mt-2 sm:mt-0 mx-2">
                                                <DatePicker selected={dob} onChange={(date) => setDob(date)}
                                                            locale="th"
                                                            peekNextMonth
                                                            showMonthDropdown
                                                            showYearDropdown
                                                            disabledKeyboardNavigation
                                                            dropdownMode="select"
                                                            dateFormat="dd/MM/yyyy"
                                                            filterDate={isWeekday}
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                            placeholderText="วัน/เดือน/ปี ค.ศ."
                                                            name="telephone-inquiry-day7"
                                                            id="telephone-inquiry-day7"
                                                />
                                                <div
                                                    className="flex absolute inset-y-0 right-0 items-center pr-3 mt-1 pointer-events-none">
                                                    <CalendarDaysIcon
                                                        className="h-6 w-6 text-gray-500 dark:text-gray-400 sm:h-5 sm:w-5"
                                                        aria-hidden="true"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border sm:border-r-0 border-slate-300 dark:bg-gray-700 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="flex flex-wrap sm:flex-auto items-center sm:mx-0 sm:my-2 mx-2">
                                            <div
                                                className="relative sm:ml-2 flex flex-auto items-center lg:h-5 w-fit mt-2 sm:mt-0 mx-2">
                                                <DatePicker selected={dob} onChange={(date) => setDob(date)}
                                                            locale="th"
                                                            peekNextMonth
                                                            showMonthDropdown
                                                            showYearDropdown
                                                            disabledKeyboardNavigation
                                                            dropdownMode="select"
                                                            dateFormat="dd/MM/yyyy"
                                                            filterDate={isWeekday}
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                            placeholderText="วัน/เดือน/ปี ค.ศ."
                                                            name="telephone-inquiry-day19"
                                                            id="telephone-inquiry-day19"
                                                />
                                                <div
                                                    className="flex absolute inset-y-0 right-0 items-center pr-3 mt-1 pointer-events-none">
                                                    <CalendarDaysIcon
                                                        className="h-6 w-6 text-gray-500 dark:text-gray-400 sm:h-5 sm:w-5"
                                                        aria-hidden="true"/>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <div
                                        className="sm:col-span-2 border sm:border-r-0 border-slate-300 dark:bg-gray-700 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="flex flex-wrap sm:flex-auto items-center sm:mx-0 sm:my-2 mx-2">
                                            <div
                                                className="relative sm:ml-2 flex flex-auto items-center lg:h-5 w-fit mt-2 sm:mt-0 mx-2">
                                                <DatePicker selected={dob} onChange={(date) => setDob(date)}
                                                            locale="th"
                                                            peekNextMonth
                                                            showMonthDropdown
                                                            showYearDropdown
                                                            disabledKeyboardNavigation
                                                            dropdownMode="select"
                                                            dateFormat="dd/MM/yyyy"
                                                            filterDate={isWeekday}
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                            placeholderText="วัน/เดือน/ปี ค.ศ."
                                                            name="telephone-inquiry-day11"
                                                            id="telephone-inquiry-day11"
                                                />
                                                <div
                                                    className="flex absolute inset-y-0 right-0 items-center pr-3 mt-1 pointer-events-none">
                                                    <CalendarDaysIcon
                                                        className="h-6 w-6 text-gray-500 dark:text-gray-400 sm:h-5 sm:w-5"
                                                        aria-hidden="true"/>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <div
                                        className="sm:col-span-2 py-5 border-b border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 text-center text-sm text-gray-600 dark:text-gray-200">
                                        Pain score
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="telephone-inquiry-day3"
                                                        id="telephone-inquiry-pain-score-day3"
                                                        min="0"
                                                        max="10"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="telephone-inquiry-day5"
                                                        id="telephone-inquiry-pain-score-day5"
                                                        min="0"
                                                        max="10"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="telephone-inquiry-day7"
                                                        id="telephone-inquiry-pain-score-day7"
                                                        min="0"
                                                        max="10"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="telephone-inquiry-day19"
                                                        id="telephone-inquiry-pain-score-day19"
                                                        min="0"
                                                        max="10"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="telephone-inquiry-day11"
                                                        id="telephone-inquiry-pain-score-day11"
                                                        min="0"
                                                        max="10"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 py-5 border-b border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 text-center text-sm text-gray-600 dark:text-gray-200">
                                        Sleep (ESAS)
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="telephone-inquiry-day3"
                                                        id="telephone-inquiry-sleep-day3"
                                                        min="0"
                                                        max="10"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="telephone-inquiry-day5"
                                                        id="telephone-inquiry-sleep-day5"
                                                        min="0"
                                                        max="10"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="telephone-inquiry-day7"
                                                        id="telephone-inquiry-sleep-day7"
                                                        min="0"
                                                        max="10"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="telephone-inquiry-day9"
                                                        id="telephone-inquiry-sleep-day9"
                                                        min="0"
                                                        max="10"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="telephone-inquiry-day11"
                                                        id="telephone-inquiry-sleep-day11"
                                                        min="0"
                                                        max="10"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 py-5 border-b border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                        Loss of appetite (ESAS)
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="telephone-inquiry-day3"
                                                        id="telephone-inquiry-loss-day3"
                                                        min="0"
                                                        max="10"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="telephone-inquiry-day5"
                                                        id="telephone-inquiry-loss-day5"
                                                        min="0"
                                                        max="10"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="telephone-inquiry-day7"
                                                        id="telephone-inquiry-loss-day7"
                                                        min="0"
                                                        max="10"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="telephone-inquiry-day9"
                                                        id="telephone-inquiry-loss-day9"
                                                        min="0"
                                                        max="10"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="telephone-inquiry-day11"
                                                        id="telephone-inquiry-loss-day11"
                                                        min="0"
                                                        max="10"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 bg-white border-slate-300 dark:bg-gray-800 dark:border-gray-600">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-8 my-1">
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words">AE</h6>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words mt-2">ตาพร่า
                                                    เป็นลม
                                                    (Fainting/syncope)</h6>
                                            </div>
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words pt-4">
                                                    วิงเวียน มึนงง (Dizziness)
                                                </h6>
                                            </div>
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words pt-9">
                                                    สับสน (confusion/delirium)
                                                </h6>
                                            </div>
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words pt-9">
                                                    ปากแห้ง คอแห้ง
                                                </h6>
                                            </div>
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words py-9">
                                                    คลื่นไส้ อาเจียน
                                                </h6>
                                            </div>
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                    ชักเกร็ง
                                                </h6>
                                            </div>
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words pt-9">
                                                    หูแว่ว ประสาทหลอน
                                                </h6>
                                            </div>
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words pt-9">
                                                    Psychosis (hallucinations/delusions)
                                                </h6>
                                            </div>
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words pt-4">
                                                    ใจสั่น
                                                </h6>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-col items-center">
                                                    <label htmlFor="ae-other-detail-set1"
                                                           className="sm:ml-6 lg:ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        อื่นๆ ระบุ
                                                    </label>
                                                    <div
                                                        className="sm:px-2 sm:py-2 flex flex-auto items-center w-full sm:basis-2/3">
                                                        <textarea
                                                            name="telephone-inquiry"
                                                            id="ae-other-detail-set1"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 pt-8 pb-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-8">
                                            <div className="pt-1">
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-fainting"
                                                                name="telephone-inquiry-day3"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-fainting"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-fainting"
                                                                name="telephone-inquiry-day3"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-fainting"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-fainting"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day3"
                                                            id="grade-of-ae-fainting"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-dizziness-day3"
                                                                name="telephone-inquiry-day3"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-dizziness-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-dizziness-day3"
                                                                name="telephone-inquiry-day3"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-dizziness-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-dizziness-day3"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day3"
                                                            id="grade-of-ae-dizziness-day3"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-confusion-day3"
                                                                name="telephone-inquiry-day3"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-confusion-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-confusion-day3"
                                                                name="telephone-inquiry-day3"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-confusion-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-confusion-day3"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day3"
                                                            id="grade-of-ae-confusion-day3"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-feel-dry-day3"
                                                                name="telephone-inquiry-day3"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-feel-dry-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-feel-dry-day3"
                                                                name="telephone-inquiry-day3"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-feel-dry-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-feel-dry-day3"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day3"
                                                            id="grade-of-ae-feel-dry-day3"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-nausea-vomiting-day3"
                                                                name="telephone-inquiry-day3"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-nausea-vomiting-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-nausea-vomiting-day3"
                                                                name="telephone-inquiry-day3"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-nausea-vomiting-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-nausea-vomiting-day3"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day3"
                                                            id="grade-of-ae-nausea-vomiting-day3"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-convulsions-day3"
                                                                name="telephone-inquiry-day3"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-convulsions-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-convulsions-day3"
                                                                name="telephone-inquiry-day3"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-convulsions-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-convulsions-day3"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day3"
                                                            id="grade-of-ae-convulsions-day3"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-psychedelic-day3"
                                                                name="telephone-inquiry-day3"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-psychedelic-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-psychedelic-day3"
                                                                name="telephone-inquiry-day3"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-psychedelic-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-psychedelic-day3"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day3"
                                                            id="grade-of-ae-psychedelic-day3"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-psychosis-day3"
                                                                name="telephone-inquiry-day3"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-psychosis-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-psychosis-day3"
                                                                name="telephone-inquiry-day3"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-psychosis-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-psychosis-day3"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day3"
                                                            id="grade-of-ae-psychosis-day3"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                    <label htmlFor="grade-of-ae-palpitation-day3"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day3"
                                                            id="grade-of-ae-palpitation-day3"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                    <label htmlFor="grade-of-ae-other-day3"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day3"
                                                            id="grade-of-ae-other-day3"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 pt-8 pb-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-8">
                                            <div className="pt-1">
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-fainting-day5"
                                                                name="telephone-inquiry-day5"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-fainting-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-fainting-day5"
                                                                name="telephone-inquiry-day5"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-fainting-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-fainting-day5"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day5"
                                                            id="grade-of-ae-fainting-day5"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-dizziness-day5"
                                                                name="telephone-inquiry-day5"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-dizziness-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-dizziness-day5"
                                                                name="telephone-inquiry-day5"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-dizziness-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-dizziness-day5"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day5"
                                                            id="grade-of-ae-dizziness-day5"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-confusion-day5"
                                                                name="telephone-inquiry-day5"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-confusion-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-confusion-day5"
                                                                name="telephone-inquiry-day5"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-confusion-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-confusion-day5"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day5"
                                                            id="grade-of-ae-confusion-day5"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-feel-dry-day5"
                                                                name="telephone-inquiry-day5"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-feel-dry-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-feel-dry-day5"
                                                                name="telephone-inquiry-day5"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-feel-dry-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-feel-dry-day5"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day5"
                                                            id="grade-of-ae-feel-dry-day5"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-nausea-vomiting-day5"
                                                                name="telephone-inquiry-day5"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-nausea-vomiting-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-nausea-vomiting-day5"
                                                                name="telephone-inquiry-day5"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-nausea-vomiting-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-nausea-vomiting-day5"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day5"
                                                            id="grade-of-ae-nausea-vomiting-day5"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-convulsions-day5"
                                                                name="telephone-inquiry-day5"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-convulsions-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-convulsions-day5"
                                                                name="telephone-inquiry-day5"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-convulsions-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-convulsions-day5"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day5"
                                                            id="grade-of-ae-convulsions-day5"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-psychedelic-day5"
                                                                name="telephone-inquiry-day5"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-psychedelic-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-psychedelic-day5"
                                                                name="telephone-inquiry-day5"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-psychedelic-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-psychedelic-day5"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day5"
                                                            id="grade-of-ae-psychedelic-day5"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-psychosis-day5"
                                                                name="telephone-inquiry-day5"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-psychosis-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-psychosis-day5"
                                                                name="telephone-inquiry-day5"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-psychosis-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-psychosis-day5"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day5"
                                                            id="grade-of-ae-psychosis-day5"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                    <label htmlFor="grade-of-ae-palpitation-day5"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day5"
                                                            id="grade-of-ae-palpitation-day5"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                    <label htmlFor="grade-of-ae-other-day5"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day5"
                                                            id="grade-of-ae-other-day5"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 pt-8 pb-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-8">
                                            <div className="pt-1">
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-fainting-day7"
                                                                name="telephone-inquiry-day7"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-fainting-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-fainting-day7"
                                                                name="telephone-inquiry-day7"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-fainting-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-fainting-day7"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day7"
                                                            id="grade-of-ae-fainting-day7"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-dizziness-day7"
                                                                name="telephone-inquiry-day7"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-dizziness-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-dizziness-day7"
                                                                name="telephone-inquiry-day7"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-dizziness-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-dizziness-day7"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day7"
                                                            id="grade-of-ae-dizziness-day7"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-confusion-day7"
                                                                name="telephone-inquiry-day7"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-confusion-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-confusion-day7"
                                                                name="telephone-inquiry-day7"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-confusion-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-confusion-day7"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day7"
                                                            id="grade-of-ae-confusion-day7"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-feel-dry-day7"
                                                                name="telephone-inquiry-day7"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-feel-dry-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-feel-dry-day7"
                                                                name="telephone-inquiry-day7"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-feel-dry-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-feel-dry-day7"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day7"
                                                            id="grade-of-ae-feel-dry-day7"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-nausea-vomiting-day7"
                                                                name="telephone-inquiry-day7"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-nausea-vomiting-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-nausea-vomiting-day7"
                                                                name="telephone-inquiry-day7"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-nausea-vomiting-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-nausea-vomiting-day7"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day7"
                                                            id="grade-of-ae-nausea-vomiting-day7"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-convulsions-day7"
                                                                name="telephone-inquiry-day7"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-convulsions-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-convulsions-day7"
                                                                name="telephone-inquiry-day7"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-convulsions-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-convulsions-day7"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day7"
                                                            id="grade-of-ae-convulsions-day7"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-psychedelic-day7"
                                                                name="telephone-inquiry-day7"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-psychedelic-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-psychedelic-day7"
                                                                name="telephone-inquiry-day7"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-psychedelic-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-psychedelic-day7"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day7"
                                                            id="grade-of-ae-psychedelic-day7"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-psychosis-day7"
                                                                name="telephone-inquiry-day7"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-psychosis-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-psychosis-day7"
                                                                name="telephone-inquiry-day7"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-psychosis-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-psychosis-day7"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day7"
                                                            id="grade-of-ae-psychosis-day7"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                    <label htmlFor="grade-of-ae-palpitation-day7"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day7"
                                                            id="grade-of-ae-palpitation-day7"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                    <label htmlFor="grade-of-ae-other-day7"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day7"
                                                            id="grade-of-ae-other-day7"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 pt-8 pb-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-8">
                                            <div className="pt-1">
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-fainting-day9"
                                                                name="telephone-inquiry-day9"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-fainting-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-fainting-day9"
                                                                name="telephone-inquiry-day9"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-fainting-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-fainting-day9"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day9"
                                                            id="grade-of-ae-fainting-day9"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-dizziness-day9"
                                                                name="telephone-inquiry-day9"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-dizziness-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-dizziness-day9"
                                                                name="telephone-inquiry-day9"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-dizziness-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-dizziness-day9"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day9"
                                                            id="grade-of-ae-dizziness-day9"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-confusion-day9"
                                                                name="telephone-inquiry-day9"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-confusion-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-confusion-day9"
                                                                name="telephone-inquiry-day9"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-confusion-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-confusion-day9"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day9"
                                                            id="grade-of-ae-confusion-day9"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-feel-dry-day9"
                                                                name="telephone-inquiry-day9"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-feel-dry-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-feel-dry-day9"
                                                                name="telephone-inquiry-day9"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-feel-dry-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-feel-dry-day9"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day9"
                                                            id="grade-of-ae-feel-dry-day9"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-nausea-vomiting-day9"
                                                                name="telephone-inquiry-day9"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-nausea-vomiting-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-nausea-vomiting-day9"
                                                                name="telephone-inquiry-day9"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-nausea-vomiting-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-nausea-vomiting-day9"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day9"
                                                            id="grade-of-ae-nausea-vomiting-day9"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-convulsions-day9"
                                                                name="telephone-inquiry-day9"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-convulsions-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-convulsions-day9"
                                                                name="telephone-inquiry-day9"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-convulsions-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-convulsions-day9"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day9"
                                                            id="grade-of-ae-convulsions-day9"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-psychedelic-day9"
                                                                name="telephone-inquiry-day9"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-psychedelic-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-psychedelic-day9"
                                                                name="telephone-inquiry-day9"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-psychedelic-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-psychedelic-day9"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day9"
                                                            id="grade-of-ae-psychedelic-day9"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-psychosis-day9"
                                                                name="telephone-inquiry-day9"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-psychosis-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-psychosis-day9"
                                                                name="telephone-inquiry-day9"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-psychosis-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-psychosis-day9"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day9"
                                                            id="grade-of-ae-psychosis-day9"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                    <label htmlFor="grade-of-ae-palpitation-day9"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day9"
                                                            id="grade-of-ae-palpitation-day9"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                    <label htmlFor="grade-of-ae-other-day9"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day9"
                                                            id="grade-of-ae-other-day9"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 pt-8 pb-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-8">
                                            <div className="pt-1">
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-fainting-day11"
                                                                name="telephone-inquiry-day11"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-fainting-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-fainting-day11"
                                                                name="telephone-inquiry-day11"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-fainting-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-fainting-day11"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day11"
                                                            id="grade-of-ae-fainting-day11"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-dizziness-day11"
                                                                name="telephone-inquiry-day11"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-dizziness-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-dizziness-day11"
                                                                name="telephone-inquiry-day11"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-dizziness-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-dizziness-day11"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day11"
                                                            id="grade-of-ae-dizziness-day11"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-confusion-day11"
                                                                name="telephone-inquiry-day11"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-confusion-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-confusion-day11"
                                                                name="telephone-inquiry-day11"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-confusion-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-confusion-day11"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day11"
                                                            id="grade-of-ae-confusion-day11"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-feel-dry-day11"
                                                                name="telephone-inquiry-day11"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-feel-dry-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-feel-dry-day11"
                                                                name="telephone-inquiry-day11"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-feel-dry-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-feel-dry-day11"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day11"
                                                            id="grade-of-ae-feel-dry-day11"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-nausea-vomiting-day11"
                                                                name="telephone-inquiry-day11"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-nausea-vomiting-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-nausea-vomiting-day11"
                                                                name="telephone-inquiry-day11"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-nausea-vomiting-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-nausea-vomiting-day11"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day11"
                                                            id="grade-of-ae-nausea-vomiting-day11"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-convulsions-day11"
                                                                name="telephone-inquiry-day11"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-convulsions-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-convulsions-day11"
                                                                name="telephone-inquiry-day11"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-convulsions-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-convulsions-day11"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day11"
                                                            id="grade-of-ae-convulsions-day11"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-psychedelic-day11"
                                                                name="telephone-inquiry-day11"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-psychedelic-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-psychedelic-day11"
                                                                name="telephone-inquiry-day11"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-psychedelic-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-psychedelic-day11"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day11"
                                                            id="grade-of-ae-psychedelic-day11"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-psychosis-day11"
                                                                name="telephone-inquiry-day11"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-psychosis-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-psychosis-day11"
                                                                name="telephone-inquiry-day11"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-psychosis-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-psychosis-day11"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day11"
                                                            id="grade-of-ae-psychosis-day11"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                    <label htmlFor="grade-of-ae-palpitation-day11"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day11"
                                                            id="grade-of-ae-palpitation-day11"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                    <label htmlFor="grade-of-ae-other-day11"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day11"
                                                            id="grade-of-ae-other-day11"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div
                                        className="sm:col-span-2 border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-8 my-3">
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                    การปรับยา</h6>
                                            </div>
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words py-60">
                                                    เหตุผลของการปรับยา
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-2 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-8">
                                            <div className="pt-1">
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-same-day3"
                                                                name="telephone-inquiry-day3"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-same-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                เท่าเดิม
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center lg:h-5 my-4 mr-2">
                                                                <input
                                                                    type="number"
                                                                    name="telephone-inquiry-day3"
                                                                    id="med-adjustment-same-amount-day3"
                                                                    min="0"
                                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                                <span
                                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">drop/day
                        </span>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-more-day3"
                                                                name="telephone-inquiry-day3"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-more-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                เพิ่มขึ้น เป็น
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center lg:h-5 my-4 mr-2">
                                                                <input
                                                                    type="number"
                                                                    name="telephone-inquiry-day3"
                                                                    id="med-adjustment-more-amount-day3"
                                                                    min="0"
                                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                                <span
                                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">drop/day
                        </span>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reduce-day3"
                                                                name="telephone-inquiry-day3"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reduce-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ลดลง เป็น
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center lg:h-5 my-4 mr-2">
                                                                <input
                                                                    type="number"
                                                                    name="telephone-inquiry-day3"
                                                                    id="med-adjustment-reduce-amount-day3"
                                                                    min="0"
                                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                                <span
                                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">drop/day
                        </span>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-symptoms-uncontrollable-day3"
                                                                name="telephone-inquiry-day3"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-symptoms-uncontrollable-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                อาการยังควบคุมไม่ได้
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <legend
                                                            className="flex-wrap sm:flex-auto sm:grow-0 text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:ml-2">
                                                            ได้แก่
                                                        </legend>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-pain-day3"
                                                                name="med-adjustment-reason-day3"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-pain-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                Pain
                                                            </label>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-insomnia-day3"
                                                                name="med-adjustment-reason-day3"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-insomnia-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                Insomnia
                                                            </label>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-appetite-day3"
                                                                name="med-adjustment-reason-day3"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-appetite-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                Appetite
                                                            </label>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-other-day3"
                                                                name="med-adjustment-reason-day3"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-other-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                อื่นๆ
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="med-adjustment-reason-day3"
                                                                    id="med-adjustment-reason-other-detail-day3"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:ml-2">
                                                            <input
                                                                id="has-side-effect-day3"
                                                                name="med-adjustment-reason-day3"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-side-effect-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มีผลข้างเคียง ระบุ
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="med-adjustment-reason-day3"
                                                                    id="has-side-effect-detail-day3"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-2 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-8">
                                            <div className="pt-1">
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-same-day5"
                                                                name="telephone-inquiry-day5"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-same-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                เท่าเดิม
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center lg:h-5 my-4 mr-2">
                                                                <input
                                                                    type="number"
                                                                    name="telephone-inquiry-day5"
                                                                    id="med-adjustment-same-amount-day5"
                                                                    min="0"
                                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                                <span
                                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">drop/day
                        </span>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-more-day5"
                                                                name="telephone-inquiry-day5"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-more-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                เพิ่มขึ้น เป็น
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center lg:h-5 my-4 mr-2">
                                                                <input
                                                                    type="number"
                                                                    name="telephone-inquiry-day5"
                                                                    id="med-adjustment-more-amount-day5"
                                                                    min="0"
                                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                                <span
                                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">drop/day
                        </span>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reduce-day5"
                                                                name="telephone-inquiry-day5"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reduce-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ลดลง เป็น
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center lg:h-5 my-4 mr-2">
                                                                <input
                                                                    type="number"
                                                                    name="telephone-inquiry-day5"
                                                                    id="med-adjustment-reduce-amount-day5"
                                                                    min="0"
                                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                                <span
                                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">drop/day
                        </span>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-symptoms-uncontrollable-day5"
                                                                name="telephone-inquiry-day5"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-symptoms-uncontrollable-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                อาการยังควบคุมไม่ได้
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <legend
                                                            className="flex-wrap sm:flex-auto sm:grow-0 text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:ml-2">
                                                            ได้แก่
                                                        </legend>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-pain-day5"
                                                                name="med-adjustment-reason-day5"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-pain-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                Pain
                                                            </label>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-insomnia-day5"
                                                                name="med-adjustment-reason-day5"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-insomnia-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                Insomnia
                                                            </label>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-appetite-day5"
                                                                name="med-adjustment-reason-day5"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-appetite-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                Appetite
                                                            </label>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-other-day5"
                                                                name="med-adjustment-reason-day5"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-other-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                อื่นๆ
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="med-adjustment-reason-day5"
                                                                    id="med-adjustment-reason-other-detail-day5"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:ml-2">
                                                            <input
                                                                id="has-side-effect-day5"
                                                                name="med-adjustment-reason-day5"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-side-effect-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มีผลข้างเคียง ระบุ
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="med-adjustment-reason-day5"
                                                                    id="has-side-effect-detail-day5"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-2 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-8">
                                            <div className="pt-1">
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-same-day7"
                                                                name="telephone-inquiry-day7"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-same-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                เท่าเดิม
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center lg:h-5 my-4 mr-2">
                                                                <input
                                                                    type="number"
                                                                    name="telephone-inquiry-day3"
                                                                    id="med-adjustment-same-amount-day3"
                                                                    min="0"
                                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                                <span
                                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">drop/day
                        </span>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-more-day7"
                                                                name="telephone-inquiry-day7"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-more-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                เพิ่มขึ้น เป็น
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center lg:h-5 my-4 mr-2">
                                                                <input
                                                                    type="number"
                                                                    name="telephone-inquiry-day7"
                                                                    id="med-adjustment-more-amount-day7"
                                                                    min="0"
                                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                                <span
                                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">drop/day
                        </span>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reduce-day7"
                                                                name="telephone-inquiry-day7"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reduce-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ลดลง เป็น
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center lg:h-5 my-4 mr-2">
                                                                <input
                                                                    type="number"
                                                                    name="telephone-inquiry-day7"
                                                                    id="med-adjustment-reduce-amount-day7"
                                                                    min="0"
                                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                                <span
                                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">drop/day
                        </span>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-symptoms-uncontrollable-day7"
                                                                name="telephone-inquiry-day7"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-symptoms-uncontrollable-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                อาการยังควบคุมไม่ได้
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <legend
                                                            className="flex-wrap sm:flex-auto sm:grow-0 text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:ml-2">
                                                            ได้แก่
                                                        </legend>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-pain-day7"
                                                                name="med-adjustment-reason-day7"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-pain-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                Pain
                                                            </label>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-insomnia-day7"
                                                                name="med-adjustment-reason-day7"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-insomnia-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                Insomnia
                                                            </label>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-appetite-day7"
                                                                name="med-adjustment-reason-day7"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-appetite-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                Appetite
                                                            </label>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-other-day7"
                                                                name="med-adjustment-reason-day7"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-other-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                อื่นๆ
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="med-adjustment-reason-day7"
                                                                    id="med-adjustment-reason-other-detail-day7"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:ml-2">
                                                            <input
                                                                id="has-side-effect-day7"
                                                                name="med-adjustment-reason-day7"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-side-effect-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มีผลข้างเคียง ระบุ
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="med-adjustment-reason-day7"
                                                                    id="has-side-effect-detail-day7"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-2 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-8">
                                            <div className="pt-1">
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-same-day9"
                                                                name="telephone-inquiry-day9"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-same-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                เท่าเดิม
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center lg:h-5 my-4 mr-2">
                                                                <input
                                                                    type="number"
                                                                    name="telephone-inquiry-day9"
                                                                    id="med-adjustment-same-amount-day9"
                                                                    min="0"
                                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                                <span
                                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">drop/day
                        </span>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-more-day3"
                                                                name="telephone-inquiry-day3"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-more-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                เพิ่มขึ้น เป็น
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center lg:h-5 my-4 mr-2">
                                                                <input
                                                                    type="number"
                                                                    name="telephone-inquiry-day9"
                                                                    id="med-adjustment-more-amount-day9"
                                                                    min="0"
                                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                                <span
                                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">drop/day
                        </span>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reduce-day9"
                                                                name="telephone-inquiry-day9"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reduce-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ลดลง เป็น
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center lg:h-5 my-4 mr-2">
                                                                <input
                                                                    type="number"
                                                                    name="telephone-inquiry-day9"
                                                                    id="med-adjustment-reduce-amount-day9"
                                                                    min="0"
                                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                                <span
                                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">drop/day
                        </span>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-symptoms-uncontrollable-day9"
                                                                name="telephone-inquiry-day9"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-symptoms-uncontrollable-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                อาการยังควบคุมไม่ได้
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <legend
                                                            className="flex-wrap sm:flex-auto sm:grow-0 text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:ml-2">
                                                            ได้แก่
                                                        </legend>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-pain-day9"
                                                                name="med-adjustment-reason-day9"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-pain-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                Pain
                                                            </label>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-insomnia-day9"
                                                                name="med-adjustment-reason-day9"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-insomnia-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                Insomnia
                                                            </label>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-appetite-day9"
                                                                name="med-adjustment-reason-day9"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-appetite-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                Appetite
                                                            </label>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-other-day9"
                                                                name="med-adjustment-reason-day9"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-other-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                อื่นๆ
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="med-adjustment-reason-day9"
                                                                    id="med-adjustment-reason-other-detail-day9"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:ml-2">
                                                            <input
                                                                id="has-side-effect-day9"
                                                                name="med-adjustment-reason-day9"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-side-effect-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มีผลข้างเคียง ระบุ
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="med-adjustment-reason-day9"
                                                                    id="has-side-effect-detail-day9"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-2 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-8">
                                            <div className="pt-1">
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-same-day11"
                                                                name="telephone-inquiry-day11"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-same-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                เท่าเดิม
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center lg:h-5 my-4 mr-2">
                                                                <input
                                                                    type="number"
                                                                    name="telephone-inquiry-day11"
                                                                    id="med-adjustment-same-amount-day11"
                                                                    min="0"
                                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                                <span
                                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">drop/day
                        </span>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-more-day11"
                                                                name="telephone-inquiry-day11"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-more-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                เพิ่มขึ้น เป็น
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center lg:h-5 my-4 mr-2">
                                                                <input
                                                                    type="number"
                                                                    name="telephone-inquiry-day11"
                                                                    id="med-adjustment-more-amount-day11"
                                                                    min="0"
                                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                                <span
                                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">drop/day
                        </span>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reduce-day11"
                                                                name="telephone-inquiry-day11"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reduce-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ลดลง เป็น
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center lg:h-5 my-4 mr-2">
                                                                <input
                                                                    type="number"
                                                                    name="telephone-inquiry-day11"
                                                                    id="med-adjustment-reduce-amount-day11"
                                                                    min="0"
                                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                                <span
                                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">drop/day
                        </span>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-symptoms-uncontrollable-day11"
                                                                name="telephone-inquiry-day11"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-symptoms-uncontrollable-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                อาการยังควบคุมไม่ได้
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <legend
                                                            className="flex-wrap sm:flex-auto sm:grow-0 text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:ml-2">
                                                            ได้แก่
                                                        </legend>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-pain-day11"
                                                                name="med-adjustment-reason-day11"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-pain-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                Pain
                                                            </label>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-insomnia-day11"
                                                                name="med-adjustment-reason-day11"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-insomnia-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                Insomnia
                                                            </label>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-appetite-day11"
                                                                name="med-adjustment-reason-day11"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-appetite-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                Appetite
                                                            </label>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-other-day11"
                                                                name="med-adjustment-reason-day11"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-other-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                อื่นๆ
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="med-adjustment-reason-day11"
                                                                    id="med-adjustment-reason-other-detail-day11"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:ml-2">
                                                            <input
                                                                id="has-side-effect-day11"
                                                                name="med-adjustment-reason-day11"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-side-effect-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มีผลข้างเคียง ระบุ
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="med-adjustment-reason-day11"
                                                                    id="has-side-effect-detail-day11"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>


                                    <div
                                        className="sm:col-span-2 border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-2">
                                        <div className="grid grid-cols-1 gap-2 gap-8 py-3">
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words">Concurrent
                                                    Medication</h6>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words mt-2">
                                                    ผู้ป่วยใช้ยา MSS เพิ่ม
                                                </h6>
                                            </div>
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                    ผู้ป่วยใช้ยา MO IR เพิ่ม
                                                </h6>
                                            </div>
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words pb-12">
                                                    ยาแก้ปวดอื่น ระบุ
                                                </h6>
                                            </div>
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words pt-28">
                                                    ยาอื่นๆ เช่น ยานอนหลับ
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 pt-10 pb-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-6">
                                            <div className="pt-1">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 py-2">
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day3"
                                                            id="add-mss-day3"
                                                            min="0"
                                                            className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                        <span
                                                            className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          ml/day
                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day3"
                                                            id="add-mo-ir-day3"
                                                            min="0"
                                                            className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                        <span
                                                            className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          เม็ด/day
                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="pt-2">
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="doctor-order-other-pain-med-day3"
                                                                name="telephone-inquiry-day3"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="doctor-order-other-pain-med-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ตามแพทย์สั่ง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="doctor-order-other-pain-med-detail-day3"
                                                                    id="telephone-inquiry-day3"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="buy-more-of-yourself-pain-med-day3"
                                                                name="telephone-inquiry-day3"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="buy-more-of-yourself-pain-med-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ซื้อเพิ่มเอง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center mt-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day3"
                                                                    id="buy-more-of-yourself-pain-med-detail-day3"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="doctor-order-other-med-day3"
                                                                name="telephone-inquiry-day3"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="doctor-order-other-med-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ตามแพทย์สั่ง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day3"
                                                                    id="doctor-order-other-med-detail-day3"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="buy-more-of-yourself-other-med-day3"
                                                                name="telephone-inquiry-day3"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="buy-more-of-yourself-other-med-day3"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ซื้อเพิ่มเอง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center mt-2 mb-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day3"
                                                                    id="buy-more-of-yourself-other-med-detail-day3"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 pt-10 pb-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-6">
                                            <div className="pt-1">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 py-2">
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day5"
                                                            id="add-mss-day5"
                                                            min="0"
                                                            className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                        <span
                                                            className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          ml/day
                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day5"
                                                            id="add-mo-ir-day5"
                                                            min="0"
                                                            className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                        <span
                                                            className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          เม็ด/day
                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="pt-2">
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="telephone-inquiry-day5"
                                                                name="doctor-order-other-pain-med-day5"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="doctor-order-other-pain-med-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ตามแพทย์สั่ง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day5"
                                                                    id="doctor-order-other-pain-med-detail-day5"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="buy-more-of-yourself-pain-med-day5"
                                                                name="telephone-inquiry-day5"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="buy-more-of-yourself-pain-med-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ซื้อเพิ่มเอง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center mt-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day5"
                                                                    id="buy-more-of-yourself-pain-med-detail-day5"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="doctor-order-other-med-day5"
                                                                name="telephone-inquiry-day5"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="doctor-order-other-med-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ตามแพทย์สั่ง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day5"
                                                                    id="doctor-order-other-med-detail-day5"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="buy-more-of-yourself-other-med-day5"
                                                                name="telephone-inquiry-day5"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="buy-more-of-yourself-other-med-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ซื้อเพิ่มเอง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center mt-2 mb-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day5"
                                                                    id="buy-more-of-yourself-other-med-detail-day5"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 pt-10 pb-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-6">
                                            <div className="pt-1">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 py-2">
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day7"
                                                            id="add-mss-day7"
                                                            min="0"
                                                            className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                        <span
                                                            className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          ml/day
                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day7"
                                                            id="add-mo-ir-day7"
                                                            min="0"
                                                            className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                        <span
                                                            className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          เม็ด/day
                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="pt-2">
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="doctor-order-other-pain-med-day7"
                                                                name="telephone-inquiry-day7"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="doctor-order-other-pain-med-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ตามแพทย์สั่ง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day7"
                                                                    id="doctor-order-other-pain-med-detail-day7"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="buy-more-of-yourself-pain-med-day7"
                                                                name="telephone-inquiry-day7"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="buy-more-of-yourself-pain-med-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ซื้อเพิ่มเอง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center mt-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day7"
                                                                    id="buy-more-of-yourself-other-med-detail-day7"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="doctor-order-other-med-day7"
                                                                name="telephone-inquiry-day7"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="doctor-order-other-med-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ตามแพทย์สั่ง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day7"
                                                                    id="doctor-order-other-med-detail-day7"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="buy-more-of-yourself-other-med-day7"
                                                                name="telephone-inquiry-day7"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="buy-more-of-yourself-other-med-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ซื้อเพิ่มเอง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center mt-2 mb-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day7"
                                                                    id="buy-more-of-yourself-other-med-detail-day7"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>

                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 pt-10 pb-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-6">
                                            <div className="pt-1">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 py-2">
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day9"
                                                            id="add-mss-day9"
                                                            min="0"
                                                            className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                        <span
                                                            className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          ml/day
                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day9"
                                                            id="add-mo-ir-day9"
                                                            min="0"
                                                            className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                        <span
                                                            className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          เม็ด/day
                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="pt-2">
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="doctor-order-other-pain-med-day9"
                                                                name="telephone-inquiry-day9"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="doctor-order-other-pain-med-day7"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ตามแพทย์สั่ง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day9"
                                                                    id="doctor-order-other-pain-med-detail-day9"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="buy-more-of-yourself-pain-med-day9"
                                                                name="telephone-inquiry-day9"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="buy-more-of-yourself-pain-med-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ซื้อเพิ่มเอง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center mt-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day9"
                                                                    id="buy-more-of-yourself-other-med-detail-day9"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="doctor-order-other-med-day9"
                                                                name="telephone-inquiry-day9"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="doctor-order-other-med-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ตามแพทย์สั่ง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day9"
                                                                    id="doctor-order-other-med-detail-day9"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="buy-more-of-yourself-other-med-day9"
                                                                name="telephone-inquiry-day9"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="buy-more-of-yourself-other-med-day9"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ซื้อเพิ่มเอง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center mt-2 mb-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day9"
                                                                    id="buy-more-of-yourself-other-med-detail-day9"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 pt-10 pb-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-6">
                                            <div className="pt-1">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 py-2">
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day11"
                                                            id="add-mss-day11"
                                                            min="0"
                                                            className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                        <span
                                                            className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          ml/day
                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day11"
                                                            id="add-mo-ir-day11"
                                                            min="0"
                                                            className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                        <span
                                                            className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          เม็ด/day
                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="pt-2">
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="doctor-order-other-pain-med-day11"
                                                                name="telephone-inquiry-day11"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="doctor-order-other-pain-med-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ตามแพทย์สั่ง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day11"
                                                                    id="doctor-order-other-pain-med-detail-day11"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="buy-more-of-yourself-pain-med-day11"
                                                                name="telephone-inquiry-day11"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="buy-more-of-yourself-pain-med-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ซื้อเพิ่มเอง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center mt-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day11"
                                                                    id="buy-more-of-yourself-other-med-detail-day11"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="doctor-order-other-med-day11"
                                                                name="telephone-inquiry-day11"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="doctor-order-other-med-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ตามแพทย์สั่ง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day11"
                                                                    id="doctor-order-other-med-detail-day11"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="buy-more-of-yourself-other-med-day11"
                                                                name="telephone-inquiry-day11"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="buy-more-of-yourself-other-med-day11"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ซื้อเพิ่มเอง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center mt-2 mb-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day11"
                                                                    id="buy-more-of-yourself-other-med-detail-day11"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 py-4 border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                        Compliance
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                        <fieldset>
                                            <div className="flex flex-col sm:flex-row">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="good-of-compliance-day3"
                                                        name="telephone-inquiry-day3"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="good-of-compliance-day3"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Good
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                    <input
                                                        id="poor-of-compliance-day3"
                                                        name="telephone-inquiry-day3"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="poor-of-compliance-day3"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        poor
                                                    </label>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                        <fieldset>
                                            <div className="flex flex-col sm:flex-row">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="good-of-compliance-day5"
                                                        name="telephone-inquiry-day5"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="good-of-compliance-day5"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Good
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                    <input
                                                        id="poor-of-compliance-day5"
                                                        name="telephone-inquiry-day5"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="poor-of-compliance-day5"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        poor
                                                    </label>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                        <fieldset>
                                            <div className="flex flex-col sm:flex-row">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="good-of-compliance-day7"
                                                        name="telephone-inquiry-day7"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="good-of-compliance-day7"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Good
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                    <input
                                                        id="poor-of-compliance-day7"
                                                        name="telephone-inquiry-day7"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="poor-of-compliance-day7"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        poor
                                                    </label>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                        <fieldset>
                                            <div className="flex flex-col sm:flex-row">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="good-of-compliance-day9"
                                                        name="telephone-inquiry-day9"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="good-of-compliance-day9"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Good
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                    <input
                                                        id="poor-of-compliance-day9"
                                                        name="telephone-inquiry-day9"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="poor-of-compliance-day9"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        poor
                                                    </label>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                        <fieldset>
                                            <div className="flex flex-col sm:flex-row">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="good-of-compliance-day11"
                                                        name="telephone-inquiry-day11"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="good-of-compliance-day11"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Good
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                    <input
                                                        id="poor-of-compliance-day11"
                                                        name="telephone-inquiry-day11"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="poor-of-compliance-day11"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        poor
                                                    </label>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>

                                    <div
                                        className="sm:col-span-2 pt-1.5 border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                        Concurrent treatment
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-2 sm:row-start-auto">
                                        <fieldset>
                                            <div className="flex flex-col gap-2">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="radiotherapy-day3"
                                                        name="telephone-inquiry-day3"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="radiotherapy-day3"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Radiotherapy
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="chemotherapy-day3"
                                                        name="telephone-inquiry-day3"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="chemotherapy-day3"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Chemotherapy
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="treatment-other-day3"
                                                        name="telephone-inquiry-day3"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="treatment-other-day3"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Other
                                                    </label>
                                                    <div className="flex flex-auto items-center my-2 mr-2">
                                                        <textarea
                                                            name="telephone-inquiry-day3"
                                                            id="treatment-other-detail-day3"
                                                            className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-2 sm:row-start-auto">
                                        <fieldset>
                                            <div className="flex flex-col gap-2">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="radiotherapy-day5"
                                                        name="telephone-inquiry-day5"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="radiotherapy-day5"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Radiotherapy
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="chemotherapy-day5"
                                                        name="telephone-inquiry-day5"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="chemotherapy-day5"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Chemotherapy
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="treatment-other-day5"
                                                        name="telephone-inquiry-day5"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="treatment-other-day5"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Other
                                                    </label>
                                                    <div className="flex flex-auto items-center my-2 mr-2">
                                                        <textarea
                                                            name="telephone-inquiry-day5"
                                                            id="treatment-other-detail-day5"
                                                            className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-2 sm:row-start-auto">
                                        <fieldset>
                                            <div className="flex flex-col gap-2">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="radiotherapy-day7"
                                                        name="telephone-inquiry-day7"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="radiotherapy-day7"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Radiotherapy
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="chemotherapy-day7"
                                                        name="telephone-inquiry-day7"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="chemotherapy-day7"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Chemotherapy
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="treatment-other-day7"
                                                        name="telephone-inquiry-day7"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="treatment-other-day7"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Other
                                                    </label>
                                                    <div className="flex flex-auto items-center my-2 mr-2">
                                                        <textarea
                                                            name="telephone-inquiry-day7"
                                                            id="treatment-other-detail-day7"
                                                            className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-2 sm:row-start-auto">
                                        <fieldset>
                                            <div className="flex flex-col gap-2">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="radiotherapy-day9"
                                                        name="telephone-inquiry-day9"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="radiotherapy-day9"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Radiotherapy
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="chemotherapy-day9"
                                                        name="telephone-inquiry-day9"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="chemotherapy-day9"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Chemotherapy
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="treatment-other-day9"
                                                        name="telephone-inquiry-day9"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="treatment-other-day9"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Other
                                                    </label>
                                                    <div className="flex flex-auto items-center my-2 mr-2">
                                                        <textarea
                                                            name="telephone-inquiry-day9"
                                                            id="treatment-other-detail-day9"
                                                            className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-2 sm:row-start-auto">
                                        <fieldset>
                                            <div className="flex flex-col gap-2">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="radiotherapy-day11"
                                                        name="telephone-inquiry-day11"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="radiotherapy-day11"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Radiotherapy
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="chemotherapy-day11"
                                                        name="telephone-inquiry-day11"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="chemotherapy-day11"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Chemotherapy
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="treatment-other-day11"
                                                        name="telephone-inquiry-day11"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="treatment-other-day11"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Other
                                                    </label>
                                                    <div className="flex flex-auto items-center my-2 mr-2">
                                                        <textarea
                                                            name="telephone-inquiry-day11"
                                                            id="treatment-other-detail-day11"
                                                            className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>

                                    <div
                                        className="sm:col-span-2 py-4 border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                        ผู้ให้ประวัติ
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                        <fieldset>
                                            <div className="flex flex-col sm:flex-row">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="patient-of-profiler-day3"
                                                        name="telephone-inquiry-day3"
                                                        type="radio"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="patient-of-profiler-day3"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        ผู้ป่วย
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                    <input
                                                        id="relative-of-profiler-day3"
                                                        name="telephone-inquiry-day3"
                                                        type="radio"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="relative-of-profiler-day3"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        ญาติ
                                                    </label>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                        <fieldset>
                                            <div className="flex flex-col sm:flex-row">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="patient-of-profiler-day5"
                                                        name="telephone-inquiry-day5"
                                                        type="radio"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="patient-of-profiler-day5"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        ผู้ป่วย
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                    <input
                                                        id="relative-of-profiler-day5"
                                                        name="telephone-inquiry-day5"
                                                        type="radio"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="relative-of-profiler-day5"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        ญาติ
                                                    </label>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                        <fieldset>
                                            <div className="flex flex-col sm:flex-row">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="patient-of-profiler-day7"
                                                        name="telephone-inquiry-day7"
                                                        type="radio"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="patient-of-profiler-day7"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        ผู้ป่วย
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                    <input
                                                        id="relative-of-profiler-day7"
                                                        name="telephone-inquiry-day7"
                                                        type="radio"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="relative-of-profiler-day7"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        ญาติ
                                                    </label>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                        <fieldset>
                                            <div className="flex flex-col sm:flex-row">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="patient-of-profiler-day9"
                                                        name="telephone-inquiry-day9"
                                                        type="radio"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="patient-of-profiler-day9"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        ผู้ป่วย
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                    <input
                                                        id="relative-of-profiler-day9"
                                                        name="telephone-inquiry-day9"
                                                        type="radio"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="relative-of-profiler-day9"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        ญาติ
                                                    </label>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                        <fieldset>
                                            <div className="flex flex-col sm:flex-row">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="patient-of-profiler-day11"
                                                        name="telephone-inquiry-day11"
                                                        type="radio"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="patient-of-profiler-day11"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        ผู้ป่วย
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                    <input
                                                        id="relative-of-profiler-day11"
                                                        name="telephone-inquiry-day11"
                                                        type="radio"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="relative-of-profiler-day11"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        ญาติ
                                                    </label>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>


                                    <div
                                        className="sm:col-span-2 py-7 border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                        ผู้บันทึก
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                        <div className="mx-2">
                                            <button
                                                type="submit"
                                                className="w-full px-2 py-3 tracking-wide text-sm text-white transition-colors duration-300 transform bg-green-500 rounded-md hover:bg-green-600 focus:outline-none focus:bg-green-500 focus:ring focus:ring-green-500 focus:ring-opacity-50">
                                                บันทึกข้อมูล Day 3
                                            </button>
                                        </div>

                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                        <div className="mx-2">
                                            <button
                                                type="submit"
                                                className="w-full px-2 py-3 tracking-wide text-sm text-white transition-colors duration-300 transform bg-green-500 rounded-md hover:bg-green-600 focus:outline-none focus:bg-green-500 focus:ring focus:ring-green-500 focus:ring-opacity-50">
                                                บันทึกข้อมูล Day 5
                                            </button>
                                        </div>

                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                        <div className="mx-2">
                                            <button
                                                type="submit"
                                                className="w-full px-2 py-3 tracking-wide text-sm text-white transition-colors duration-300 transform bg-green-500 rounded-md hover:bg-green-600 focus:outline-none focus:bg-green-500 focus:ring focus:ring-green-500 focus:ring-opacity-50">
                                                บันทึกข้อมูล Day 7
                                            </button>
                                        </div>

                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                        <div className="mx-2">
                                            <button
                                                type="submit"
                                                className="w-full px-2 py-3 tracking-wide text-sm text-white transition-colors duration-300 transform bg-green-500 rounded-md hover:bg-green-600 focus:outline-none focus:bg-green-500 focus:ring focus:ring-green-500 focus:ring-opacity-50">
                                                บันทึกข้อมูล Day 9
                                            </button>
                                        </div>

                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                        <div className="mx-2">
                                            <button
                                                type="submit"
                                                className="w-full px-2 py-3 tracking-wide text-sm text-white transition-colors duration-300 transform bg-green-500 rounded-md hover:bg-green-600 focus:outline-none focus:bg-green-500 focus:ring focus:ring-green-500 focus:ring-opacity-50">
                                                บันทึกข้อมูล Day 11
                                            </button>
                                        </div>

                                    </div>

                                </div>
                            </div>

                            <div
                                className="bg-white rounded-md rounded-b-none rounded-t-none shadow-md dark:bg-gray-800 px-4 py-5 sm:px-6 sm:py-4">
                                {/*<div className="block text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0">*/}
                                {/*    <h6>4.1 แบบบันทึกการสอบถามข้อมูลทางโทรศัพท์</h6>*/}
                                {/*</div>*/}
                                <div
                                    className="grid grid-cols-1 md:grid-cols-12 border border-gray-300 border-separate bg-gray-50 dark:bg-gray-700 dark:border-gray-600">
                                    <div
                                        className="sm:col-span-2 py-4 row-start-1 row-end-auto text-center text-sm text-gray-600 dark:text-gray-200">
                                        Day
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-slate-300 dark:bg-gray-700 dark:border-gray-600 py-4 row-start-2 sm:row-start-auto text-center text-sm text-gray-600 dark:text-gray-200">
                                        13
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-slate-300 dark:bg-gray-700 dark:border-gray-600 py-4 sm:row-start-auto text-center text-sm text-gray-600 dark:text-gray-200">
                                        15
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-slate-300 dark:bg-gray-700 dark:border-gray-600 py-4 sm:row-start-auto text-center text-sm text-gray-600 dark:text-gray-200">
                                        17
                                    </div>

                                    <div
                                        className="sm:col-span-2 border-l border-slate-300 dark:bg-gray-700 dark:border-gray-600 py-4 sm:row-start-auto text-center text-sm text-gray-600 dark:text-gray-200">
                                        19
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-slate-300 dark:bg-gray-700 dark:border-gray-600 py-4 sm:row-start-auto text-center text-sm text-gray-600 dark:text-gray-200">

                                    </div>

                                    <div
                                        className="sm:col-span-2 border-t border-b border-slate-300 dark:bg-gray-700 dark:border-gray-600 py-6 text-center text-sm text-gray-600 dark:text-gray-200">
                                        วันที่
                                    </div>
                                    <div
                                        className="sm:col-span-2 border sm:border-r-0 border-slate-300 dark:bg-gray-700 dark:border-gray-600 py-4 row-start-3 sm:row-start-auto">
                                        <div className="flex flex-wrap sm:flex-auto items-center sm:mx-0 sm:my-2 mx-2">
                                            <div
                                                className="relative sm:ml-2 flex flex-auto items-center lg:h-5 w-fit mt-2 sm:mt-0 mx-2">
                                                <DatePicker selected={dob} onChange={(date) => setDob(date)}
                                                            locale="th"
                                                            peekNextMonth
                                                            showMonthDropdown
                                                            showYearDropdown
                                                            disabledKeyboardNavigation
                                                            dropdownMode="select"
                                                            dateFormat="dd/MM/yyyy"
                                                            filterDate={isWeekday}
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                            placeholderText="วัน/เดือน/ปี ค.ศ."
                                                            name="telephone-inquiry-day13"
                                                            id="telephone-inquiry-day13"
                                                />
                                                <div
                                                    className="flex absolute inset-y-0 right-0 items-center pr-3 mt-1 pointer-events-none">
                                                    <CalendarDaysIcon
                                                        className="h-6 w-6 text-gray-500 dark:text-gray-400 sm:h-5 sm:w-5"
                                                        aria-hidden="true"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border sm:border-r-0 border-slate-300 dark:bg-gray-700 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="flex flex-wrap sm:flex-auto items-center sm:mx-0 sm:my-2 mx-2">
                                            <div
                                                className="relative sm:ml-2 flex flex-auto items-center lg:h-5 w-fit mt-2 sm:mt-0 mx-2">
                                                <DatePicker selected={dob} onChange={(date) => setDob(date)}
                                                            locale="th"
                                                            peekNextMonth
                                                            showMonthDropdown
                                                            showYearDropdown
                                                            disabledKeyboardNavigation
                                                            dropdownMode="select"
                                                            dateFormat="dd/MM/yyyy"
                                                            filterDate={isWeekday}
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                            placeholderText="วัน/เดือน/ปี ค.ศ."
                                                            name="telephone-inquiry-day15"
                                                            id="telephone-inquiry-day15"
                                                />
                                                <div
                                                    className="flex absolute inset-y-0 right-0 items-center pr-3 mt-1 pointer-events-none">
                                                    <CalendarDaysIcon
                                                        className="h-6 w-6 text-gray-500 dark:text-gray-400 sm:h-5 sm:w-5"
                                                        aria-hidden="true"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border sm:border-r-0 border-slate-300 dark:bg-gray-700 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="flex flex-wrap sm:flex-auto items-center sm:mx-0 sm:my-2 mx-2">
                                            <div
                                                className="relative sm:ml-2 flex flex-auto items-center lg:h-5 w-fit mt-2 sm:mt-0 mx-2">
                                                <DatePicker selected={dob} onChange={(date) => setDob(date)}
                                                            locale="th"
                                                            peekNextMonth
                                                            showMonthDropdown
                                                            showYearDropdown
                                                            disabledKeyboardNavigation
                                                            dropdownMode="select"
                                                            dateFormat="dd/MM/yyyy"
                                                            filterDate={isWeekday}
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                            placeholderText="วัน/เดือน/ปี ค.ศ."
                                                            name="telephone-inquiry-day17"
                                                            id="telephone-inquiry-day17"
                                                />
                                                <div
                                                    className="flex absolute inset-y-0 right-0 items-center pr-3 mt-1 pointer-events-none">
                                                    <CalendarDaysIcon
                                                        className="h-6 w-6 text-gray-500 dark:text-gray-400 sm:h-5 sm:w-5"
                                                        aria-hidden="true"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border sm:border-r-0 border-slate-300 dark:bg-gray-700 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="flex flex-wrap sm:flex-auto items-center sm:mx-0 sm:my-2 mx-2">
                                            <div
                                                className="relative sm:ml-2 flex flex-auto items-center lg:h-5 w-fit mt-2 sm:mt-0 mx-2">
                                                <DatePicker selected={dob} onChange={(date) => setDob(date)}
                                                            locale="th"
                                                            peekNextMonth
                                                            showMonthDropdown
                                                            showYearDropdown
                                                            disabledKeyboardNavigation
                                                            dropdownMode="select"
                                                            dateFormat="dd/MM/yyyy"
                                                            filterDate={isWeekday}
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                            placeholderText="วัน/เดือน/ปี ค.ศ."
                                                            name="telephone-inquiry-day19"
                                                            id="telephone-inquiry-day19"
                                                />
                                                <div
                                                    className="flex absolute inset-y-0 right-0 items-center pr-3 mt-1 pointer-events-none">
                                                    <CalendarDaysIcon
                                                        className="h-6 w-6 text-gray-500 dark:text-gray-400 sm:h-5 sm:w-5"
                                                        aria-hidden="true"/>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <div
                                        className="sm:col-span-2 border sm:border-r-0 border-slate-300 dark:bg-gray-700 dark:border-gray-600 py-4 sm:row-start-auto">
                                    </div>
                                    <div
                                        className="sm:col-span-2 py-5 border-b border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 text-center text-sm text-gray-600 dark:text-gray-200">
                                        Pain score
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="telephone-inquiry-day13"
                                                        id="telephone-inquiry-pain-score-day13"
                                                        min="0"
                                                        max="10"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="telephone-inquiry-day15"
                                                        id="telephone-inquiry-pain-score-day15"
                                                        min="0"
                                                        max="10"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="telephone-inquiry-day17"
                                                        id="telephone-inquiry-pain-score-day17"
                                                        min="0"
                                                        max="10"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="telephone-inquiry-day19"
                                                        id="telephone-inquiry-pain-score-day19"
                                                        min="0"
                                                        max="10"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                    </div>
                                    <div
                                        className="sm:col-span-2 py-5 border-b border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 text-center text-sm text-gray-600 dark:text-gray-200">
                                        Sleep (ESAS)
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="telephone-inquiry-day13"
                                                        id="telephone-inquiry-sleep-day13"
                                                        min="0"
                                                        max="10"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="telephone-inquiry-day15"
                                                        id="telephone-inquiry-sleep-day15"
                                                        min="0"
                                                        max="10"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="telephone-inquiry-day17"
                                                        id="telephone-inquiry-sleep-day17"
                                                        min="0"
                                                        max="10"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="telephone-inquiry-day19"
                                                        id="telephone-inquiry-sleep-day19"
                                                        min="0"
                                                        max="10"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                    </div>
                                    <div
                                        className="sm:col-span-2 py-5 border-b border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                        Loss of appetite (ESAS)
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="telephone-inquiry-day13"
                                                        id="telephone-inquiry-loss-day13"
                                                        min="0"
                                                        max="10"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="telephone-inquiry-day15"
                                                        id="telephone-inquiry-loss-day15"
                                                        min="0"
                                                        max="10"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="telephone-inquiry-day17"
                                                        id="telephone-inquiry-loss-day17"
                                                        min="0"
                                                        max="10"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-7 my-1">
                                            <div
                                                className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                <div
                                                    className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                    <input
                                                        type="number"
                                                        name="telephone-inquiry-day19"
                                                        id="telephone-inquiry-loss-day19"
                                                        min="0"
                                                        max="10"
                                                        className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                    />
                                                    <span
                                                        className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          /10
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-b sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4 sm:row-start-auto">
                                    </div>
                                    <div
                                        className="sm:col-span-2 bg-white border-slate-300 dark:bg-gray-800 dark:border-gray-600">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-8 my-1">
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words">AE</h6>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words mt-2">ตาพร่า
                                                    เป็นลม
                                                    (Fainting/syncope)</h6>
                                            </div>
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words pt-4">
                                                    วิงเวียน มึนงง (Dizziness)
                                                </h6>
                                            </div>
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words pt-9">
                                                    สับสน (confusion/delirium)
                                                </h6>
                                            </div>
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words pt-9">
                                                    ปากแห้ง คอแห้ง
                                                </h6>
                                            </div>
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words py-9">
                                                    คลื่นไส้ อาเจียน
                                                </h6>
                                            </div>
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                    ชักเกร็ง
                                                </h6>
                                            </div>
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words pt-9">
                                                    หูแว่ว ประสาทหลอน
                                                </h6>
                                            </div>
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words pt-9">
                                                    Psychosis (hallucinations/delusions)
                                                </h6>
                                            </div>
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words pt-4">
                                                    ใจสั่น
                                                </h6>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-col items-center">
                                                    <label htmlFor="ae-other-detail-set2"
                                                           className="sm:ml-6 lg:ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        อื่นๆ ระบุ
                                                    </label>
                                                    <div
                                                        className="sm:px-2 sm:py-2 flex flex-auto items-center w-full sm:basis-2/3">
                                                        <textarea
                                                            name="telephone-inquiry"
                                                            id="ae-other-detail-set2"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 pt-8 pb-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-8">
                                            <div className="pt-1">
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-fainting-day13"
                                                                name="telephone-inquiry-day13"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-fainting-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-fainting-day13"
                                                                name="telephone-inquiry-day13"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-fainting-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-fainting-day13"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day13"
                                                            id="grade-of-ae-fainting-day13"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-dizziness-day13"
                                                                name="telephone-inquiry-day13"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-dizziness-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-dizziness-day13"
                                                                name="telephone-inquiry-day13"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-dizziness-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-dizziness-day13"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day13"
                                                            id="grade-of-ae-dizziness-day13"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-confusion-day13"
                                                                name="telephone-inquiry-day13"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-confusion-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-confusion-day13"
                                                                name="telephone-inquiry-day13"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-confusion-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-confusion-day13"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day13"
                                                            id="grade-of-ae-confusion-day13"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-feel-dry-day13"
                                                                name="telephone-inquiry-day13"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-feel-dry-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-feel-dry-day13"
                                                                name="telephone-inquiry-day13"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-feel-dry-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-feel-dry-day13"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day13"
                                                            id="grade-of-ae-feel-dry-day13"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-nausea-vomiting-day13"
                                                                name="telephone-inquiry-day13"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-nausea-vomiting-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-nausea-vomiting-day13"
                                                                name="telephone-inquiry-day13"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-nausea-vomiting-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-nausea-vomiting-day13"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day13"
                                                            id="grade-of-ae-nausea-vomiting-day13"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-convulsions-day13"
                                                                name="telephone-inquiry-day13"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-convulsions-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-convulsions-day13"
                                                                name="telephone-inquiry-day13"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-convulsions-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-convulsions-day13"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day13"
                                                            id="grade-of-ae-convulsions-day13"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-psychedelic-day13"
                                                                name="telephone-inquiry-day13"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-psychedelic-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-psychedelic-day13"
                                                                name="telephone-inquiry-day13"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-psychedelic-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-psychedelic-day13"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day13"
                                                            id="grade-of-ae-psychedelic-day13"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-psychosis-day13"
                                                                name="telephone-inquiry-day13"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-psychosis-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-psychosis-day13"
                                                                name="telephone-inquiry-day13"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-psychosis-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-psychosis-day13"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day13"
                                                            id="grade-of-ae-psychosis-day13"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                    <label htmlFor="grade-of-ae-palpitation-day13"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day13"
                                                            id="grade-of-ae-palpitation-day13"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                    <label htmlFor="grade-of-ae-other-day13"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day13"
                                                            id="grade-of-ae-other-day13"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 pt-8 pb-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-8">
                                            <div className="pt-1">
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-fainting-day15"
                                                                name="telephone-inquiry-day15"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-fainting-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-fainting-day15"
                                                                name="telephone-inquiry-day15"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-fainting-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-fainting-day15"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day15"
                                                            id="grade-of-ae-fainting-day15"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-dizziness-day15"
                                                                name="telephone-inquiry-day15"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-dizziness-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-dizziness-day15"
                                                                name="telephone-inquiry-day15"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-dizziness-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-dizziness-day15"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day15"
                                                            id="grade-of-ae-dizziness-day15"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-confusion-day15"
                                                                name="telephone-inquiry-day15"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-confusion-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-confusion-day15"
                                                                name="telephone-inquiry-day15"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-confusion-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-confusion-day15"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day15"
                                                            id="grade-of-ae-confusion-day15"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-feel-dry-day15"
                                                                name="telephone-inquiry-day15"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-feel-dry-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-feel-dry-day15"
                                                                name="telephone-inquiry-day15"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-feel-dry-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-feel-dry-day15"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day15"
                                                            id="grade-of-ae-feel-dry-day15"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-nausea-vomiting-day15"
                                                                name="telephone-inquiry-day15"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-nausea-vomiting-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-nausea-vomiting-day15"
                                                                name="telephone-inquiry-day15"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-nausea-vomiting-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-nausea-vomiting-day15"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day15"
                                                            id="grade-of-ae-nausea-vomiting-day15"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-convulsions-day15"
                                                                name="telephone-inquiry-day15"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-convulsions-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-convulsions-day15"
                                                                name="telephone-inquiry-day15"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-convulsions-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-convulsions-day15"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day15"
                                                            id="grade-of-ae-convulsions-day15"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-psychedelic-day15"
                                                                name="telephone-inquiry-day15"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-psychedelic-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-psychedelic-day15"
                                                                name="telephone-inquiry-day15"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-psychedelic-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-psychedelic-day15"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day15"
                                                            id="grade-of-ae-psychedelic-day15"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-psychosis-day15"
                                                                name="telephone-inquiry-day15"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-psychosis-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-psychosis-day15"
                                                                name="telephone-inquiry-day15"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-psychosis-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-psychosis-day15"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day15"
                                                            id="grade-of-ae-psychosis-day15"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                    <label htmlFor="grade-of-ae-palpitation-day15"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day15"
                                                            id="grade-of-ae-palpitation-day15"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                    <label htmlFor="grade-of-ae-other-day15"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day15"
                                                            id="grade-of-ae-other-day15"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 pt-8 pb-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-8">
                                            <div className="pt-1">
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-fainting-day17"
                                                                name="telephone-inquiry-day17"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-fainting-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-fainting-day17"
                                                                name="telephone-inquiry-day17"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-fainting-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-fainting-day17"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day17"
                                                            id="grade-of-ae-fainting-day17"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-dizziness-day17"
                                                                name="telephone-inquiry-day17"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-dizziness-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-dizziness-day17"
                                                                name="telephone-inquiry-day17"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-dizziness-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-dizziness-day17"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day17"
                                                            id="grade-of-ae-dizziness-day17"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-confusion-day17"
                                                                name="telephone-inquiry-day17"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-confusion-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-confusion-day17"
                                                                name="telephone-inquiry-day17"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-confusion-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-confusion-day17"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day17"
                                                            id="grade-of-ae-confusion-day17"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-feel-dry-day17"
                                                                name="telephone-inquiry-day17"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-feel-dry-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-feel-dry-day17"
                                                                name="telephone-inquiry-day17"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-feel-dry-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-feel-dry-day17"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day17"
                                                            id="grade-of-ae-feel-dry-day17"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-nausea-vomiting-day17"
                                                                name="telephone-inquiry-day17"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-nausea-vomiting-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-nausea-vomiting-day17"
                                                                name="telephone-inquiry-day17"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-nausea-vomiting-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-nausea-vomiting-day17"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day17"
                                                            id="grade-of-ae-nausea-vomiting-day17"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-convulsions-day17"
                                                                name="telephone-inquiry-day17"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-convulsions-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-convulsions-day17"
                                                                name="telephone-inquiry-day17"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-convulsions-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-convulsions-day17"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day17"
                                                            id="grade-of-ae-convulsions-day17"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-psychedelic-day17"
                                                                name="telephone-inquiry-day17"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-psychedelic-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-psychedelic-day17"
                                                                name="telephone-inquiry-day17"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-psychedelic-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-psychedelic-day17"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day17"
                                                            id="grade-of-ae-psychedelic-day17"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-psychosis-day17"
                                                                name="telephone-inquiry-day17"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-psychosis-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-psychosis-day17"
                                                                name="telephone-inquiry-day17"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-psychosis-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-psychosis-day17"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day17"
                                                            id="grade-of-ae-psychosis-day17"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                    <label htmlFor="grade-of-ae-palpitation-day17"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day17"
                                                            id="grade-of-ae-palpitation-day17"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                    <label htmlFor="grade-of-ae-other-day17"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day17"
                                                            id="grade-of-ae-other-day17"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 pt-8 pb-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-8">
                                            <div className="pt-1">
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-fainting-day19"
                                                                name="telephone-inquiry-day19"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-fainting-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-fainting-day19"
                                                                name="telephone-inquiry-day19"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-fainting-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-fainting-day19"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day19"
                                                            id="grade-of-ae-fainting-day19"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-dizziness-day19"
                                                                name="telephone-inquiry-day19"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-dizziness-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-dizziness-day19"
                                                                name="telephone-inquiry-day19"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-dizziness-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-dizziness-day19"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day19"
                                                            id="grade-of-ae-dizziness-day19"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-confusion-day19"
                                                                name="telephone-inquiry-day19"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-confusion-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-confusion-day19"
                                                                name="telephone-inquiry-day19"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-confusion-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-confusion-day19"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day19"
                                                            id="grade-of-ae-confusion-day19"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-feel-dry-day19"
                                                                name="telephone-inquiry-day19"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-feel-dry-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-feel-dry-day19"
                                                                name="telephone-inquiry-day19"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-feel-dry-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-feel-dry-day19"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day19"
                                                            id="grade-of-ae-feel-dry-day19"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-nausea-vomiting-day19"
                                                                name="telephone-inquiry-day19"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-nausea-vomiting-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-nausea-vomiting-day19"
                                                                name="telephone-inquiry-day19"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-nausea-vomiting-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-nausea-vomiting-day19"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day19"
                                                            id="grade-of-ae-nausea-vomiting-day19"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-convulsions-day19"
                                                                name="telephone-inquiry-day19"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-convulsions-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-convulsions-day19"
                                                                name="telephone-inquiry-day19"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-convulsions-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-convulsions-day19"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day19"
                                                            id="grade-of-ae-convulsions-day19"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-psychedelic-day19"
                                                                name="telephone-inquiry-day19"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-psychedelic-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-psychedelic-day19"
                                                                name="telephone-inquiry-day19"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-psychedelic-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-psychedelic-day19"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day19"
                                                            id="grade-of-ae-psychedelic-day19"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col sm:flex-row">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="not-has-ae-psychosis-day19"
                                                                name="telephone-inquiry-day19"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="not-has-ae-psychosis-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ไม่มี
                                                            </label>

                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                            <input
                                                                id="has-ae-psychosis-day19"
                                                                name="telephone-inquiry-day19"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-ae-psychosis-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มี
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 pt-4">
                                                    <label htmlFor="grade-of-ae-psychosis-day19"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day19"
                                                            id="grade-of-ae-psychosis-day19"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                    <label htmlFor="grade-of-ae-palpitation-day19"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day19"
                                                            id="grade-of-ae-palpitation-day19"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                    <label htmlFor="grade-of-ae-other-day19"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words sm:mt-0 sm:w-fit w-full">
                                                        Grade
                                                    </label>
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day19"
                                                            id="grade-of-ae-other-day19"
                                                            min="0"
                                                            className="block w-full px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 rounded-md dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t sm:border-t-0 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 pt-8 pb-4 sm:row-start-auto">
                                    </div>

                                    <div
                                        className="sm:col-span-2 border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-8 my-3">
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                    การปรับยา</h6>
                                            </div>
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words py-60">
                                                    เหตุผลของการปรับยา
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-2 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-8">
                                            <div className="pt-1">
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-same-day13"
                                                                name="telephone-inquiry-day13"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-same-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                เท่าเดิม
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center lg:h-5 my-4 mr-2">
                                                                <input
                                                                    type="number"
                                                                    name="telephone-inquiry-day13"
                                                                    id="med-adjustment-same-amount-day13"
                                                                    min="0"
                                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                                <span
                                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">drop/day
                        </span>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-more-day13"
                                                                name="telephone-inquiry-day13"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-more-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                เพิ่มขึ้น เป็น
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center lg:h-5 my-4 mr-2">
                                                                <input
                                                                    type="number"
                                                                    name="telephone-inquiry-day13"
                                                                    id="med-adjustment-more-amount-day13"
                                                                    min="0"
                                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                                <span
                                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">drop/day
                        </span>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reduce-day13"
                                                                name="telephone-inquiry-day13"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reduce-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ลดลง เป็น
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center lg:h-5 my-4 mr-2">
                                                                <input
                                                                    type="number"
                                                                    name="telephone-inquiry-day13"
                                                                    id="med-adjustment-reduce-amount-day13"
                                                                    min="0"
                                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                                <span
                                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">drop/day
                        </span>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-symptoms-uncontrollable-day13"
                                                                name="telephone-inquiry-day13"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-symptoms-uncontrollable-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                อาการยังควบคุมไม่ได้
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <legend
                                                            className="flex-wrap sm:flex-auto sm:grow-0 text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:ml-2">
                                                            ได้แก่
                                                        </legend>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-pain-day13"
                                                                name="med-adjustment-reason-day13"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-pain-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                Pain
                                                            </label>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-insomnia-day13"
                                                                name="med-adjustment-reason-day13"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-insomnia-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                Insomnia
                                                            </label>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-appetite-day13"
                                                                name="med-adjustment-reason-day13"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-appetite-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                Appetite
                                                            </label>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-other-day13"
                                                                name="med-adjustment-reason-day13"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-other-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                อื่นๆ
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="med-adjustment-reason-day13"
                                                                    id="med-adjustment-reason-other-detail-day13"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:ml-2">
                                                            <input
                                                                id="has-side-effect-day13"
                                                                name="med-adjustment-reason-day13"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-side-effect-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มีผลข้างเคียง ระบุ
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="med-adjustment-reason-day13"
                                                                    id="has-side-effect-detail-day13"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-2 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-8">
                                            <div className="pt-1">
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-same-day15"
                                                                name="telephone-inquiry-day15"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-same-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                เท่าเดิม
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center lg:h-5 my-4 mr-2">
                                                                <input
                                                                    type="number"
                                                                    name="telephone-inquiry-day15"
                                                                    id="med-adjustment-same-amount-day15"
                                                                    min="0"
                                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                                <span
                                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">drop/day
                        </span>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-more-day15"
                                                                name="telephone-inquiry-day15"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-more-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                เพิ่มขึ้น เป็น
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center lg:h-5 my-4 mr-2">
                                                                <input
                                                                    type="number"
                                                                    name="telephone-inquiry-day15"
                                                                    id="med-adjustment-more-amount-day15"
                                                                    min="0"
                                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                                <span
                                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">drop/day
                        </span>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reduce-day15"
                                                                name="telephone-inquiry-day15"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reduce-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ลดลง เป็น
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center lg:h-5 my-4 mr-2">
                                                                <input
                                                                    type="number"
                                                                    name="telephone-inquiry-day15"
                                                                    id="med-adjustment-reduce-amount-day15"
                                                                    min="0"
                                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                                <span
                                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">drop/day
                        </span>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-symptoms-uncontrollable-day15"
                                                                name="telephone-inquiry-day15"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-symptoms-uncontrollable-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                อาการยังควบคุมไม่ได้
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <legend
                                                            className="flex-wrap sm:flex-auto sm:grow-0 text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:ml-2">
                                                            ได้แก่
                                                        </legend>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-pain-day15"
                                                                name="med-adjustment-reason-day15"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-pain-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                Pain
                                                            </label>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-insomnia-day15"
                                                                name="med-adjustment-reason-day15"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-insomnia-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                Insomnia
                                                            </label>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-appetite-day15"
                                                                name="med-adjustment-reason-day15"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-appetite-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                Appetite
                                                            </label>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-other-day15"
                                                                name="med-adjustment-reason-day15"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-other-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                อื่นๆ
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="med-adjustment-reason-day15"
                                                                    id="med-adjustment-reason-other-detail-day15"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:ml-2">
                                                            <input
                                                                id="has-side-effect-day15"
                                                                name="med-adjustment-reason-day15"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-side-effect-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มีผลข้างเคียง ระบุ
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="med-adjustment-reason-day15"
                                                                    id="has-side-effect-detail-day15"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-2 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-8">
                                            <div className="pt-1">
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-same-day17"
                                                                name="telephone-inquiry-day17"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-same-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                เท่าเดิม
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center lg:h-5 my-4 mr-2">
                                                                <input
                                                                    type="number"
                                                                    name="telephone-inquiry-day17"
                                                                    id="med-adjustment-same-amount-day17"
                                                                    min="0"
                                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                                <span
                                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">drop/day
                        </span>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-more-day17"
                                                                name="telephone-inquiry-day17"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-more-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                เพิ่มขึ้น เป็น
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center lg:h-5 my-4 mr-2">
                                                                <input
                                                                    type="number"
                                                                    name="telephone-inquiry-day17"
                                                                    id="med-adjustment-more-amount-day17"
                                                                    min="0"
                                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                                <span
                                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">drop/day
                        </span>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reduce-day17"
                                                                name="telephone-inquiry-day17"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reduce-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ลดลง เป็น
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center lg:h-5 my-4 mr-2">
                                                                <input
                                                                    type="number"
                                                                    name="telephone-inquiry-day17"
                                                                    id="med-adjustment-reduce-amount-day17"
                                                                    min="0"
                                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                                <span
                                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">drop/day
                        </span>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-symptoms-uncontrollable-day17"
                                                                name="telephone-inquiry-day17"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-symptoms-uncontrollable-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                อาการยังควบคุมไม่ได้
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <legend
                                                            className="flex-wrap sm:flex-auto sm:grow-0 text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:ml-2">
                                                            ได้แก่
                                                        </legend>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-pain-day17"
                                                                name="med-adjustment-reason-day17"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-pain-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                Pain
                                                            </label>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-insomnia-day17"
                                                                name="med-adjustment-reason-day17"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-insomnia-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                Insomnia
                                                            </label>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-appetite-day17"
                                                                name="med-adjustment-reason-day17"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-appetite-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                Appetite
                                                            </label>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-other-day17"
                                                                name="med-adjustment-reason-day17"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-other-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                อื่นๆ
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="med-adjustment-reason-day17"
                                                                    id="med-adjustment-reason-other-detail-day17"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:ml-2">
                                                            <input
                                                                id="has-side-effect-day17"
                                                                name="med-adjustment-reason-day17"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-side-effect-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มีผลข้างเคียง ระบุ
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="med-adjustment-reason-day17"
                                                                    id="has-side-effect-detail-day17"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-2 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-8">
                                            <div className="pt-1">
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-same-day19"
                                                                name="telephone-inquiry-day19"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-same-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                เท่าเดิม
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center lg:h-5 my-4 mr-2">
                                                                <input
                                                                    type="number"
                                                                    name="telephone-inquiry-day19"
                                                                    id="med-adjustment-same-amount-day19"
                                                                    min="0"
                                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                                <span
                                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">drop/day
                        </span>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-more-day19"
                                                                name="telephone-inquiry-day19"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-more-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                เพิ่มขึ้น เป็น
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center lg:h-5 my-4 mr-2">
                                                                <input
                                                                    type="number"
                                                                    name="telephone-inquiry-day19"
                                                                    id="med-adjustment-more-amount-day19"
                                                                    min="0"
                                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                                <span
                                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">drop/day
                        </span>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reduce-day19"
                                                                name="telephone-inquiry-day19"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reduce-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ลดลง เป็น
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center lg:h-5 my-4 mr-2">
                                                                <input
                                                                    type="number"
                                                                    name="telephone-inquiry-day19"
                                                                    id="med-adjustment-reduce-amount-day19"
                                                                    min="0"
                                                                    className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                                <span
                                                                    className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">drop/day
                        </span>
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-symptoms-uncontrollable-day19"
                                                                name="telephone-inquiry-day19"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-symptoms-uncontrollable-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                อาการยังควบคุมไม่ได้
                                                            </label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <legend
                                                            className="flex-wrap sm:flex-auto sm:grow-0 text-sm text-gray-600 dark:text-gray-200 mt-2 sm:mt-0 sm:ml-2">
                                                            ได้แก่
                                                        </legend>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-pain-day19"
                                                                name="med-adjustment-reason-day19"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-pain-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                Pain
                                                            </label>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-insomnia-day19"
                                                                name="med-adjustment-reason-day19"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-insomnia-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                Insomnia
                                                            </label>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-appetite-day19"
                                                                name="med-adjustment-reason-day19"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-appetite-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                Appetite
                                                            </label>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="med-adjustment-reason-other-day19"
                                                                name="med-adjustment-reason-day19"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="med-adjustment-reason-other-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                อื่นๆ
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="med-adjustment-reason-day19"
                                                                    id="med-adjustment-reason-other-detail-day19"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:ml-2">
                                                            <input
                                                                id="has-side-effect-day19"
                                                                name="med-adjustment-reason-day19"
                                                                type="radio"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="has-side-effect-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                มีผลข้างเคียง ระบุ
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="med-adjustment-reason-day19"
                                                                    id="has-side-effect-detail-day19"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-2 sm:row-start-auto">
                                    </div>

                                    <div
                                        className="sm:col-span-2 border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-2">
                                        <div className="grid grid-cols-1 gap-2 gap-8 py-3">
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words">Concurrent
                                                    Medication</h6>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words mt-2">
                                                    ผู้ป่วยใช้ยา MSS เพิ่ม
                                                </h6>
                                            </div>
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                    ผู้ป่วยใช้ยา MO IR เพิ่ม
                                                </h6>
                                            </div>
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words pb-12">
                                                    ยาแก้ปวดอื่น ระบุ
                                                </h6>
                                            </div>
                                            <div>
                                                <h6 className="text-center text-sm text-gray-600 dark:text-gray-200 break-words pt-28">
                                                    ยาอื่นๆ เช่น ยานอนหลับ
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 pt-10 pb-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-6">
                                            <div className="pt-1">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 py-2">
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day13"
                                                            id="add-mss-day13"
                                                            min="0"
                                                            className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                        <span
                                                            className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          ml/day
                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day13"
                                                            id="add-mo-ir-day13"
                                                            min="0"
                                                            className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                        <span
                                                            className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          เม็ด/day
                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="pt-2">
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="doctor-order-other-pain-med-day13"
                                                                name="telephone-inquiry-day13"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="doctor-order-other-pain-med-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ตามแพทย์สั่ง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="doctor-order-other-pain-med-detail-day13"
                                                                    id="telephone-inquiry-day13"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="buy-more-of-yourself-pain-med-day13"
                                                                name="telephone-inquiry-day13"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="buy-more-of-yourself-pain-med-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ซื้อเพิ่มเอง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center mt-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day13"
                                                                    id="buy-more-of-yourself-pain-med-detail-day13"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="doctor-order-other-med-day13"
                                                                name="telephone-inquiry-day13"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="doctor-order-other-med-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ตามแพทย์สั่ง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day13"
                                                                    id="doctor-order-other-med-detail-day13"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="buy-more-of-yourself-other-med-day13"
                                                                name="telephone-inquiry-day13"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="buy-more-of-yourself-other-med-day13"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ซื้อเพิ่มเอง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center mt-2 mb-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day13"
                                                                    id="buy-more-of-yourself-other-med-detail-day13"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 pt-10 pb-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-6">
                                            <div className="pt-1">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 py-2">
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day15"
                                                            id="add-mss-day15"
                                                            min="0"
                                                            className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                        <span
                                                            className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          ml/day
                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day15"
                                                            id="add-mo-ir-day15"
                                                            min="0"
                                                            className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                        <span
                                                            className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          เม็ด/day
                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="pt-2">
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="telephone-inquiry-day15"
                                                                name="doctor-order-other-pain-med-day15"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="doctor-order-other-pain-med-day5"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ตามแพทย์สั่ง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day15"
                                                                    id="doctor-order-other-pain-med-detail-day15"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="buy-more-of-yourself-pain-med-day15"
                                                                name="telephone-inquiry-day15"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="buy-more-of-yourself-pain-med-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ซื้อเพิ่มเอง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center mt-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day15"
                                                                    id="buy-more-of-yourself-pain-med-detail-day15"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="doctor-order-other-med-day15"
                                                                name="telephone-inquiry-day15"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="doctor-order-other-med-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ตามแพทย์สั่ง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day15"
                                                                    id="doctor-order-other-med-detail-day15"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="buy-more-of-yourself-other-med-day15"
                                                                name="telephone-inquiry-day15"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="buy-more-of-yourself-other-med-day15"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ซื้อเพิ่มเอง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center mt-2 mb-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day15"
                                                                    id="buy-more-of-yourself-other-med-detail-day15"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 pt-10 pb-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-6">
                                            <div className="pt-1">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 py-2">
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day17"
                                                            id="add-mss-day17"
                                                            min="0"
                                                            className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                        <span
                                                            className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          ml/day
                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day17"
                                                            id="add-mo-ir-day17"
                                                            min="0"
                                                            className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                        <span
                                                            className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          เม็ด/day
                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="pt-2">
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="doctor-order-other-pain-med-day17"
                                                                name="telephone-inquiry-day17"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="doctor-order-other-pain-med-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ตามแพทย์สั่ง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day17"
                                                                    id="doctor-order-other-pain-med-detail-day17"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="buy-more-of-yourself-pain-med-day17"
                                                                name="telephone-inquiry-day17"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="buy-more-of-yourself-pain-med-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ซื้อเพิ่มเอง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center mt-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day17"
                                                                    id="buy-more-of-yourself-other-med-detail-day17"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="doctor-order-other-med-day17"
                                                                name="telephone-inquiry-day17"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="doctor-order-other-med-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ตามแพทย์สั่ง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day17"
                                                                    id="doctor-order-other-med-detail-day17"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="buy-more-of-yourself-other-med-day17"
                                                                name="telephone-inquiry-day17"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="buy-more-of-yourself-other-med-day17"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ซื้อเพิ่มเอง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center mt-2 mb-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day17"
                                                                    id="buy-more-of-yourself-other-med-detail-day17"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>

                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 pt-10 pb-4 sm:row-start-auto">
                                        <div className="grid grid-cols-1 gap-2 lg:gap-6">
                                            <div className="pt-1">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2 py-2">
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day19"
                                                            id="add-mss-day19"
                                                            min="0"
                                                            className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                        <span
                                                            className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          ml/day
                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center sm:col-span-2">
                                                    <div
                                                        className="sm:mx-2 flex flex-auto items-center lg:h-5 mt-2 sm:mt-0 w-full sm:basis-2/3">
                                                        <input
                                                            type="number"
                                                            name="telephone-inquiry-day19"
                                                            id="add-mo-ir-day19"
                                                            min="0"
                                                            className="block w-full flex-1 rounded-none rounded-l-md block px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                        <span
                                                            className="px-3 py-2 inline-flex items-center rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm">
                          เม็ด/day
                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="pt-2">
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="doctor-order-other-pain-med-day19"
                                                                name="telephone-inquiry-day19"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="doctor-order-other-pain-med-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ตามแพทย์สั่ง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day19"
                                                                    id="doctor-order-other-pain-med-detail-day19"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="buy-more-of-yourself-pain-med-day19"
                                                                name="telephone-inquiry-day19"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="buy-more-of-yourself-pain-med-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ซื้อเพิ่มเอง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center mt-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day19"
                                                                    id="buy-more-of-yourself-other-med-detail-day19"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div>
                                                <fieldset>
                                                    <div className="flex flex-col gap-2">
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="doctor-order-other-med-day19"
                                                                name="telephone-inquiry-day19"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="doctor-order-other-med-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ตามแพทย์สั่ง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center my-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day19"
                                                                    id="doctor-order-other-med-detail-day19"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                            <input
                                                                id="buy-more-of-yourself-other-med-day19"
                                                                name="telephone-inquiry-day19"
                                                                type="checkbox"
                                                                className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                            <label htmlFor="buy-more-of-yourself-other-med-day19"
                                                                   className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                                ซื้อเพิ่มเอง
                                                            </label>
                                                            <div
                                                                className="flex flex-auto items-center mt-2 mb-2 mr-2">
                                                                <textarea
                                                                    name="telephone-inquiry-day19"
                                                                    id="buy-more-of-yourself-other-med-detail-day19"
                                                                    className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 pt-10 pb-4 sm:row-start-auto">
                                    </div>
                                    <div
                                        className="sm:col-span-2 py-4 border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                        Compliance
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                        <fieldset>
                                            <div className="flex flex-col sm:flex-row">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="good-of-compliance-day13"
                                                        name="telephone-inquiry-day13"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="good-of-compliance-day13"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Good
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                    <input
                                                        id="poor-of-compliance-day13"
                                                        name="telephone-inquiry-day13"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="poor-of-compliance-day13"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        poor
                                                    </label>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                        <fieldset>
                                            <div className="flex flex-col sm:flex-row">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="good-of-compliance-day15"
                                                        name="telephone-inquiry-day15"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="good-of-compliance-day15"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Good
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                    <input
                                                        id="poor-of-compliance-day15"
                                                        name="telephone-inquiry-day15"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="poor-of-compliance-day15"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        poor
                                                    </label>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                        <fieldset>
                                            <div className="flex flex-col sm:flex-row">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="good-of-compliance-day17"
                                                        name="telephone-inquiry-day17"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="good-of-compliance-day17"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Good
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                    <input
                                                        id="poor-of-compliance-day17"
                                                        name="telephone-inquiry-day17"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="poor-of-compliance-day17"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        poor
                                                    </label>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                        <fieldset>
                                            <div className="flex flex-col sm:flex-row">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="good-of-compliance-day19"
                                                        name="telephone-inquiry-day19"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="good-of-compliance-day19"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Good
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                    <input
                                                        id="poor-of-compliance-day19"
                                                        name="telephone-inquiry-day19"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="poor-of-compliance-day19"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        poor
                                                    </label>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                    </div>

                                    <div
                                        className="sm:col-span-2 pt-1.5 border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                        Concurrent treatment
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-2 sm:row-start-auto">
                                        <fieldset>
                                            <div className="flex flex-col gap-2">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="radiotherapy-day13"
                                                        name="telephone-inquiry-day13"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="radiotherapy-day13"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Radiotherapy
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="chemotherapy-day13"
                                                        name="telephone-inquiry-day13"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="chemotherapy-day13"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Chemotherapy
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="treatment-other-day13"
                                                        name="telephone-inquiry-day13"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="treatment-other-day13"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Other
                                                    </label>
                                                    <div className="flex flex-auto items-center my-2 mr-2">
                                                        <textarea
                                                            name="telephone-inquiry-day13"
                                                            id="treatment-other-detail-day13"
                                                            className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-2 sm:row-start-auto">
                                        <fieldset>
                                            <div className="flex flex-col gap-2">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="radiotherapy-day15"
                                                        name="telephone-inquiry-day15"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="radiotherapy-day15"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Radiotherapy
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="chemotherapy-day15"
                                                        name="telephone-inquiry-day15"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="chemotherapy-day15"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Chemotherapy
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="treatment-other-day15"
                                                        name="telephone-inquiry-day15"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="treatment-other-day15"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Other
                                                    </label>
                                                    <div className="flex flex-auto items-center my-2 mr-2">
                                                        <textarea
                                                            name="telephone-inquiry-day15"
                                                            id="treatment-other-detail-day15"
                                                            className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-2 sm:row-start-auto">
                                        <fieldset>
                                            <div className="flex flex-col gap-2">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="radiotherapy-day17"
                                                        name="telephone-inquiry-day17"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="radiotherapy-day17"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Radiotherapy
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="chemotherapy-day17"
                                                        name="telephone-inquiry-day17"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="chemotherapy-day17"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Chemotherapy
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="treatment-other-day17"
                                                        name="telephone-inquiry-day17"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="treatment-other-day17"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Other
                                                    </label>
                                                    <div className="flex flex-auto items-center my-2 mr-2">
                                                        <textarea
                                                            name="telephone-inquiry-day17"
                                                            id="treatment-other-detail-day17"
                                                            className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-2 sm:row-start-auto">
                                        <fieldset>
                                            <div className="flex flex-col gap-2">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="radiotherapy-day19"
                                                        name="telephone-inquiry-day19"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="radiotherapy-day19"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Radiotherapy
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="chemotherapy-day19"
                                                        name="telephone-inquiry-day19"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="chemotherapy-day19"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Chemotherapy
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto sm:grow-0 items-center mt-2 sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="treatment-other-day19"
                                                        name="telephone-inquiry-day19"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="treatment-other-day19"
                                                           className="ml-2 block text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        Other
                                                    </label>
                                                    <div className="flex flex-auto items-center my-2 mr-2">
                                                        <textarea
                                                            name="telephone-inquiry-day19"
                                                            id="treatment-other-detail-day19"
                                                            className="block w-full flex-1 rounded-md px-3 py-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-300 dark:placeholder-gray-600 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:ring-1 focus:outline-none focus:ring-blue-500 sm:text-sm"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-2 sm:row-start-auto">
                                    </div>

                                    <div
                                        className="sm:col-span-2 py-4 border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                        ผู้ให้ประวัติ
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                        <fieldset>
                                            <div className="flex flex-col sm:flex-row">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="patient-of-profiler-day13"
                                                        name="telephone-inquiry-day13"
                                                        type="radio"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="patient-of-profiler-day13"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        ผู้ป่วย
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                    <input
                                                        id="relative-of-profiler-day13"
                                                        name="telephone-inquiry-day13"
                                                        type="radio"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="relative-of-profiler-day13"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        ญาติ
                                                    </label>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                        <fieldset>
                                            <div className="flex flex-col sm:flex-row">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="patient-of-profiler-day15"
                                                        name="telephone-inquiry-day15"
                                                        type="radio"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="patient-of-profiler-day15"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        ผู้ป่วย
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                    <input
                                                        id="relative-of-profiler-day15"
                                                        name="telephone-inquiry-day15"
                                                        type="radio"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="relative-of-profiler-day15"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        ญาติ
                                                    </label>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                        <fieldset>
                                            <div className="flex flex-col sm:flex-row">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="patient-of-profiler-day17"
                                                        name="telephone-inquiry-day17"
                                                        type="radio"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="patient-of-profiler-day17"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        ผู้ป่วย
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                    <input
                                                        id="relative-of-profiler-day17"
                                                        name="telephone-inquiry-day17"
                                                        type="radio"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="relative-of-profiler-day17"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        ญาติ
                                                    </label>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                        <fieldset>
                                            <div className="flex flex-col sm:flex-row">
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center sm:mt-0 sm:ml-2">
                                                    <input
                                                        id="patient-of-profiler-day19"
                                                        name="telephone-inquiry-day19"
                                                        type="radio"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="patient-of-profiler-day19"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        ผู้ป่วย
                                                    </label>
                                                </div>
                                                <div
                                                    className="flex flex-wrap sm:flex-auto items-center mt-2 sm:mt-0 sm:ml-6">
                                                    <input
                                                        id="relative-of-profiler-day19"
                                                        name="telephone-inquiry-day19"
                                                        type="radio"
                                                        className="h-4 w-4 rounded rounded-sm border-gray-300 dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring-blue-500 transition duration-300 cursor-pointer"/>
                                                    <label htmlFor="relative-of-profiler-day19"
                                                           className="ml-2 block text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                                        ญาติ
                                                    </label>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                    </div>


                                    <div
                                        className="sm:col-span-2 py-7 border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 text-center text-sm text-gray-600 dark:text-gray-200 break-words">
                                        ผู้บันทึก
                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                        <div className="mx-2">
                                            <button
                                                type="submit"
                                                className="w-full px-2 py-3 tracking-wide text-sm text-white transition-colors duration-300 transform bg-green-500 rounded-md hover:bg-green-600 focus:outline-none focus:bg-green-500 focus:ring focus:ring-green-500 focus:ring-opacity-50">
                                                บันทึกข้อมูล Day 13
                                            </button>
                                        </div>

                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                        <div className="mx-2">
                                            <button
                                                type="submit"
                                                className="w-full px-2 py-3 tracking-wide text-sm text-white transition-colors duration-300 transform bg-green-500 rounded-md hover:bg-green-600 focus:outline-none focus:bg-green-500 focus:ring focus:ring-green-500 focus:ring-opacity-50">
                                                บันทึกข้อมูล Day 15
                                            </button>
                                        </div>

                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                        <div className="mx-2">
                                            <button
                                                type="submit"
                                                className="w-full px-2 py-3 tracking-wide text-sm text-white transition-colors duration-300 transform bg-green-500 rounded-md hover:bg-green-600 focus:outline-none focus:bg-green-500 focus:ring focus:ring-green-500 focus:ring-opacity-50">
                                                บันทึกข้อมูล Day 17
                                            </button>
                                        </div>

                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">
                                        <div className="mx-2">
                                            <button
                                                type="submit"
                                                className="w-full px-2 py-3 tracking-wide text-sm text-white transition-colors duration-300 transform bg-green-500 rounded-md hover:bg-green-600 focus:outline-none focus:bg-green-500 focus:ring focus:ring-green-500 focus:ring-opacity-50">
                                                บันทึกข้อมูล Day 19
                                            </button>
                                        </div>

                                    </div>
                                    <div
                                        className="sm:col-span-2 border-l border-t border-slate-300 border-slate-300 bg-white dark:bg-gray-800 dark:border-gray-600 py-4">

                                    </div>

                                </div>
                            </div>


                            <div className="bg-gray-50 dark:bg-gray-700 rounded-b-md px-4 py-3 text-right sm:px-6">
                                {/*<button*/}
                                {/*    type="button"*/}
                                {/*    onClick={handleBack}*/}
                                {/*    className="inline-flex justify-center px-6 py-3 tracking-wide text-sm text-white transition-colors duration-300 transform bg-blue-600 rounded-md hover:bg-blue-700 focus:outline-none focus:bg-blue-700 focus:ring focus:ring-blue-500 focus:ring-opacity-50">*/}
                                {/*    ย้อนกลับ*/}
                                {/*</button>*/}


                                <button
                                    type="submit"
                                    className="inline-flex justify-center px-6 py-3 tracking-wide text-sm text-white transition-colors duration-300 transform bg-blue-600 rounded-md hover:bg-blue-700 focus:outline-none focus:bg-blue-700 focus:ring focus:ring-blue-500 focus:ring-opacity-50">
                                    บันทึกข้อมูล
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </>
    )
}

export default ResearchData;