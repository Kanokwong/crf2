const Nation =  [
    {
        code: "002",
        name: "โปรตุเกส",
        enabled: false
    },
    {
        code: "003",
        name: "ดัตช์",
        enabled: false
    },
    {
        code: "004",
        name: "เยอรมัน",
        enabled: false
    },
    {
        code: "005",
        name: "ฝรั่งเศส",
        enabled: false
    },
    {
        code: "006",
        name: "เดนมาร์ก",
        enabled: false
    },
    {
        code: "007",
        name: "สวีเดน",
        enabled: false
    },
    {
        code: "008",
        name: "สวิส",
        enabled: false
    },
    {
        code: "009",
        name: "อิตาลี",
        enabled: false
    },
    {
        code: "010",
        name: "นอร์เวย์",
        enabled: false
    },
    {
        code: "011",
        name: "ออสเตรีย",
        enabled: false
    },
    {
        code: "012",
        name: "ไอริช",
        enabled: false
    },
    {
        code: "013",
        name: "ฟินแลนด์",
        enabled: false
    },
    {
        code: "014",
        name: "เบลเยียม",
        enabled: false
    },
    {
        code: "015",
        name: "สเปน",
        enabled: false
    },
    {
        code: "016",
        name: "รัสเซีย",
        enabled: false
    },
    {
        code: "017",
        name: "โปแลนด์",
        enabled: false
    },
    {
        code: "018",
        name: "เช็ก",
        enabled: false
    },
    {
        code: "019",
        name: "ฮังการี",
        enabled: false
    },
    {
        code: "020",
        name: "กรีก",
        enabled: false
    },
    {
        code: "021",
        name: "ยูโกสลาฟ",
        enabled: false
    },
    {
        code: "022",
        name: "ลักเซมเบิร์ก",
        enabled: false
    },
    {
        code: "023",
        name: "วาติกัน",
        enabled: false
    },
    {
        code: "024",
        name: "มอลตา",
        enabled: false
    },
    {
        code: "025",
        name: "ลีซู",
        enabled: false
    },
    {
        code: "026",
        name: "บัลแกเรีย",
        enabled: false
    },
    {
        code: "027",
        name: "โรมาเนีย",
        enabled: false
    },
    {
        code: "028",
        name: "ไซปรัส",
        enabled: false
    },
    {
        code: "029",
        name: "อเมริกัน",
        enabled: false
    },
    {
        code: "030",
        name: "แคนาดา",
        enabled: false
    },
    {
        code: "031",
        name: "เม็กซิกัน",
        enabled: false
    },
    {
        code: "032",
        name: "คิวบา",
        enabled: false
    },
    {
        code: "033",
        name: "อาร์เจนตินา",
        enabled: false
    },
    {
        code: "034",
        name: "บราซิล",
        enabled: false
    },
    {
        code: "035",
        name: "ชิลี",
        enabled: false
    },
    {
        code: "036",
        name: "อาข่า",
        enabled: false
    },
    {
        code: "037",
        name: "โคลัมเบีย",
        enabled: false
    },
    {
        code: "038",
        name: "ลั๊ว",
        enabled: false
    },
    {
        code: "039",
        name: "เปรู",
        enabled: false
    },
    {
        code: "040",
        name: "ปานามา",
        enabled: false
    },
    {
        code: "041",
        name: "อุรุกวัย",
        enabled: false
    },
    {
        code: "042",
        name: "เวเนซุเอลา",
        enabled: false
    },
    {
        code: "043",
        name: "เปอร์โตริโก้",
        enabled: false
    },
    {
        code: "044",
        name: "จีน",
        enabled: true
    },
    {
        code: "045",
        name: "อินเดีย",
        enabled: false
    },
    {
        code: "046",
        name: "เวียดนาม",
        enabled: true
    },
    {
        code: "047",
        name: "ญี่ปุ่น",
        enabled: true
    },
    {
        code: "048",
        name: "เมียนมา",
        enabled: true
    },
    {
        code: "049",
        name: "ฟิลิปปิน",
        enabled: true
    },
    {
        code: "050",
        name: "มาเลเซีย",
        enabled: true
    },
    {
        code: "051",
        name: "อินโดนีเซีย",
        enabled: true
    },
    {
        code: "052",
        name: "ปากีสถาน",
        enabled: false
    },
    {
        code: "053",
        name: "เกาหลีใต้",
        enabled: false
    },
    {
        code: "054",
        name: "สิงคโปร์",
        enabled: false
    },
    {
        code: "055",
        name: "เนปาล",
        enabled: false
    },
    {
        code: "056",
        name: "ลาว",
        enabled: true
    },
    {
        code: "057",
        name: "กัมพูชา",
        enabled: true
    },
    {
        code: "058",
        name: "ศรีลังกา",
        enabled: false
    },
    {
        code: "059",
        name: "ซาอุดีอาระเบีย",
        enabled: false
    },
    {
        code: "060",
        name: "อิสราเอล",
        enabled: false
    },
    {
        code: "061",
        name: "เลบานอน",
        enabled: false
    },
    {
        code: "062",
        name: "อิหร่าน",
        enabled: false
    },
    {
        code: "063",
        name: "ตุรกี",
        enabled: false
    },
    {
        code: "064",
        name: "บังกลาเทศ",
        enabled: false
    },
    {
        code: "065",
        name: "ถูกถอนสัญชาติ",
        enabled: false
    },
    {
        code: "066",
        name: "ซีเรีย",
        enabled: false
    },
    {
        code: "067",
        name: "อิรัก",
        enabled: false
    },
    {
        code: "068",
        name: "คูเวต",
        enabled: false
    },
    {
        code: "069",
        name: "บรูไน",
        enabled: false
    },
    {
        code: "070",
        name: "แอฟริกาใต้",
        enabled: false
    },
    {
        code: "071",
        name: "กะเหรี่ยง",
        enabled: false
    },
    {
        code: "072",
        name: "ลาหู่",
        enabled: false
    },
    {
        code: "073",
        name: "เคนยา",
        enabled: false
    },
    {
        code: "074",
        name: "อียิปต์",
        enabled: false
    },
    {
        code: "075",
        name: "เอธิโอเปีย",
        enabled: false
    },
    {
        code: "076",
        name: "ไนจีเรีย",
        enabled: false
    },
    {
        code: "077",
        name: "สหรัฐอาหรับเอมิเรตส์",
        enabled: false
    },
    {
        code: "078",
        name: "กินี",
        enabled: false
    },
    {
        code: "079",
        name: "ออสเตรเลีย",
        enabled: false
    },
    {
        code: "080",
        name: "นิวซีแลนด์",
        enabled: false
    },
    {
        code: "081",
        name: "ปาปัวนิวกินี",
        enabled: false
    },
    {
        code: "082",
        name: "ม้ง",
        enabled: false
    },
    {
        code: "083",
        name: "เมี่ยน",
        enabled: false
    },
    {
        code: "084",
        name: "ชาวเขาที่ไม่ได้รับสัญขาติไทย",
        enabled: false
    },
    {
        code: "085",
        name: "-",
        enabled: false
    },
    {
        code: "086",
        name: "จีนฮ่อ",
        enabled: false
    },
    {
        code: "087",
        name: "จีน (อดีตทหารจีนคณะชาติ ,อดีตทหารจีนชาติ)",
        enabled: false
    },
    {
        code: "088",
        name: "ผู้พลัดถิ่นสัญชาติพม่า",
        enabled: false
    },
    {
        code: "089",
        name: "ผู้อพยพเชื้อสายจากกัมพูชา",
        enabled: false
    },
    {
        code: "090",
        name: "ลาว (ลาวอพยพ)",
        enabled: false
    },
    {
        code: "091",
        name: "เขมรอพยพ",
        enabled: false
    },
    {
        code: "092",
        name: "ผู้อพยพอินโดจีนสัญชาติเวียดนาม",
        enabled: false
    },
    {
        code: "093",
        name: "รอให้สัญชาติไทย",
        enabled: false
    },
    {
        code: "094",
        name: "ไทย-อิสลาม,อิสลาม-ไทย",
        enabled: false
    },
    {
        code: "095",
        name: "ไทย-จีน,จีน-ไทย",
        enabled: false
    },
    {
        code: "096",
        name: "ไร้สัญชาติ",
        enabled: false
    },
    {
        code: "097",
        name: "อื่นๆ",
        enabled: false
    },
    {
        code: "098",
        name: "ไม่ได้สัญชาติไทย",
        enabled: false
    },
    {
        code: "099",
        name: "ไทย",
        enabled: true
    },
    {
        code: "100",
        name: "อัฟกัน",
        enabled: false
    },
    {
        code: "101",
        name: "บาห์เรน",
        enabled: false
    },
    {
        code: "102",
        name: "ภูฏาน",
        enabled: false
    },
    {
        code: "103",
        name: "จอร์แดน",
        enabled: false
    },
    {
        code: "104",
        name: "เกาหลีเหนือ",
        enabled: false
    },
    {
        code: "105",
        name: "มัลดีฟ",
        enabled: false
    },
    {
        code: "106",
        name: "มองโกเลีย",
        enabled: false
    },
    {
        code: "107",
        name: "โอมาน",
        enabled: false
    },
    {
        code: "108",
        name: "กาตาร์",
        enabled: false
    },
    {
        code: "109",
        name: "เยเมน",
        enabled: false
    },
    {
        code: "110",
        name: "เยเมน(ใต้)**",
        enabled: false
    },
    {
        code: "111",
        name: "หมู่เกาะฟิจิ",
        enabled: false
    },
    {
        code: "112",
        name: "คิริบาส",
        enabled: false
    },
    {
        code: "113",
        name: "นาอูรู",
        enabled: false
    },
    {
        code: "114",
        name: "หมู่เกาะโซโลมอน",
        enabled: false
    },
    {
        code: "115",
        name: "ตองก้า",
        enabled: false
    },
    {
        code: "116",
        name: "ตูวาลู",
        enabled: false
    },
    {
        code: "117",
        name: "วานูอาตู",
        enabled: false
    },
    {
        code: "118",
        name: "ซามัว",
        enabled: false
    },
    {
        code: "119",
        name: "แอลเบเนีย",
        enabled: false
    },
    {
        code: "120",
        name: "อันดอร์รา",
        enabled: false
    },
    {
        code: "121",
        name: "เยอรมนีตะวันออก**",
        enabled: false
    },
    {
        code: "122",
        name: "ไอซ์แลนด์",
        enabled: false
    },
    {
        code: "123",
        name: "ลิกเตนสไตน์",
        enabled: false
    },
    {
        code: "124",
        name: "โมนาโก",
        enabled: false
    },
    {
        code: "125",
        name: "ซานมารีโน",
        enabled: false
    },
    {
        code: "126",
        name: "บริติช  (อังกฤษ, สก็อตแลนด์)",
        enabled: false
    },
    {
        code: "127",
        name: "แอลจีเรีย",
        enabled: false
    },
    {
        code: "128",
        name: "แองโกลา",
        enabled: false
    },
    {
        code: "129",
        name: "เบนิน",
        enabled: false
    },
    {
        code: "130",
        name: "บอตสวานา",
        enabled: false
    },
    {
        code: "131",
        name: "บูร์กินาฟาโซ",
        enabled: false
    },
    {
        code: "132",
        name: "บุรุนดี",
        enabled: false
    },
    {
        code: "133",
        name: "แคเมอรูน",
        enabled: false
    },
    {
        code: "134",
        name: "เคปเวิร์ด",
        enabled: false
    },
    {
        code: "135",
        name: "แอฟริกากลาง",
        enabled: false
    },
    {
        code: "136",
        name: "ชาด",
        enabled: false
    },
    {
        code: "137",
        name: "คอโมโรส",
        enabled: false
    },
    {
        code: "138",
        name: "คองโก",
        enabled: false
    },
    {
        code: "139",
        name: "ไอโวเรี่ยน",
        enabled: false
    },
    {
        code: "140",
        name: "จิบูตี",
        enabled: false
    },
    {
        code: "141",
        name: "อิเควทอเรียลกินี",
        enabled: false
    },
    {
        code: "142",
        name: "กาบอง",
        enabled: false
    },
    {
        code: "143",
        name: "แกมเบีย",
        enabled: false
    },
    {
        code: "144",
        name: "กานา",
        enabled: false
    },
    {
        code: "145",
        name: "กินีบีสเซา",
        enabled: false
    },
    {
        code: "146",
        name: "เลโซโท",
        enabled: false
    },
    {
        code: "147",
        name: "ไลบีเรีย",
        enabled: false
    },
    {
        code: "148",
        name: "ลิเบีย",
        enabled: false
    },
    {
        code: "149",
        name: "มาลากาซี",
        enabled: false
    },
    {
        code: "150",
        name: "มาลาวี",
        enabled: false
    },
    {
        code: "151",
        name: "มาลี",
        enabled: false
    },
    {
        code: "152",
        name: "มอริเตเนีย",
        enabled: false
    },
    {
        code: "153",
        name: "มอริเชียส",
        enabled: false
    },
    {
        code: "154",
        name: "โมร็อกโก",
        enabled: false
    },
    {
        code: "155",
        name: "โมซัมบิก",
        enabled: false
    },
    {
        code: "156",
        name: "ไนเจอร์",
        enabled: false
    },
    {
        code: "157",
        name: "รวันดา",
        enabled: false
    },
    {
        code: "158",
        name: "เซาโตเมและปรินซิเป",
        enabled: false
    },
    {
        code: "159",
        name: "เซเนกัล",
        enabled: false
    },
    {
        code: "160",
        name: "เซเชลส์",
        enabled: false
    },
    {
        code: "161",
        name: "เซียร์ราลีโอน",
        enabled: false
    },
    {
        code: "162",
        name: "โซมาลี",
        enabled: false
    },
    {
        code: "163",
        name: "ซูดาน",
        enabled: false
    },
    {
        code: "164",
        name: "สวาซี",
        enabled: false
    },
    {
        code: "165",
        name: "แทนซาเนีย",
        enabled: false
    },
    {
        code: "166",
        name: "โตโก",
        enabled: false
    },
    {
        code: "167",
        name: "ตูนิเซีย",
        enabled: false
    },
    {
        code: "168",
        name: "ยูกันดา",
        enabled: false
    },
    {
        code: "169",
        name: "ซาอีร์",
        enabled: false
    },
    {
        code: "170",
        name: "แซมเบีย",
        enabled: false
    },
    {
        code: "171",
        name: "ซิมบับเว",
        enabled: false
    },
    {
        code: "172",
        name: "แอนติกาและบาร์บูดา",
        enabled: false
    },
    {
        code: "173",
        name: "บาฮามาส",
        enabled: false
    },
    {
        code: "174",
        name: "บาร์เบโดส",
        enabled: false
    },
    {
        code: "175",
        name: "เบลิซ",
        enabled: false
    },
    {
        code: "176",
        name: "คอสตาริกา",
        enabled: false
    },
    {
        code: "177",
        name: "โดมินิกา",
        enabled: false
    },
    {
        code: "178",
        name: "โดมินิกัน",
        enabled: false
    },
    {
        code: "179",
        name: "เอลซัลวาดอร์",
        enabled: false
    },
    {
        code: "180",
        name: "เกรเนดา",
        enabled: false
    },
    {
        code: "181",
        name: "กัวเตมาลา",
        enabled: false
    },
    {
        code: "182",
        name: "เฮติ",
        enabled: false
    },
    {
        code: "183",
        name: "ฮอนดูรัส",
        enabled: false
    },
    {
        code: "184",
        name: "จาเมกา",
        enabled: false
    },
    {
        code: "185",
        name: "นิการากัว",
        enabled: false
    },
    {
        code: "186",
        name: "เซนต์คิตส์และเนวิส",
        enabled: false
    },
    {
        code: "187",
        name: "เซนต์ลูเซีย",
        enabled: false
    },
    {
        code: "188",
        name: "เซนต์วินเซนต์และเกรนาดีนส์",
        enabled: false
    },
    {
        code: "189",
        name: "ตรินิแดดและโตเบโก",
        enabled: false
    },
    {
        code: "190",
        name: "โบลีเวีย",
        enabled: false
    },
    {
        code: "191",
        name: "เอกวาดอร์",
        enabled: false
    },
    {
        code: "192",
        name: "กายอานา",
        enabled: false
    },
    {
        code: "193",
        name: "ปารากวัย",
        enabled: false
    },
    {
        code: "194",
        name: "ซูรินาเม",
        enabled: false
    },
    {
        code: "195",
        name: "อาหรับ",
        enabled: false
    },
    {
        code: "196",
        name: "คะฉิ่น",
        enabled: false
    },
    {
        code: "197",
        name: "ว้า",
        enabled: false
    },
    {
        code: "198",
        name: "ไทยใหญ่",
        enabled: false
    },
    {
        code: "199",
        name: "ไทยลื้อ",
        enabled: false
    },
    {
        code: "200",
        name: "ขมุ",
        enabled: false
    },
    {
        code: "201",
        name: "ตองสู",
        enabled: false
    },
    {
        code: "202",
        name: "เงี้ยว**",
        enabled: false
    },
    {
        code: "203",
        name: "ละว้า",
        enabled: false
    },
    {
        code: "204",
        name: "แม้ว",
        enabled: false
    },
    {
        code: "205",
        name: "ปะหร่อง",
        enabled: false
    },
    {
        code: "206",
        name: "ถิ่น",
        enabled: false
    },
    {
        code: "207",
        name: "ปะโอ",
        enabled: false
    },
    {
        code: "208",
        name: "มอญ",
        enabled: false
    },
    {
        code: "209",
        name: "มลาบรี",
        enabled: false
    },
    {
        code: "210",
        name: "เฮาะ**",
        enabled: false
    },
    {
        code: "211",
        name: "สก็อตแลน์**",
        enabled: false
    },
    {
        code: "212",
        name: "จีน (จีนฮ่ออิสระ)",
        enabled: false
    },
    {
        code: "213",
        name: "จีนอพยพ**",
        enabled: false
    },
    {
        code: "214",
        name: "จีน (จีนฮ่ออพยพ)",
        enabled: false
    },
    {
        code: "215",
        name: "ไต้หวัน**",
        enabled: false
    },
    {
        code: "216",
        name: "ยูเครน",
        enabled: false
    },
    {
        code: "217",
        name: "อาณานิคมอังกฤษ**",
        enabled: false
    },
    {
        code: "218",
        name: "ดูไบ**",
        enabled: false
    },
    {
        code: "219",
        name: "จีน(ฮ่องกง)",
        enabled: false
    },
    {
        code: "220",
        name: "จีน(ไต้หวัน)",
        enabled: false
    },
    {
        code: "221",
        name: "โครเอเชีย",
        enabled: false
    },
    {
        code: "223",
        name: "คาซัค",
        enabled: false
    },
    {
        code: "222",
        name: "บริทิธ**",
        enabled: false
    },
    {
        code: "224",
        name: "อาร์เมเนีย",
        enabled: false
    },
    {
        code: "225",
        name: "อาเซอร์ไบจาน",
        enabled: false
    },
    {
        code: "226",
        name: "จอร์เจีย",
        enabled: false
    },
    {
        code: "227",
        name: "คีร์กีซ",
        enabled: false
    },
    {
        code: "228",
        name: "ทาจิก",
        enabled: false
    },
    {
        code: "229",
        name: "อุซเบก",
        enabled: false
    },
    {
        code: "230",
        name: "หมู่เกาะมาร์แชลล์",
        enabled: false
    },
    {
        code: "231",
        name: "ไมโครนีเซีย",
        enabled: false
    },
    {
        code: "232",
        name: "ปาเลา",
        enabled: false
    },
    {
        code: "233",
        name: "เบลารุส",
        enabled: false
    },
    {
        code: "234",
        name: "บอสเนียและเฮอร์เซโกวีนา",
        enabled: false
    },
    {
        code: "235",
        name: "เติร์กเมน",
        enabled: false
    },
    {
        code: "236",
        name: "เอสโตเนีย",
        enabled: false
    },
    {
        code: "237",
        name: "ลัตเวีย",
        enabled: false
    },
    {
        code: "238",
        name: "ลิทัวเนีย",
        enabled: false
    },
    {
        code: "239",
        name: "มาซิโดเนีย",
        enabled: false
    },
    {
        code: "240",
        name: "มอลโดวา",
        enabled: false
    },
    {
        code: "241",
        name: "สโลวัก",
        enabled: false
    },
    {
        code: "242",
        name: "สโลวีน",
        enabled: false
    },
    {
        code: "243",
        name: "เอริเทรีย",
        enabled: false
    },
    {
        code: "244",
        name: "นามิเบีย",
        enabled: false
    },
    {
        code: "245",
        name: "โบลิเวีย",
        enabled: false
    },
    {
        code: "246",
        name: "หมู่เกาะคุก",
        enabled: false
    },
    {
        code: "247",
        name: "เนปาล (เนปาลอพยพ)",
        enabled: false
    },
    {
        code: "248",
        name: "มอญ  (ผู้พลัดถิ่นสัญชาติพม่า)",
        enabled: false
    },
    {
        code: "249",
        name: "ไทยใหญ่  (ผู้พลัดถิ่นสัญชาติพม่า)",
        enabled: false
    },
    {
        code: "250",
        name: "เวียดนาม  (ญวนอพยพ)",
        enabled: false
    },
    {
        code: "251",
        name: "มาเลเชีย  (อดีต จีนคอมมิวนิสต์)",
        enabled: false
    },
    {
        code: "252",
        name: "จีน  (อดีต จีนคอมมิวนิสต์)",
        enabled: false
    },
    {
        code: "253",
        name: "สิงคโปร์  (อดีต จีนคอมมิวนิสต์)",
        enabled: false
    },
    {
        code: "254",
        name: "กะเหรี่ยง  (ผู้หลบหนีเข้าเมือง)",
        enabled: false
    },
    {
        code: "255",
        name: "มอญ  (ผู้หลบหนีเข้าเมือง)",
        enabled: false
    },
    {
        code: "256",
        name: "ไทยใหญ่  (ผู้หลบหนีเข้าเมือง)",
        enabled: false
    },
    {
        code: "257",
        name: "กัมพูชา  (ผู้หลบหนีเข้าเมือง)",
        enabled: false
    },
    {
        code: "258",
        name: "มอญ  (ชุมชนบนพื้นที่สูง)",
        enabled: false
    },
    {
        code: "259",
        name: "กะเหรี่ยง  (ชุมชนบนพื้นที่สูง)",
        enabled: false
    },
    {
        code: "260",
        name: "ปาเลสไตน์",
        enabled: false
    },
    {
        code: "261",
        name: "ติมอร์-เลสเต",
        enabled: false
    },
    {
        code: "262",
        name: "สละสัญชาติไทย",
        enabled: false
    },
    {
        code: "263",
        name: "เซอร์เบีย แอนด์ มอนเตเนโกร",
        enabled: false
    },
    {
        code: "264",
        name: "กัมพูชา(แรงงาน)",
        enabled: false
    },
    {
        code: "265",
        name: "เมียนมา(แรงงาน)",
        enabled: false
    },
    {
        code: "266",
        name: "ลาว(แรงงาน)",
        enabled: false
    },
    {
        code: "267",
        name: "เซอร์เบียน",
        enabled: false
    },
    {
        code: "270",
        name: "กาบูเวอร์ดี",
        enabled: false
    },
    {
        code: "271",
        name: "คอซอวอ",
        enabled: false
    },
    {
        code: "272",
        name: "เซาท์ซูดาน",
        enabled: false
    },
    {
        code: "268",
        name: "มอนเตเนกริน",
        enabled: false
    },
    {
        code: "989",
        name: "บุคคลที่ไม่มีสถานะทางทะเบียน",
        enabled: false
    },
    {
        code: "999",
        name: "ไม่ระบุ",
        enabled: false
    },
]

export default Nation;