import bcrypt from "bcryptjs";

const users = [
    {
        title: "นางสาว",
        firstName: "กนกวงศ์",
        lastName: "จวบสมบัติ",
        email: "kanokwong.udch@gmail.com",
        username: "kanokwong",
        password: bcrypt.hashSync("12276@kanokwong",10),
        hospital: {
            code: "UD",
            name: "รพ.มะเร็งอุดรธานี",
        },
        isAdmin: true
    }
];

export default users;