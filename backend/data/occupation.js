const Occupation = [
    {
        code: "1323",
        name: "รับเหมาก่อสร้าง",
        enabled: true
    },
    {
        code: "9000",
        name: "นักเรียน/นักศึกษา",
        enabled: true
    },
    {
        code: "9001",
        name: "แม่บ้าน/พ่อบ้าน",
        enabled: true
    },
    {
        code: "9003",
        name: "ข้าราชการบำนาญ",
        enabled: true
    },
    {
        code: "9004",
        name: "ลูกจ้างประจำ",
        enabled: true
    },
    {
        code: "9005",
        name: "ลูกจ้างชั่วคราว",
        enabled: true
    },
    {
        code: "0001",
        name: "ทำนา/ทำสวน/ทำไร่",
        enabled: true
    },
    {
        code: "1110",
        name: "ข้าราชการ",
        enabled: true
    },
    {
        code: "0013",
        name: "วิศวกร",
        enabled: true
    },
    {
        code: "0003",
        name: "ว่างงาน",
        enabled: true
    },
    {
        code: "0004",
        name: "ข้าราชการท้องถิ่น/นักการเมืองท้องถิ่น",
        enabled: true
    },
    {
        code: "0005",
        name: "รับจ้างทั่วไป",
        enabled: true
    },
    {
        code: "0006",
        name: "ขับรถรับจ้าง",
        enabled: true
    },
    {
        code: "0007",
        name: "ธุรกิจส่วนตัว",
        enabled: true
    },
    {
        code: "0010",
        name: "แม่ชี",
        enabled: true
    },
    {
        code: "0009",
        name: "เอกชน",
        enabled: true
    },
    {
        code: "0011",
        name: "รัฐวิสาหกิจ",
        enabled: true
    },
    {
        code: "0012",
        name: "ค้าขาย",
        enabled: true
    },
    {
        code: "0000",
        name: "อื่นๆ",
        enabled: true
    },
    {
        code: "0014",
        name: "นักบวช/สมณะ",
        enabled: true
    },
    {
        code: "11101",
        name: "พนักงานราชการ",
        enabled: true
    }
]

export default Occupation;