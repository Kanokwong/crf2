const hospitals = [
    {
        id: "1",
        code: "MT",
        name: "รพ.มหาวชิราลงกรณธัญบุรี",
    },
    {
        id: "2",
        code: "NC",
        name: "สถาบันมะเร็งแห่งชาติ",
    },
    {
        id: "3",
        code: "LA",
        name: "รพ.มะเร็งลำปาง",
    },
    {
        id: "4",
        code: "LO",
        name: "รพ.มะเร็งลพบุรี",
    },
    {
        id: "5",
        code: "CH",
        name: "รพ.มะเร็งชลบุรี",
    },
    {
        id: "6",
        code: "UD",
        name: "รพ.มะเร็งอุดรธานี",
    },
    {
        id: "7",
        code: "UB",
        name: "รพ.มะเร็งอุบลราชธานี",
    },
    {
        id: "8",
        code: "SU",
        name: "รพ.มะเร็งสุราษฎร์ธานี",
    },
    {
        id: "9",
        code: "BU",
        name: "รพ.บุรีรัมย์",
    },
];

export default hospitals;