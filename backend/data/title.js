const Title = [
    // {
    //     codeId: "1",
    //     code: "132",
    //     name: "ศาสตราจารย์นายแพทย์",
    //     shortName: "ศจ.น.พ."
    // },
    // {
    //     codeId: "2",
    //     code: "133",
    //     name: "แพทย์หญิงคุณหญิง",
    //     shortName: "พ.ญ.คุณหญิง"
    // },
    {
        codeId: "1",
        code: "134",
        name: "นายแพทย์",
        shortName: "น.พ."
    },
    {
        codeId: "2",
        code: "135",
        name: "แพทย์หญิง",
        shortName: "พ.ญ."
    },
    {
        codeId: "3",
        code: "001",
        name: "ดอกเตอร์นายแพทย์",
        shortName: "ดร.น.พ."
    },
    {
        codeId: "4",
        code: "535",
        name: "เรืออากาศเอกนายแพทย์",
        shortName: "ร.อ.น.พ."
    },
    {
        codeId: "5",
        code: "140",
        name: "ดอกเตอร์",
        shortName: "ดร."
    },
    {
        codeId: "6",
        code: "002",
        name: "พยาบาลวิชาชีพ",
        shortName: "พว."
    },
    {
        codeId: "7",
        code: "136",
        name: "ทันตแพทย์",
        shortName: "ท.พ."
    },
    {
        codeId: "8",
        code: "137",
        name: "ทันตแพทย์หญิง",
        shortName: "ท.ญ."
    },
    // {
    //     codeId: "6",
    //     code: "141",
    //     name: "ผู้ช่วยศาสตราจารย์",
    //     shortName: "ผศ."
    // },
    // {
    //     codeId: "7",
    //     code: "142",
    //     name: "รองศาสตราจารย์",
    //     shortName: "รศ."
    // },
    // {
    //     codeId: "8",
    //     code: "143",
    //     name: "ศาสตราจารย์",
    //     shortName: "ศจ."
    // },
    {
        codeId: "9",
        code: "144",
        name: "เภสัชกรชาย",
        shortName: "ภก."
    },
    {
        codeId: "10",
        code: "145",
        name: "เภสัชกรหญิง",
        shortName: "ภญ."
    },
    {
        codeId: "11",
        code: "962",
        name: "นาย",
        shortName: "นาย"
    },
    {
        codeId: "12",
        code: "963",
        name: "นางสาว",
        shortName: "น.ส."

    },
    {
        codeId: "13",
        code: "964",
        name: "นาง",
        shortName: "นาง"
    },
]

export default Title;