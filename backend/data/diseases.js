const Diseases = [
    {"code": "1", "name": "ไม่มีโรคประจำตัว", "enabled": true},
    {"code": "2", "name": "เบาหวาน", "enabled": true},
    {"code": "3", "name": "โรคไต", "enabled": true},
    {"code": "4", "name": "ความดันโลหิตสูง", "enabled": true},
    {"code": "5", "name": "ตับแข็ง", "enabled": true},
]

export default Diseases;