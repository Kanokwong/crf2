const Marriage = [
    {
        code: "1",
        name: "โสด"
    },
    {
        code: "2",
        name: "คู่"
    },
    {
        code: "3",
        name: "หม้าย"
    },
    {
        code: "4",
        name: "หย่า"
    },
    {
        code: "5",
        name: "แยก"
    },
    {
        code: "6",
        name: "สมณะ"
    },
    {
        code: "9",
        name: "ไม่ทราบ"
    }
]

export default Marriage;
