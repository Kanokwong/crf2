import express from "express";
import users from "./data/users.js";
import User from "./models/userModel.js";
import Hospital from "./models/hospitalModel.js";
import hospitals from "./data/hospitals.js";
import provinces from "./data/provinces.js";
import Province from "./models/provinceModel.js";
import expressAsyncHandler from "express-async-handler";
import Title from "./models/titleModel.js";
import titles from "./data/title.js";
import Nation from "./models/nationModel.js";
import nations from "./data/nation.js";
import occupation from "./data/occupation.js";
import Occupation from "./models/occupationModel.js";
import Marriage from "./models/marriageModel.js";
import marriages from "./data/marriage.js";
import Education from "./models/educationModel.js";
import educations from "./data/education.js";
import Disease from "./models/diseaseModel.js";
import diseases from "./data/diseases.js";

const importData = express.Router();

importData.post("/users", expressAsyncHandler(async (req, res) => {
    await User.deleteMany({});
    const importUsers = await User.insertMany(users);
    res.send({importUsers});
}));

importData.post("/hospitals", expressAsyncHandler(async (req, res) => {
    await Hospital.deleteMany({});
    const importHospitals = await Hospital.insertMany(hospitals);
    res.send({importHospitals});
}));

importData.post("/provinces", expressAsyncHandler(async (req, res) => {
    await Province.deleteMany({});
    const importProvinces = await Province.insertMany(provinces);
    res.send({importProvinces});
}));

importData.post("/titles", expressAsyncHandler(async (req, res) => {
    await Title.deleteMany({});
    const importTitles = await Title.insertMany(titles);
    res.send({importTitles});
}));

importData.post("/nations", expressAsyncHandler(async (req, res) => {
    await Nation.deleteMany({});
    const importNations = await Nation.insertMany(nations);
    res.send({importNations});
}));

importData.post("/occupations", expressAsyncHandler(async (req, res) => {
    await Occupation.deleteMany({});
    const importOccupations = await Occupation.insertMany(occupation);
    res.send({importOccupations});
}));

importData.post("/marriages", expressAsyncHandler(async (req, res) => {
    await Marriage.deleteMany({});
    const importMarriages = await Marriage.insertMany(marriages);
    res.send({importMarriages});
}));


importData.post("/educations", expressAsyncHandler(async (req, res) => {
    await Education.deleteMany({});
    const importEducations = await Education.insertMany(educations);
    res.send({importEducations});
}));

importData.post("/diseases", expressAsyncHandler(async (req, res) => {
    await Disease.deleteMany({});
    const importDiseases = await Disease.insertMany(diseases);
    res.send({importDiseases});
}));

export default importData;