import mongoose from "mongoose";

const diseaseModel = mongoose.Schema(
    {
        code: {
            type: Number,
            require: true
        },
        name: {
            type: String,
            require: true
        },
        enabled: {
            type: Boolean,
            require: true,
            default: true
        },
    },
    {
        timestamps: true,
    }
);

const Disease = mongoose.model("Disease", diseaseModel);

export default Disease;