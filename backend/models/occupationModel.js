import mongoose from "mongoose";

const occupationModel = mongoose.Schema(
    {
        code: {
            type: Number,
            require: true
        },
        name: {
            type: String,
            require: true
        },
        enabled: {
            type: Boolean,
            require: true,
            default: true
        },
    },
    {
        timestamps: true,
    }
);

const Occupation = mongoose.model("Occupation", occupationModel);

export default Occupation;