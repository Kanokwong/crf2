import mongoose from "mongoose";

const generalFormSchema = mongoose.Schema(
    {
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User",
            required: true,
        },
        birthDay: {
            type: String,
            require: true,
        },
        age: {
            type: String,
            require: true,
            default: 0,
        },
        sex: {
            type: String,
            require: true,
            enum: ["ชาย", "หญิง"],
        },
        province: {
            provinceCode: {
                type: String,
                require: true,
            },
            provinceName: {
                type: String,
                require: true,
            },
        },
        race: {
            type: String,
            require: true,
        },
        nationality: {
            type: String,
            require: true,
        },
        occupation: {
            type: String,
            require: true,
        },
        marriage: {
            type: String,
            require: true,
            enum: ["โสด", "คู่", "หม้าย/แยก/หย่าร้าง"],
        },
        education: {
            type: String,
            require: true,
            enum: ["ประถมศึกษา", "มัธยมศึกษาตอนต้น", "มัธยมศึกษาตอนปลาย/ปวส.", "ปวส./อนุปริญญา", "ปริญญาตรี", "สูงกว่าปริญญาตรี"],
        },
        drinking: {
            type: String,
            require: true,
        },
        smoking: {
            type: String,
            require: true,
        },
        cannabis: {
            usage: {
                type: String,
                require: true,
                enum: ["เคย", "ไม่เคย"],
            },
            reason: {
                type: String,
                require: true,
            },
            lastTimeDate: {
                type: String,
                require: true,
            },
            lastTimeAbout: {
                type: String,
                require: true,
            },
            sideEffect: {
                type: String,
                require: true,
            },
            type: {
                type: String,
                require: true,
            }
        },
        narcotic: {
            type: String,
            require: true,
        },
        drugAllergy: {
            type: String,
            require: true,
        },
        congenitalDisease: {
            type: String,
            require: true,
        }

    },
    {
        timestamps: true,
    }
);

const GeneralForm = mongoose.model("GeneralForm", generalFormSchema);
export default GeneralForm;