import mongoose from "mongoose";

const nationModel = mongoose.Schema(
    {
        code: {
            type: Number,
            require: true
        },
        name: {
            type: String,
            require: true
        },
        enabled: {
            type: Boolean,
            require: true,
            default: true
        },
    },
    {
        timestamps: true,
    }
);

const Nation = mongoose.model("Nation", nationModel);
export default Nation;