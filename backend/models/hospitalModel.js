import mongoose from "mongoose";

const hospitalSchema = mongoose.Schema(
    {
        hospitalId: {
            type: Number,
            require: true,
        },
        code: {
            type: String,
            require: true,
        },
        name: {
            type: String,
            require: true,
        },
    },
    {
        timestamps: true,
    }
);

const Hospital = mongoose.model("Hospital", hospitalSchema);
export default Hospital;