import mongoose from "mongoose";

const marriageModel = mongoose.Schema(
    {
        code: {
            type: Number,
            require: true
        },
        name: {
            type: String,
            require: true
        },
    },
    {
        timestamps: true,
    }
)

const Marriage = mongoose.model("Marriage", marriageModel);
export default Marriage;