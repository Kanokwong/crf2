import mongoose from "mongoose";

const educationModel = mongoose.Schema(
    {
        code: {
            type: Number,
            require: true
        },
        name: {
            type: String,
            require: true
        },
    },
    {
        timestamps: true,
    }
)

const Education = mongoose.model("Education", educationModel);
export default Education;