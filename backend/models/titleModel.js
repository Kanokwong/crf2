import mongoose from "mongoose";


const titleModel = mongoose.Schema(
    {
        codeId: {
            type: Number,
            require: true
        },
        code: {
            type: Number,
            require: true
        },
        name: {
            type: String,
            require: true
        },
        shortName: {
            type: String,
            require: true
        },
    },
    {
        timestamps: true,
    }
);

const Title = mongoose.model("Title", titleModel);
export default Title;