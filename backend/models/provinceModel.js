import mongoose from "mongoose";

const provinceSchema = mongoose.Schema(
    {
        provinceId: {
            type: Number,
            require: true,
        },
        code: {
            type: String,
            require: true,
        },
        name: {
            type: String,
            require: true,
        },
        geoId: {
            type: Number,
            require: true,
        },
    },
    {
        timestamps: true,
    }
);

const Province = mongoose.model("Province", provinceSchema);
export default Province;