import * as dotenv from "dotenv";
import db from "./config/db.js";
import express from "express";
import cors from "cors";
import importData from "./dataImport.js";
import hospitalRoute from "./routes/hospitalRoute.js";
import userRoute from "./routes/userRoute.js";
import titleRoute from "./routes/titleRoute.js";
import provinceRoute from "./routes/provinceRoute.js";
import nationRoute from "./routes/nationRoute.js";
import occupationRoute from "./routes/occupationRoute.js";
import marriageRoute from "./routes/marriageRoute.js";
import educationRoute from "./routes/educationRoute.js";
import diseaseRoute from "./routes/diseaseRoute.js";
import {errorHandler, notFound} from "./middleware/errors.js";


dotenv.config();
db();
const app = express();
app.use(cors())
app.use(express.json());


// API
app.use("/api/import", importData);
app.use("/api/hospitals", hospitalRoute);
app.use("/api/users", userRoute);
app.use("/api/titles", titleRoute);
app.use("/api/provinces", provinceRoute);
app.use("/api/nations", nationRoute);
app.use("/api/occupations", occupationRoute);
app.use("/api/marriages", marriageRoute);
app.use("/api/educations", educationRoute);
app.use("/api/diseases", diseaseRoute);

// Error handler
app.use(notFound);
app.use(errorHandler);


// // Load hospitals of data test from server
// app.get("/api/hospitals", (req, res) => {
//     res.json(hospitals);
// });
//
// // Load single hospital of data test from server
// app.get("/api/hospitals/:id", (req, res) => {
//     const hospital = hospitals.find((h) => h.hospitalId === req.params.id);
//     res.json(hospital);
// });
//
//
// // // Load single hospital from server
// // app.get("/api/hospitals/:id", (req, res) => {
// //     const hospital = hospitals.find((h) => h._id === req.params.id);
// //     res.json(hospital);
// // });


app.get("/", (req, res) => {
    res.send("API is running...");
});

const PORT = process.env.PORT || 5000;

app.listen(PORT, console.log(`Server running on port ${PORT}`))