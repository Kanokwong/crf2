import express from "express";
import expressAsyncHandler from "express-async-handler";
import Hospital from "../models/hospitalModel.js";

const hospitalRoute = express.Router();

// Get all hospitals
hospitalRoute.get("/", expressAsyncHandler(async (req, res) => {
    const hospitals = await Hospital.find({});
    res.json(hospitals);
}));

// Get hospital by id
hospitalRoute.get("/:id", expressAsyncHandler(async (req, res) => {
    const hospitals = await Hospital.findById(req.params.id);
    if(hospitals) {
        res.json(hospitals);
    }else {
        res.status(404);
        throw new Error("Hospital not found");
    }

}));


export default hospitalRoute;