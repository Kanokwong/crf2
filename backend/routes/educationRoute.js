import express from "express";
import expressAsyncHandler from "express-async-handler";
import Education from "../models/educationModel.js";

const educationRoute = express.Router();

// Get all educations
educationRoute.get("/", expressAsyncHandler(async (req, res) => {
    const educations = await Education.find({});
    res.json(educations);
}));

// Get educations by id
educationRoute.get("/:id", expressAsyncHandler(async (req, res) => {
    const educations = await Education.findById(req.params.id);
    if (educations) {
        res.json(educations);
    } else {
        res.status(404);
        throw new Error("Education not found");
    }

}));

export default educationRoute;