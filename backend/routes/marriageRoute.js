import express from "express";
import expressAsyncHandler from "express-async-handler";
import Marriage from "../models/marriageModel.js";

const marriageRoute = express.Router();

// Get all marriages
marriageRoute.get("/", expressAsyncHandler(async (req, res) => {
    const marriages = await Marriage.find({});
    res.json(marriages);
}));

// Get marriages by id
marriageRoute.get("/:id", expressAsyncHandler(async (req, res) => {
    const marriages = await Marriage.findById(req.params.id);
    if (marriages) {
        res.json(marriages);
    } else {
        res.status(404);
        throw new Error("Marriage not found");
    }

}));


export default marriageRoute;