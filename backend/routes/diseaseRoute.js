import express from "express";
import expressAsyncHandler from "express-async-handler";
import Disease from "../models/diseaseModel.js";


const diseaseRoute = express.Router();

// Get all diseases
diseaseRoute.get("/", expressAsyncHandler(async (req, res) => {
    const diseases = await Disease.find({enabled: true});
    res.json(diseases);
    // console.log(nations)
}));

// Get diseases by id
diseaseRoute.get("/:id", expressAsyncHandler(async (req, res) => {
    const diseases = await Disease.findById(req.params.id);
    if (diseases) {
        res.json(diseases);
    } else {
        res.status(404);
        throw new Error("Disease not found");
    }

}));

export default diseaseRoute;