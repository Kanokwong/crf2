import express from "express";
import expressAsyncHandler from "express-async-handler";
import User from "../models/userModel.js";
import generateToken from "../utils/generateToken.js";
import protect from "../middleware/authMiddleware.js";

const userRoute = express.Router();

// Login
userRoute.post("/login", expressAsyncHandler(async (req, res) => {
    const {username, password} = req.body;
    const user = await User.findOne({username});

    if (user && (await user.matchPassword(password))) {
        res.json({
            _id: user._id,
            title: user.title,
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            username: user.username,
            isAdmin: user.isAdmin,
            hospital: user.hospital,
            token: generateToken(user._id),
            createdAt: user.createdAt,
        });
    } else {
        res.status(401);
        throw new Error("ชื่อผู้ใช้งานหรือรหัสผ่าน ไม่ถูกต้อง");
    }
}));

// Register
userRoute.post("/register", expressAsyncHandler(async (req, res) => {
    const {title, firstName, lastName, email, username, password, confirmPassword, hospital} = req.body;
    const userExist = await User.findOne({username});
    const emailExist = await User.findOne({email});

    if (emailExist) {
        res.status(400);
        throw new Error("emailExist");
    } else if (userExist) {
        res.status(400);
        throw new Error("userExist");
    } else if (password !== confirmPassword) {
        res.status(400);
        throw new Error("PasswordNotMatch");
    }

    const user = await User.create({
        title,
        firstName,
        lastName,
        email,
        username,
        password,
        hospital,
    });

    if (user) {
        res.status(201).json({
            _id: user._id,
            title: user.title,
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            username: user.username,
            hospital: user.hospital,
            isAdmin: user.isAdmin,
            token: generateToken(user._id),
        });
    } else {
        res.status(400);
        throw new Error("ชื่อผู้ใช้งาน ไม่ถูกต้อง");
    }
}));


// Profile
userRoute.get("/profile", protect, expressAsyncHandler(async (req, res) => {
    // res.send("User Profile");
    const user = await User.findById(req.user._id);
    if (user) {
        res.json({
            _id: user._id,
            title: user.title,
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            username: user.username,
            hospital: user.hospital,
            isAdmin: user.isAdmin,
            createdAt: user.createdAt,
        });
    } else {
        res.status(404);
        throw new Error("ไม่พบ ชื่อผู้ใช้งาน ในระบบ");
    }
}));

// Update Profile
userRoute.put("/profile", protect, expressAsyncHandler(async (req, res) => {
        // res.send("User Profile");
        const user = await User.findById(req.user._id);
        if (user) {
            user.hospital = req.body.hospital || user.hospital
            user.title = req.body.title || user.title
            user.firstName = req.body.firstName || user.firstName
            user.lastName = req.body.lastName || user.lastName
            user.email = req.body.email || user.email
            user.username = req.body.username || user.username
            if (req.body.password) {
                user.password = req.body.password
            }

            const updatedUser = await user.save()
            res.json({
                _id: updatedUser._id,
                title: updatedUser.title,
                firstName: updatedUser.firstName,
                lastName: updatedUser.lastName,
                email: updatedUser.email,
                username: updatedUser.username,
                hospital: updatedUser.hospital,
                isAdmin: updatedUser.isAdmin,
                createdAt: updatedUser.createdAt,
                token: generateToken(updatedUser._id),
            });
        } else {
            res.status(404);
            throw new Error("ไม่พบ ชื่อผู้ใช้งาน ในระบบ");
        }
    }
));

export default userRoute;