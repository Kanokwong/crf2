import express from "express";
import expressAsyncHandler from "express-async-handler";
import Province from "../models/provinceModel.js";

const provinceRoute = express.Router();

// Get all hospitals
provinceRoute.get("/", expressAsyncHandler(async (req, res) => {
    const provinces = await Province.find({});
    res.json(provinces);
}));

// Get hospital by id
provinceRoute.get("/:id", expressAsyncHandler(async (req, res) => {
    const provinces = await Province.findById(req.params.id);
    if(provinces) {
        res.json(provinces);
    }else {
        res.status(404);
        throw new Error("Province not found");
    }

}));


export default provinceRoute;