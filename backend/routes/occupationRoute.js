import express from "express";
import expressAsyncHandler from "express-async-handler";
import Occupation from "../models/occupationModel.js";


const occupationRoute = express.Router();

// Get all occupations
occupationRoute.get("/", expressAsyncHandler(async (req, res) => {
    const occupations = await Occupation.find({enabled: true});
    res.json(occupations);
    // console.log(nations)
}));

// Get occupation by id
occupationRoute.get("/:id", expressAsyncHandler(async (req, res) => {
    const occupations = await Occupation.findById(req.params.id);
    if (occupations) {
        res.json(occupations);
    } else {
        res.status(404);
        throw new Error("Occupation not found");
    }

}));

export default occupationRoute;