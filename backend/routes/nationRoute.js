import express from "express";
import expressAsyncHandler from "express-async-handler";
import Nation from "../models/nationModel.js";

const nationRoute = express.Router();

// Get all nations
nationRoute.get("/", expressAsyncHandler(async (req, res) => {
    const nations = await Nation.find({enabled: true});
    res.json(nations);
    // console.log(nations)
}));

// Get nation by id
nationRoute.get("/:id", expressAsyncHandler(async (req, res) => {
    const nations = await Nation.findById(req.params.id);
    if (nations) {
        res.json(nations);
    } else {
        res.status(404);
        throw new Error("Nation not found");
    }

}));


export default nationRoute;