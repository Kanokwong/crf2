import express from "express";
import expressAsyncHandler from "express-async-handler";
import Title from "../models/titleModel.js";

const titleRoute = express.Router();

// Get all titles
titleRoute.get("/", expressAsyncHandler(async (req, res) => {
    const titles = await Title.find({});
    res.json(titles);
}));

// Get titles by id
titleRoute.get("/:id", expressAsyncHandler(async (req, res) => {
    const titles = await Title.findById(req.params.id);
    if(titles) {
        res.json(titles);
    }else {
        res.status(404);
        throw new Error("Title not found");
    }

}));


export default titleRoute;